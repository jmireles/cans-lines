package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Resps is the array of one or several RespResp or an error
type Resps struct {
	// One or more responses since there were one or more requests
	// where every respond can have statusCode, content and CANs differently.
	Resps []*Resp `json:"resps,omitempty"`

	// Error shows an error when even not was possible to do any
	// request such as API invalid path
	Error string `json:"error,omitempty"`
}

func NewResps(resps interface{}) *Resps {
	if resps != nil {
		switch resps.(type) {
		case error:
			return &Resps{Error:(resps.(error)).Error()}
		case []*Resp:
			return &Resps{Resps: resps.([]*Resp)}
		}
	}
	return &Resps{}
}


func (r *Resps) Json() []byte {
	return JsonBytes(r)
}



type Resp struct {
	// StatusCode is the HTTP code responded by server
	StatusCode int `json:"status,omitempty"`

	// Content is the HTTP response after CAN request was sent
	// Should be a string with a Json for cans-base/HttpResp that
	// is showing Net, Data, Sent (bytes) of the request
	Content string `json:"content"`

	// Resps are all the CAN responses after CAN request.
	// Normally responses are for Get type Requests not Set type.
	Cans []*Can `json:"cans,omitempty"`

	// Error can represent a Json content parsing or timeout elapsed
	Error string `json:"error,omitempty"`
}

// NewResp do a HTTP post to the given url
// and return response status code, the content as Resp
// or request error
func NewResp(timeout time.Duration, url string) *Resp {
	// Timeouts? see:
	// https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/
	r := &Resp{}
	if resp, err := http.Post(url, "", nil); err != nil {
		r.Set(nil, err)
	} else {
		defer resp.Body.Close()
		if body, err := ioutil.ReadAll(resp.Body); err != nil {
			r.Set(nil, err)
		} else {
			r.Content = string(body)
		}
		r.StatusCode = resp.StatusCode
	}
	return r
}

func NewRespError(err error) *Resp {
	r := &Resp{}
	r.Set(nil, err)
	return r
}

// Set appends to Resp fields Error and/or Cans
func (r *Resp) Set(cans []*Can, err error) {
	if cans != nil {
		r.Cans = cans
	}
	if err != nil {
		r.Error = fmt.Sprintf("%q", err.Error())
	}
}

func (r *Resp) Json() []byte {
	return JsonBytes(r)
}

func RespErrorJson(err string) []byte {
	resp := NewRespError(fmt.Errorf("%q", err))
	return resp.Json()
}

