package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"sort"
)

type Servers struct {
	Project []*Server `json:"project,omitempty"`
	Error   error     `json:"error,omitempty"`
}

// NewServers builds a Servers from Json string(s)
//	- src can be a string json
//	- src can be a []string jsons of individual project definitions
func NewServers(src interface{}) (*Servers, error) {
	if src == nil {
		return nil, errors.New("Invalid src")
	}
	switch src.(type) {

	case string:
		s := src.(string)
		var servers Servers
		if err := json.Unmarshal([]byte(s), &servers); err != nil {
			return nil, err
		}
		return &servers, nil
	
	case []string:
		jsons := src.([]string)
		project := make([]*Server, 0)
		for _, json := range jsons {
			if s, err := NewServer(json); err != nil {
				return nil, err
			} else if err := s.valid(serverSocket | serverLines); err != nil {
				return nil, err
			} else {
				project = append(project, s)
			}
		}
		sort.Slice(project, func(i, j int) bool {
			return project[i].Server < project[j].Server
		})
		return &Servers{
			Project: project,
		}, nil
	
	default:
		return nil, errors.New("Invalid src type not (string,[]string)")
	}
}

// Json returns the Json representation of Servers.
func (s *Servers) Json() []byte {
	return JsonBytes(s)
}

const (
	serverSocket = 1 << iota
	serverLines
	serverCans
)

type Server struct {
	Server  int      `json:"server"`
	Sockets *Sockets `json:"sockets,omitempty"`
	Lines   *Lines   `json:"lines,omitempty"`
	Cans    []*Can   `json:"cans,omitempty"`
	Error   string   `json:"error,omitempty"`
}

// NewService builds a Server from a Json representation
func NewServer(s string) (*Server, error) {
	var server Server
	if err := json.Unmarshal([]byte(s), &server); err != nil {
		return nil, err
	}
	fields := 0	
	if server.Sockets != nil {
		fields |= serverSocket
	}
	if server.Lines != nil {
		fields |= serverLines
	}
	if len(server.Cans) > 0 {
		fields |= serverCans
	}
	if err := server.valid(fields); err != nil {
		return nil, err
	}
	return &server, nil
}

// NewServerProject returns a Server with fields Server, Sockets and Lines. Example:
//	{
//	  "server": 1,
//	  "sockets": {"ip":"localhost", "http-port":8081, "tcp-port":8082},
//	  "lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
//	}
func NewServerProject(server int, sockets *Sockets, lines *Lines) (*Server, error) {
	s := &Server{
		Server:  server,
		Sockets: sockets,
		Lines:   lines,
	}
	if err := s.valid(serverSocket | serverLines); err != nil {
		return nil, err
	}
	return s, nil
}

// NewServerLines returns a Server with fields Server and Lines. Example:
//	{
//	  "server": 1,
//	  "lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
//	}
//
func NewServerLines(server int, lines *Lines) (*Server, error) {
	s := &Server{
		Server:  server,
		Lines:   lines,
	}
	if err := s.valid(serverLines); err != nil {
		return nil, err
	}
	return s, nil
}

// NewServersCans returns a Server with fields Server and Cans. Example:
//	{
//	  "server": 1,
//	  "cans": [
//	    { "net":1, "line":1, "box":1, "comm":3, "extra":[0, 1, 60, 40, 65, 60, 40, 20] },
//	    { "net":1, "line":1, "box":1, "comm":3, "extra":[1, 20, 3, 232, 0, 19, 1, 132] },
//	    { "net":1, "line":1, "box":1, "comm":3, "extra":[2, 1, 16, 3, 184, 3, 184, 0,] },
//		{ "net":1, "line":1, "box":1, "comm":3, "extra":[3, 172, 40, 3, 184, 3, 184, 0] }
//	  ]
//	}
func NewServerCans(server int, cans []*Can) (*Server, error) {
	s := &Server{
		Server: server,
		Cans:   cans,
	}
	if err := s.valid(serverCans); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Server) valid(fields int) error {
	if s == nil {
		return errors.New("Server: nil")
	}
	if s.Server < 1 {
		return errors.New("Server: Id < 1")
	}
	if fields & serverSocket != 0 {
		if err := s.Sockets.valid(); err != nil {
			return err
		}
	}
	if fields & serverLines != 0 {
		if err := s.Lines.valid(true); err != nil {
			return err
		}
	}
	if fields & serverCans != 0 {
		for _, can := range s.Cans {
			if err := can.valid(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Server) Json() []byte {
	return JsonBytes(s)
}

// ServerCans parses servers as body in http request and
// returns the server id, the Can requests array and error.
// Json example expected to be read:
//	{
//	  "server": 1,
//	  "cans": [
//	    { "net":1, "line":1, "box":1, "comm":3, "extra":[1,2,3] },
//	  ]
//	}
func ServerCansRequest(r *http.Request) (int, []*Can, error) {
	// read client's servers extra
	defer r.Body.Close()
	var s Server
	if err := json.NewDecoder(r.Body).Decode(&s); err != nil {
		return 0, nil, err
	}
	if err := s.valid(serverCans); err != nil {
		return 0, nil, err
	}
	return s.Server, s.Cans, nil
}



