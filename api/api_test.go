package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/p1"
	"gitlab.com/jmireles/cans-base/to"
)

func TestCan(t *testing.T) {
	tt := base.Test{t}
	dst, _ := to.NewDst(1, to.P1, 7)

	comm := byte(4)
	extra := []byte{1,2,3,4,5,6,7,8}

	shouldOk := func(can *Can, extra []byte) {
		tt.Equals(byte(dst.Net), can.Net)
		tt.Equals(dst.Box, can.Box)
		tt.Equals(comm, can.Comm)
		tt.Equals(extra, can.ExtraBytes())
		tt.Equals(byte(dst.Line), can.Line)
	}
	// 1) NewCan with params
	t.Run("Params", func (t *testing.T) {
		sol := base.Sol(4)
		// reject net out of bounds
		can, err := newCan(16, byte(dst.Line), dst.Box, byte(sol), nil)
		tt.Assert(err != nil, "Net: %v", can)
		// reject extra out of bounds
		req, err := NewCanSol(dst, sol, []byte{1,2,3,4,5,6,7,8,9})
		tt.Assert(err != nil, "Req: %v", req)
		req, err = NewCanSol(dst, sol, extra)
		tt.Ok(err)
		shouldOk(req, extra)
	})
	t.Run("Json", func(t *testing.T) {
		jsonKeys := `{"net":%d,"line":%d,"box":%d,"comm":%d,"extra":%s}`
		// reject net out of bounds
		can, err := NewCanJson(`{"net":16}`)
		tt.Assert(err != nil, "Net: %v", can)
		// reject extra out of bounds
		jsonBad := fmt.Sprintf(jsonKeys, byte(dst.Net), byte(dst.Line), dst.Box, comm, "[1,2,3,4,5,6,7,8,9]")
		can, err = NewCanJson(jsonBad)
		tt.Assert(err != nil, "Can %d", can)
		jsonOk :=  fmt.Sprintf(jsonKeys, byte(dst.Net), byte(dst.Line), dst.Box, comm, "[1,2,3,4,5,6,7,8]")
		can, err = NewCanJson(jsonOk)
		tt.Ok(err)
		shouldOk(can, extra)
	})
}

func TestClient(t *testing.T) {

}

func TestLines(t *testing.T) {
	tt := base.Test{t}
	lines, err := NewLines(0)
	tt.Assert(err != nil, "Missing error for lines %v", lines)
	lines, err = NewLines(5)
	tt.Assert(err != nil, "Missing error for lines %v", lines)

	lines, err = NewLines(2)
	tt.Ok(err)
	for _, bad := range [][]int { 
		[]int{}, // no nets
		[]int{1}, // one net
		[]int{1,0,0}, // three nets
		[]int{0,0}, // sum is 0
		[]int{-1,0}, // first net lower than 0
		[]int{256,0}, // first net greater than 255
		[]int{200,200}, // sum greater than 255
	} {
		err := lines.Set(to.P1, bad)
		tt.Assert(err != nil, "Missing error for lines %v", lines)
	}
	err = lines.Set(to.PC, []int{1,0})
	tt.Assert(err != nil, "Missing error for lines %v", lines)
	for _, good := range []to.To {
		to.P1,
		to.RS,
		to.T4,
		to.I4,
	} {
		err := lines.Set(good, []int{1,0})
		tt.Ok(err)
	}
}

func TestServers(t *testing.T) {
	tt := base.Test{t}
	t.Run("FromJson", func(t *testing.T) {
		s, err := NewServer(`{
			"server": 1,
			"sockets":{"ip":"192.168.1.77","http-port":7000,"tcp-port":7001},
			"lines":{"nets":2,"pms":[10,10],"i4s":[1,0],"rss":[0,1],"t4s":[3,3]}
		}`)
		tt.Ok(err)
		tt.Equals(1, s.Server)
		so := s.Sockets
		tt.Assert(so != nil, "Unexpected sockets nil")
		tt.Equals("192.168.1.77", so.IP)
		tt.Equals(7000, so.HttpPort)
		tt.Equals(7001, so.TcpPort)

		lines := s.Lines
		tt.Assert(lines != nil, "Unexpected lines nil")
		tt.Equals(2, lines.Nets)
		tt.Equals([]int{10,10}, lines.PMs)
		tt.Equals([]int{1,0}, lines.I4s)
		tt.Equals([]int{0,1}, lines.RSs)
		tt.Equals([]int{3,3}, lines.T4s)
	})

	testLinesDefault := func() (*Lines, error) {
		if lines, err := NewLines(2); err != nil {
			return nil, err
		} else if err := lines.Set(to.P1, []int{10,10}); err != nil {
			return nil, err
		} else if err := lines.Set(to.I4, []int{1,0}); err != nil {
			return nil, err
		} else {
			return lines, nil
		}
	}

	t.Run("toJson", func(t *testing.T) {
		server := 1
		sockets := NewSocketsDefault()
		lines, err := testLinesDefault()
		tt.Ok(err)
		_, err = NewServerProject(server, sockets, lines)
		tt.Ok(err)
		_, err = NewServerLines(server, lines)
		tt.Ok(err)

		reqs := []*Can{
			&Can{ Net:1, Line:1, Box:1, Comm:3 },
		}
		_, err = NewServerCans(server, reqs)
		tt.Ok(err)

	    resps := []*Can{
	    	&Can{ Net:1, Line:1, Box:1, Comm:3, Extra:[]int{0, 1, 60, 40, 65, 60, 40, 20} },
	    	&Can{ Net:1, Line:1, Box:1, Comm:3, Extra:[]int{1, 20, 3, 232, 0, 19, 1, 132} },
	    	&Can{ Net:1, Line:1, Box:1, Comm:3, Extra:[]int{2, 1, 16, 3, 184, 3, 184, 0} },
	    	&Can{ Net:1, Line:1, Box:1, Comm:3, Extra:[]int{3, 172, 40, 3, 184, 3, 184, 0} },
	    }
		_, err = NewServerCans(server, resps)
		tt.Ok(err)
	})
	t.Run("HttpCans", func(t *testing.T) {
		for _, goodJson := range []string {
			`{"server":1,"cans":[]}`,
			`{"server":2,"cans":[{"net":1}]}`,
			`{"server":3,"cans":[{"net":1,"line":1}]}`,
			`{"server":4,"cans":[{"net":1,"line":1,"box":2}]}`,
			`{"server":5,"cans":[{"net":1,"line":1,"box":2,"comm":3}]}`,
			`{"server":6,"cans":[{"net":1,"line":1,"box":2,"comm":3,"extra":[4]}]}`,
		} {
			r := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(goodJson))
			_, _, err := ServerCansRequest(r)
			tt.Ok(err)
		}
		for _, badJson := range []string {
			``, // empty
			`{"server":"a"}`, // bad server
			`{"server":0}`, // server out of range
			`{"server":1,"cans":[{}]}`, // wrong cans
			`{"server":1,"cans":1}`, // not array
			`{"server":1,"cans":{}}`, // not array
			`{"server":1,"cans":[{"net":"1"}]}`, // bad net
			`{"server":1,"cans":[{"net":16}]}`, // net out of range
			`{"server":1,"cans":[{"net":1,"line":"1"}]}`, // bad line
			`{"server":1,"cans":[{"net":1,"line":1000}]}`, // not byte
			`{"server":1,"cans":[{"net":1,"line":1,"extra":[260]}]}`, // out of range
			`{"server":1,"cans":[{"net":1,"line":1,"extra":[-1]}]}`, // out of range
			`{"server":1,"cans":[{"net":1,"line":1,"extra":[1,2,3,4,5,6,7,8,9]}]}`, // large
		} {
			r := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(badJson))
			_, _, err := ServerCansRequest(r)
			tt.Assert(err != nil, "should reject bad json '%s'", badJson)
		}
	})
	t.Run("Servers", func(t *testing.T) {
		// accept
		goodJsons := []string {
			`{
	    		"server": 1,
	    		"sockets": {"ip":"127.0.0.1", "http-port":8081, "tcp-port":8082},
	    		"lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
	    	}`,
			`{
	    		"server": 2,
	    		"sockets": {"ip":"192.168.1.70", "http-port":8081, "tcp-port":8082},
	    		"lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
			}`,
		}
		servers, err := NewServers(goodJsons)
		tt.Ok(err)
		tt.Equals(2, len(servers.Project))
		p1 := servers.Project[0]
		tt.Equals(1, p1.Server)
		// reject
		for _, badJsons := range [][]string {
			[]string {
				`{ "server": "1" }`, // wrong server
			},
			[]string {
				`{ "server": 1 }`, // empty sockets
			},
			[]string {
				`{ "server":1,
					"sockets":{"ip":"127.0.0.1", "http-port":8081, "tcp-port":8082}
				}`, // empty lines
			},
			[]string {
				`{ "server":1,
		    		"lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
				}`, // empty sockets
			},
			[]string {
				`{ "server":1,
					"sockets":{"ip":"BAD-IP", "http-port":8081, "tcp-port":8082},
		    		"lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
				}`, // bad socket
			},
			[]string {
				`{ "server":1,
					"sockets":{"ip":"127.0.0.1", "http-port":8081, "tcp-port":8082},
		    		"lines": {"nets":2, "pms":[10,10,10], "i4s":[1,0]}
				}`, // bad lines more pms than lines
			},
			[]string {
				`{ "server":1,
					"sockets":{"ip":"127.0.0.1", "http-port":8081, "tcp-port":8082},
		    		"lines": {"nets":2, "pms":[-10,10], "i4s":[1,0]}
				}`, // negative pms
			},
			[]string {
				`{ "server":-1,
					"sockets":{"ip":"127.0.0.1", "http-port":8081, "tcp-port":8082},
		    		"lines": {"nets":2, "pms":[10,10], "i4s":[1,0]}
				}`, // negative server
			},
		} {
			_, err := NewServers(badJsons)
			tt.Assert(err != nil, "should reject project badJson %s", badJsons)
		}
	})
}

func TestSockets(t *testing.T) {
	tt := base.Test{t}
	t.Run("Default", func(t *testing.T) {
		sockets := NewSocketsDefault()
		net, dst := 1, byte(7)
		now, _ := base.UTCNow()
		req := to.NewComm(to.P1, dst, now)
		comm := p1.Set04_Config
		extra := []byte{1,10,11,12,13,14,15,16}
		req.Set(comm, extra)
		exp := "http://127.0.0.1:8081/can/1/req/1,7,0,4,1,10,11,12,13,14,15,16"
		tt.Equals(exp, sockets.ReqCanBytes(net, req.Bytes()))
	})
	t.Run("Generic", func(t *testing.T) {
		sockets, err := NewSockets("192.0.2", 0, 0)
		tt.Assert(err != nil, "bad IP for %v", sockets)
		ip, p1, p2 := "192.168.1.11", 9000, 9001
		sockets, err = NewSockets(ip, 1023, p2)
		tt.Assert(err != nil, "small port for %v", sockets)
		sockets, err = NewSockets(ip, 0xffff+1, p2)
		tt.Assert(err != nil, "big port for %v", sockets)
		sockets, err = NewSockets(ip, p1, 1023)
		tt.Assert(err != nil, "small port for %v", sockets)
		sockets, err = NewSockets(ip, p1, 0xffff+1)
		tt.Assert(err != nil, "big port for %v", sockets)
		sockets, err = NewSockets(ip, p1, p1)
		tt.Assert(err != nil, "repeated ports for %v", sockets)
		_, err = NewSockets(ip, p1, p2) // valid
		tt.Ok(err)
	})
}
