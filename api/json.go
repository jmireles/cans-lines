package api

import (
	"encoding/json"
	"fmt"
)

func JsonBytes(i interface{}) []byte {
	switch i.(type) {
	case error:
		e := i.(error)
		return []byte(fmt.Sprintf(`{"error":%q}`, e.Error()))
	default:
		if bytes, err := json.Marshal(i); err != nil {
			return []byte(fmt.Sprintf(`{"error":%q}`, err.Error()))
		} else {
			return bytes
		}
	}
}
