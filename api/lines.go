package api

import (
	"errors"
	"fmt"

	"gitlab.com/jmireles/cans-base/to"
)

type Lines struct {
	Nets int   `json:"nets"`
	PMs  []int `json:"pms,omitempty"`
	RSs  []int `json:"rss,omitempty"`
	T4s  []int `json:"t4s,omitempty"`
	I4s  []int `json:"i4s,omitempty"`
}

// NewLines create project equipment
// nets number must be 1 or 2
func NewLines(nets int) (*Lines, error) {
	lines := &Lines{Nets:nets}
	if err := lines.valid(false); err != nil {
		return nil, err
	}
	return lines, nil
}

// valid returns error if lines is invalid. If full is:
//	- false: only nets errors are returned
//	- true: nets and pms,rss,t4s,i4s errors are returned.
func (l *Lines) valid(full bool) error {
	if l == nil {
		return fmt.Errorf("Lines: nil")
	}
	if l.Nets < 1 || l.Nets > 4 {
		return fmt.Errorf("Lines: Nets out of range (1,4): %d", l.Nets)
	}
	if full {
		for _, boxes := range [][]int { 
			l.PMs,
			l.RSs,
			l.T4s,
			l.I4s,
		} {
			if err := l.validBoxes(boxes); err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *Lines) validBoxes(boxes []int) error {
	if boxes == nil {
		return nil
	}
	if len(boxes) != c.Nets {
		return fmt.Errorf("Boxes %d nets not equals nets:%d", boxes, c.Nets)
	}
	sum := 0
	for net, box := range boxes {
		if box < 0 {
			return fmt.Errorf("Boxes %v net %d lower than 0", boxes, net)
		}
		if box > 255 {
			return fmt.Errorf("Boxes %v net %d greater than 255", boxes, net)
		}
		sum += box
	}
	if sum == 0 {
		return fmt.Errorf("Boxes %v sum nets is 0.", boxes)
	}
	if sum > 255 {
		return fmt.Errorf("Boxes %v sum nets greater than 255.", boxes)
	}
	return nil
}

// Set set the boxes of type "to" for this Lines group.
// No boxes number for any net must be out of range 0-255, neither the sum
// of boxes for sum nets must be out of range 0-255.
// The valid "to" lines are PM, RS, T4 and I4.
func (l *Lines) Set(toLine to.To, boxes []int) error {
	if err := l.validBoxes(boxes); err != nil {
		return err
	}
	switch toLine {
	case to.P1:
		l.PMs = boxes
	case to.RS:
		l.RSs = boxes
	case to.T4:
		l.T4s = boxes
	case to.I4:
		l.I4s = boxes
	default:
		return fmt.Errorf("Invalid type of line: %d", toLine)
	}
	return nil
}

func (l *Lines) Equals(other *Lines) error {
	if l.Nets != other.Nets {
		return errors.New("Nets different")
	}
	if !l.equals(l.PMs, other.PMs) {
		return errors.New("PMs different")
	}
	if !l.equals(l.RSs, other.RSs) {
		return errors.New("RSs different")
	}
	if !l.equals(l.T4s, other.T4s) {
		return errors.New("T4s different")
	}
	if !l.equals(l.I4s, other.I4s) {
		return errors.New("I4s different")
	}
	return nil
}

func (l *Lines) equals(a []int, b []int) bool {
    if len(a) != len(b) {
        return false
    }
    for i, v := range a {
        if v != b[i] {
            return false
        }
    }
    return true
}



