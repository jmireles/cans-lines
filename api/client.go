package api

import (
	"context"
	"strings"
	"strconv"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/to"
)

type Client struct {
	socks   *Sockets
	log     base.Log
	timeout time.Duration
	delay   time.Duration
}

func NewClient(socks *Sockets, log base.Log) *Client {
	return &Client{
		socks:   socks,
		log:     log,
		timeout: 2 * time.Second,
		delay:   100 * time.Millisecond,
	}
}

func (c *Client) SetTimeout(timeout time.Duration) {
	c.timeout = timeout
}

// SetDelay sets the delay to sleep between consecutives requests
func (c *Client) SetDelay(delay time.Duration) {
	c.delay = delay
}

func (c *Client) Post(sols *from.Sols) *Resp {
	// delay is needed to space groups of requests
	time.Sleep(c.delay)

	req := sols.Req
	if req.Set != nil {
		// The Set request does not expect responses.
		// Do the post and return http-response without cans-data immediatly.
		return c.post(req)
	}

	masks, err := sols.Data()
	if err != nil {
		return NewRespError(err)
	}

	// The request expects responses (like get cfg).
	net := byte(req.Net)
	
	// tcp connection to receive CAN solicited responses defined in masks.
	//conn, resps, err := c.readResps(masks)
	tcp, err := from.NewTcpC(masks)
	if err != nil {
		return NewRespError(err)
	}
	conn, resps, err := tcp.Read(c.socks.TcpAddress(), net)
	if err != nil {
		return NewRespError(err)
	}
	defer conn.Close()
	
	// Do the post and return http-response with cans-data or timeout.
	ctx, cancel := context.WithTimeout(context.Background(), c.timeout)
	defer cancel()
	apiResp := c.post(req)

	// Collect responses
	for {	
		select {
		case tcps := <-resps:
			// whole mask list matched, return before timeout
			// convert from TCP cans to api's CANs
			respsCans := make([]*Can, len(tcps.Resps))
			for pos, resp := range tcps.Resps {
				respsCans[pos] = NewCanFrom(net, resp)
			}
			apiResp.Set(respsCans, nil)
			return apiResp

		case <-ctx.Done():
			// http timeout, exiting will close TCP client connection
			apiResp.Set(nil, ctx.Err())
			return apiResp
		}
	}
}

func (c *Client) post(can *to.Can) *Resp {
	// TODO do string inside to.Can
	data := can.Data()
	var sb strings.Builder
	for pos, b := range data {
		if pos > 0 {
			sb.WriteString(`,`)
		}
		sb.WriteString(strconv.Itoa(int(b)))
	}
	post := c.socks.ReqUrl(int(can.Net), sb.String())

	return NewResp(c.timeout, post)
}

// Log pushes message to available log channel.
func (c *Client) Log(format string, data ...interface{}) {
	if c.log != nil {
		c.log(format, data...)
	}
}

