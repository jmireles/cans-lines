package api

import (
	"bytes"
	"fmt"
	"net"

	"gitlab.com/jmireles/cans-base/to"
)

// Sockets holds references where to find CANs microservices or its simulator.
// Includes the ip, the http port where to access the API and the tcp port
// used to suscribe to responses, solicited or not.
type Sockets struct {
	IP       string `json:"ip"`
	HttpPort int    `json:"http-port"`
	TcpPort  int    `json:"tcp-port"`
}

// NewSocketsDefault returns a Sockets to communicate 
// with a cans microservice located at localhost IP with port http=8081, tcp=8082.
func NewSocketsDefault() *Sockets {
	return &Sockets{
		IP:       "127.0.0.1",
		HttpPort: 8081,
		TcpPort:  8082,
	}
}

// NewSockets returns a Sockets to get URIs used
// to communicate with CANs microservices.
// ip is the IP address where to locate the microservice
// http and tcp are HTTP and TCP ports.
// NewSockets returns a Sockets to communicate with a can microservice
// with given IP and http and tcp ports.
func NewSockets(ip string, httpPort, tcpPort int) (*Sockets, error) {
	s := &Sockets{
		IP:       ip, 
		HttpPort: httpPort, 
		TcpPort:  tcpPort,
	}
	if err := s.valid(); err != nil {
		return nil, err
	}
	return s, nil
}

// valid returns a non-nil error if Ip is not a valid IP and
// if http/tcp ports are out of range (1024-65535) or are equal.
func (s *Sockets) valid() error {
	if s == nil {
		return fmt.Errorf("Sockets: nil")
	}
	if net.ParseIP(s.IP) == nil {
		return fmt.Errorf("Sockets: Invalid IP: %s", s.IP)
	}
	if s.HttpPort < 1024 || s.HttpPort > 0xffff {
		return fmt.Errorf("Sockets: HTTP port out of range: %d", s.HttpPort)
	}
	if s.TcpPort < 1024 || s.TcpPort > 0xffff {
		return fmt.Errorf("Sockets: TCP port out of range: %d", s.TcpPort)	
	}
	if s.HttpPort == s.TcpPort {
		return fmt.Errorf("Sockets: HTTP and TCP ports are equal: %d", s.HttpPort)		
	}
	return nil
}

// TcpAddress returns the string to get
func (s *Sockets) TcpAddress() string {
	return fmt.Sprintf("%s:%d", s.IP, s.TcpPort)
}

// HttpAddress returns the string to get
func (s *Sockets) HttpAddress() string {
	return fmt.Sprintf("%s:%d", s.IP, s.HttpPort)
}

func (s *Sockets) HttpPortString() string {
	return fmt.Sprintf(":%d", s.HttpPort)
}

func (s *Sockets) TcpPortString() string {
	return fmt.Sprintf(":%d", s.TcpPort)
}

func (s *Sockets) url() string {
	return fmt.Sprintf("http://%s:%d", s.IP, s.HttpPort)
}

// TcpOnUrl returns string
//	http://{ip}:{http-port}/nets/tcp/on
// intended to POST service to turn on the TCP responses.
func (s *Sockets) TcpOnUrl() string {
	return fmt.Sprintf("%s/nets/tcp/on", s.url())
}

// PortOpenUrl returns string
//	http://{ip}:{http-port} /port/{port}/open
// intented to POST service to open the specified port to start receiving
// responses.
func (s *Sockets) PortOpenUrl(port int) string {
	return fmt.Sprintf("%s/port/%d/open", s.url(), port)
}

// ReqUrl returns string:
//	http://{ip}:{http-port}/can/{can}/req/{req}
// intented to POST service to request specific CANs equipment.
func (s *Sockets) ReqUrl(net int, req string) string {
	return fmt.Sprintf("%s/can/%d/req/%s", s.url(), net, req)
}

func (s *Sockets) ReqCanBytes(can int, canBytes to.Bytes) string {
	var b bytes.Buffer
	for i, n := range canBytes {
		if i > 0 {
			b.WriteString(",")
		}
		b.WriteString(fmt.Sprintf("%d", int(n)))
	}
	return s.ReqUrl(can, b.String())
}


// HttpListener returns a valid listener to socket's HTTP port 
// or port resolve/listen error.
func (s *Sockets) HttpListener() (net.Listener, error) {
	if addr, err := net.ResolveTCPAddr("tcp4", s.HttpPortString()); err != nil {
		return nil, err
	} else if listener, err := net.Listen("tcp", addr.String()); err != nil {
		return nil, err
	} else {
		return listener, nil
	}
}

// TcpAddr returns the Sockets TCP addres when valid and free to be used.
func (s *Sockets) TcpAddr() (string, error) {
	if addr, err := net.ResolveTCPAddr("tcp4", s.TcpPortString()); err != nil {
		return "", err
	} else {
		return addr.String(), nil
	}
}

