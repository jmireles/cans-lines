package api

import (
	"encoding/json"
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/to"
)

type Can struct {
	Net   byte  `json:"net"`
	Line  byte  `json:"line"`
	Box   byte  `json:"box"`
	Comm  byte  `json:"comm"`
	Extra []int `json:"extra,omitempty"`
}

func byteToIntArray(bs []byte) []int {
	i := make([]int, len(bs))
	for pos, b := range bs {
		i[pos] = int(b)
	}
	return i
}


func NewCanSol(dst *to.Dst, sol base.Sol, extra []byte) (*Can, error) {
	can := &Can{
		Net:   byte(dst.Net),
		Line:  byte(dst.Line),
		Box:   dst.Box,
		Comm:  byte(sol),
		Extra: byteToIntArray(extra),
	}
	if err := can.valid(); err != nil {
		return nil, err
	}
	return can, nil
}

func NewCanUnsol(src *from.Src, unsol base.Unsol, extra []byte) (*Can, error) {
	can := &Can{
		Net:   byte(src.Net),
		Line:  byte(src.Line),
		Box:   src.Box,
		Comm:  byte(unsol),
		Extra: byteToIntArray(extra),
	}
	if err := can.valid(); err != nil {
		return nil, err
	}
	return can, nil
}

func NewCanResp(req *Can, from from.From, sol base.Sol, extra []byte) *Can {
	return &Can{
		Net:   req.Net,
		Line:  byte(from), 
		Box:   req.Box, 
		Comm:  byte(sol),
		Extra: byteToIntArray(extra),
	}
}

func NewCanFrom(net byte, data base.Data) *Can {
	extra := data.Extra()
	e := make([]int, len(extra))
	for pos, ex := range extra {
		e[pos] = int(ex)
	}
	return &Can{
		Net:   net, 
		Line:  data.Line(),
		Box:   data[2],
		Comm:  data[3],
		Extra: e,
	}
}

// String returns a Can string representation 
// which can be used as key identifier
func (c *Can) String() string {
	extra := ""
	if b := c.ExtraBytes(); len(b) > 0 {
		extra = fmt.Sprintf("#%02x", b)
	}
	return fmt.Sprintf("%01x%02x%02x%02x%s", c.Net, c.Line, c.Box, c.Comm, extra)	
}

func (c *Can) Json() []byte {
	return JsonBytes(c)
}

func (c *Can) ExtraBytes() []byte {
	if c.Extra == nil {
		return nil
	}
	bytes := make([]byte, len(c.Extra))
	for p, i := range c.Extra {
		bytes[p] = byte(i)
	}
	return bytes
}


func NewCanJson(s string) (*Can, error) {
	var can Can
	if err := json.Unmarshal([]byte(s), &can); err != nil {
		return nil, err
	}
	if err := can.valid(); err != nil {
		return nil, err
	}
	return &can, nil
}

func newCan(net, line, box, comm byte, extra []byte) (*Can, error) {
	ints := make([]int, len(extra))
	for p, d := range extra {
		ints[p] = int(d)
	}
	can := &Can{
		Net:   net,
		Line:  line,
		Box:   box,
		Comm:  comm,
		Extra: ints,
	}
	if err := can.valid(); err != nil {
		return nil, err
	}
	return can, nil
}

// valid checks Net is within range, otherwise returns an error
// In case line is within rangedefined at cans-base/lines/to, To field is set
func (c *Can) valid() error {
	if _, err := base.NewNet(c.Net); err != nil {
		return err
	}
	if s := len(c.Extra); s > 8 {
		return fmt.Errorf("Extra out of size: %d", s)
	}
	for _, b := range c.Extra {
		if b < 0 || b > 255 {
			return fmt.Errorf("Extra byte out of range")
		}
	}
	return nil
}
