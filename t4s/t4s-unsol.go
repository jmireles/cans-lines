package t4s

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/t4"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/t4s/i18n"
	"gitlab.com/jmireles/cans-lines/unsols"
)

// UnsolsFunc implements lines.RespFunc
func UnsolsFunc(req *from.Can) (resp *lines.Resp, err error) {
	resp, err = unsols.Func(req)
	if err != nil {
		return
	}
	switch *req.Unsol {

	case base.Unsol01_Restart:
		resp.Map = unsols.MapRestart

	case t4.Unsol09_Stage:
		if m, ok := unsolStageMap(req.Extra); ok {
			resp.Map = m
		} else {
			err = unsols.ErrorInvalidStall
		}
	case t4.Unsol10_Mode:
		if m, ok := unsolModeMap(req.Extra); ok {
			resp.Map = m
		} else {
			err = unsols.ErrorInvalidStall
		}
	case t4.Unsol11_LetDown:
		err = unsols.ErrorInvalidUnsol // fixme

	case t4.Unsol13_Levels:
		if m, ok := unsolLevelMap(req.Extra); ok {
			resp.Map = m
		} else {
			err = unsols.ErrorInvalidStall
		}
	case t4.Unsol14_Colors:
		if m, ok := unsolLedsMap(req.Extra); ok {
			resp.Map = m
		} else {
			err = unsols.ErrorInvalidStall
		}

	case t4.Unsol19_Flow:
		if m, ok := unsolFlowMap(req.Extra); ok {
			resp.Map = m
		} else {
			err = unsols.ErrorInvalidStall
		}

	default:
		err = unsols.ErrorInvalidUnsol
	}
	return
}

func unsolStageMap(data []byte) (mm lines.Map, ok bool) {
	if len(data) < 2 {
		return
	}
	stall := data[0]
	m := unsols.NewStallMap(stall)
	stage := Stage(data[1] & 0xF0)
	//button := Button(data[1] & 0x03)
	if stage, ok1 := stage.value(); !ok1 {
		return
	} else {
		m.Stage(&stage)
	}
	switch stage {
	case StageReady:
		if len(data) < 5 { // change to 7 to read grams
			return
		}
		stop := Stop(data[2])
		min :=  data[3]
		sec :=  data[4]
		if stop, ok1 := stop.value(); !ok1 {
			return
		} else {
			time := 60*int(min) + int(sec)
			m.Ready(&stop, &time)
		}
	case StageLetDown:
		if len(data) < 6 {
			return
		}
		flags := data[2]
		ohms :=  uint16(data[4])<<8 + uint16(data[3])
		time :=  data[5]
		m.LetDown(&flags, &ohms, &time)
	}
	return *m.Map, true
}

func unsolModeMap(data []byte) (mm lines.Map, ok bool) {
	if len(data) < 2 {
		return
	}
	stall := data[0]
	m := unsols.NewStallMap(stall)
	mode := Mode(data[1] & 0xF0)
	//button := Button(data[1] & 0x03)
	if mode, ok1 := mode.value(); !ok1 {
		return
	} else {
		m.Mode(&mode)
	}
	return *m.Map, true
}

func unsolLevelMap(data []byte) (mm lines.Map, ok bool) {
	if len(data) < 6 {
		return
	}
	stall := data[0]
	m := unsols.NewStallMap(stall)
	stage := Stage(data[1] & 0xF0)
	//button := Button(data[1] & 0x03)
	ohms := uint16(data[3])<<8 + uint16(data[2])
	min := data[4]
	sec := data[5]
	// grs := uint16(data[7])<<8 + uint16(data[6])
	if stage, ok1 := stage.value(); !ok1 {
		return
	} else {
		time := 60*uint16(min) + uint16(sec)
		m.Level(&stage, &ohms, &time)
	}
	return *m.Map, true
}

func unsolLedsMap(data []byte) (mm lines.Map, ok bool) {
	if len(data) < 6 {
		return
	}
	stall := data[0]
	m := unsols.NewStallMap(stall)
	stage := Stage(data[1] & 0xF0)
	//button := Button(data[1] & 0x03)
	blink := Blink(data[5] & 0xF0)
	color := Color(data[5] & 0x0F)
	if stage, ok1 := stage.value(); !ok1 {
		return
	} else if blink, ok1 := blink.value(); !ok1 {
		return
	} else if color, ok1 := color.value(); !ok1 {
		return
	} else {
		_ = stage
		m.Leds(&blink, &color)
	}
	return *m.Map, true
}

func unsolFlowMap(data []byte) (mm lines.Map, ok bool) {
	if len(data) < 8 {
		return
	}
	/* fixme needs to add stall
	grams := uint16(data[1])<<8 + uint16(data[0])
	celsius := data[3]
	mhos := data[4]
	ohms := data[5]
	min := data[6]
	sec := data[7] */
	return
}




type Stage byte
const (
	StageReady Stage = 0x00
	StagePrep        = 0x10
	StagePrepEnd     = 0x20
	StageAttach      = 0x30
	StageLetDown     = 0x40
	StageMilking     = 0x50
	StageDetach      = 0x60
	StagePreWash     = 0x70
	StageWash        = 0x80
	StageWashDetach  = 0x90
)

func (s Stage) value() (v unsols.ValueStage, ok bool) {
	if pos := int(s >> 4); pos < len(stageValues) {
		v, ok = stageValues[pos], true
	}
	return
}

var stageValues = []unsols.ValueStage {
	unsols.ValueStageReady,
	unsols.ValueStagePrep,
	unsols.ValueStagePrepEnd,
	unsols.ValueStageAttach,
	unsols.ValueStageLetDown,
	unsols.ValueStageMilking,
	unsols.ValueStageDetach,
	unsols.ValueStagePreWash,
	unsols.ValueStageWash,
	unsols.ValueStageWashDetach,
}

var stageI18ns = []string {
	i18n.StageReady,
	i18n.StagePrep,
	i18n.StagePrepEnd,
	i18n.StageAttach,
	i18n.StageLetDown,
	i18n.StageMilking,
	i18n.StageDetach,
	i18n.StagePreWash,
	i18n.StageWash,
	i18n.StageWashDetach,
}

var stageStyles = []string {
	"i18n.StyleIdle",
	"i18n.StylePrep",
	"i18n.StylePrep",
	"i18n.StyleMilk",
	"i18n.StyleMilk",
	"i18n.StyleMilk",
	"i18n.StyleIdle",
	"i18n.StyleWash",
	"i18n.StyleWash",
	"i18n.StyleWash",
}

type Stop byte
const (
	StopNormal Stop = 0
	StopNoMilk      = 1
	StopUser        = 2
	StopKickoff     = 3
	StopGate        = 4
	StopMaxTime     = 5
	StopSecondTurn  = 6
	StopRestart     = 7
	StopMaxMilkTime = 8
	StopForced      = 9
)

func (s Stop) value() (v unsols.ValueStop, ok bool) {
	if pos := int(s); pos < len(stopValues) {
		v, ok = stopValues[pos], true
	}
	return
}

var stopValues = []unsols.ValueStop {
	unsols.ValueStopNormal,
	unsols.ValueStopNoMilk,
	unsols.ValueStopUser,
	unsols.ValueStopKickoff,
	unsols.ValueStopGate,
	unsols.ValueStopMaxTime,
	unsols.ValueStopSecondTurn,
	unsols.ValueStopRestart,
	unsols.ValueStopMaxMilkTime,
	unsols.ValueStopForced,
}

var stopI18ns = []string {
	i18n.StopNormal,
	i18n.StopNoMilk,
	i18n.StopUser,
	i18n.StopKickoff,
	i18n.StopGate,
	i18n.StopMaxTime,
	i18n.StopSecondTurn,
	i18n.StopRestart,
	i18n.StopMaxMilkTime,
	i18n.StopForced,
}

type Mode byte
const (
	ModeManual Mode = 0x00
	ModeAuto        = 0x10
)

func (m Mode) value() (v unsols.ValueMode, ok bool) {
	if pos := int(m >> 4); pos < len(modeValues) {
		v, ok = modeValues[pos], true
	}
	return
}

var modeValues = []unsols.ValueMode {
	unsols.ValueModeManual,
	unsols.ValueModeAuto,
}

var modeI18ns = []string {
	i18n.ModeManual,
	i18n.ModeAuto,
}

type Blink byte
const (
	BlinkSteady Blink = 0x00
	BlinkSlow   = 0x10
	BlinkMedium = 0x20
	BlinkFast   = 0x30
)

func (b Blink) value() (v unsols.ValueBlink, ok bool) {
	if pos := int(b >> 4); pos < len(blinkValues) {
		v, ok = blinkValues[pos], true
	}
	return
}

var blinkValues = []unsols.ValueBlink {
	unsols.ValueBlinkSteady,
	unsols.ValueBlinkSlow,
	unsols.ValueBlinkMedium,
	unsols.ValueBlinkFast,
}

var blinkI18ns = []string {
	i18n.BlinkSteady,
	i18n.BlinkSlow,
	i18n.BlinkMedium,
	i18n.BlinkFast,
}

type Color byte
const (
	ColorNone   Color = 0x00
	ColorDarkGray     = 0x01
	ColorLightGray    = 0x02
	ColorWhite        = 0x03
	ColorRed          = 0x04
	ColorOrange       = 0x05
	ColorMagenta      = 0x06
	ColorLightMagenta = 0x07
	ColorGreen        = 0x08
	ColorGreenBlue    = 0x09
	ColorGreenYellow  = 0x0A
	ColorYellow       = 0x0B
	ColorBlue         = 0x0C
	ColorLightGreen   = 0x0D
	ColorPurple       = 0x0E
	ColorCyan         = 0x0F
)

func (c Color) value() (v unsols.ValueColor, ok bool) {
	if pos := int(c); pos < len(colorValues) {
		v, ok = colorValues[pos], true
	}
	return
}

var colorValues = []unsols.ValueColor {
	unsols.ValueColorNone,
	unsols.ValueColorDarkGray,
	unsols.ValueColorLightGray,
	unsols.ValueColorWhite,
	unsols.ValueColorRed,
	unsols.ValueColorOrange,
	unsols.ValueColorMagenta,
	unsols.ValueColorLightMagenta,
	unsols.ValueColorGreen,
	unsols.ValueColorGreenBlue,
	unsols.ValueColorGreenYellow,
	unsols.ValueColorYellow,
	unsols.ValueColorBlue,
	unsols.ValueColorLightGreen,
	unsols.ValueColorPurple,
	unsols.ValueColorCyan,
}

var colorI18ns = []string {
	i18n.ColorNone,
	i18n.ColorDarkGray,
	i18n.ColorLightGray,
	i18n.ColorWhite,
	i18n.ColorRed,
	i18n.ColorOrange,
	i18n.ColorMagenta,
	i18n.ColorLightMagenta,
	i18n.ColorGreen,
	i18n.ColorGreenBlue,
	i18n.ColorGreenYellow,
	i18n.ColorYellow,
	i18n.ColorBlue,
	i18n.ColorLightGreen,
	i18n.ColorPurple,
	i18n.ColorCyan,
}

var colorRGBs = []string {
	"0,0,0,0", // transparent
	"150,150,150",
	"200,200,200",
	"255,255,255",
	"255,0,0",
	"255,100,0",
	"255,0,255",
	"255,175,255",
	"0,255,50",
	"100,255,200",
	"175,255,0",
	"255,255,0",
	"70,125,255",
	"150,255,200",
	"180,160,255",
	"0,255,255",
}












