package t4s

import (
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

func ConfigStallView(stall byte, xml *dom.XML, prefs *nav.Prefs) {
	xml.W0(`<div>
<div>Stall: ` + strconv.Itoa(int(stall)) + `</div>
<div>
<button>IDs</button>
<button>Stalls</button>
<button>Equipment</button>
<button>Milking</button>
<button>Profiles</button>
<button>Colors</button>
<button>Rotary</button>
</div>
<div>
</div>
</div>
	`)
}
