package t4s

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/t4"

	"gitlab.com/jmireles/cans-lines/unsols"
)

func TestUnsol(t *testing.T) {
	tt := base.Test{t}
	src, _ := from.NewSrc(2, from.T4, 11)
	can := func(unsol base.Unsol, extra []byte) *from.Can {
		can, _  := from.NewCanUnsol(src, unsol, extra)
		return can
	}
	// well formed
	restart, _  := from.NewCanUnsol(src, base.Unsol01_Restart, nil)
	ready := func(stall byte, button Button, stop Stop, min, sec byte) *from.Can {
		return can(t4.Unsol09_Stage, []byte{
			stall, 
			byte(StageReady) | byte(button),
			byte(stop),
			min,
			sec % 60,
		})
	}
	stage := func(stall byte, stage Stage, button Button) *from.Can {
		return can(t4.Unsol09_Stage, []byte{
			stall, 
			byte(stage) | byte(button),
		})
	}
	letDown := func(stall byte, button Button, flags byte, ohms uint16, time byte) *from.Can {
		return can(t4.Unsol09_Stage, []byte{
			stall, 
			byte(StageLetDown) | byte(button),
			byte(flags),
			byte(ohms),
			byte(ohms >> 8),
			time,
		})
	}
	mode := func(stall byte, button Button, mode Mode) *from.Can {
		return can(t4.Unsol10_Mode, []byte{
			stall,
			byte(mode) | byte(button),
		})
	}
	level := func(stall byte, stage Stage, button Button, ohms uint16, min, sec byte) *from.Can {
		return can(t4.Unsol13_Levels, []byte{
			stall,
			byte(stage) | byte(button),
			byte(ohms),
			byte(ohms >> 8),
			min,
			sec % 60,
		})
	}
	leds := func(stall byte, stage Stage, button Button, blink Blink, color Color) *from.Can {
		return can(t4.Unsol13_Levels, []byte{
			stall,
			byte(stage) | byte(button),
			0, // r
			0, // g
			0, // b
			byte(blink) | byte(color),
		})
	}
	// valid events:
	for can, json := range map[*from.Can]string {
		restart:                                    "{2:{7:{11:{1:1}}}}",
		// stage 0
		ready(1, Button1, StopNormal,       0,  0): "{2:{7:{11:{257:0,513:0,769:0}}}}",
		ready(1, Button1, StopNoMilk,       0, 15): "{2:{7:{11:{257:0,513:1,769:15}}}}",
		ready(1, Button1, StopUser,         0, 30): "{2:{7:{11:{257:0,513:2,769:30}}}}",
		ready(1, Button1, StopKickoff,      0, 45): "{2:{7:{11:{257:0,513:3,769:45}}}}",
		ready(1, Button1, StopGate,         1, 00): "{2:{7:{11:{257:0,513:4,769:60}}}}",
		ready(1, Button1, StopMaxTime,      1, 15): "{2:{7:{11:{257:0,513:5,769:75}}}}",
		ready(1, Button1, StopSecondTurn,   1, 30): "{2:{7:{11:{257:0,513:6,769:90}}}}",
		ready(1, Button1, StopRestart,      3, 14): "{2:{7:{11:{257:0,513:7,769:194}}}}",
		ready(1, Button1, StopMaxMilkTime, 10,  0): "{2:{7:{11:{257:0,513:8,769:600}}}}",
		ready(1, Button1, StopForced,      60,  0): "{2:{7:{11:{257:0,513:9,769:3600}}}}",
		ready(2, Button1, StopNormal,       0,  0): "{2:{7:{11:{258:0,514:0,770:0}}}}", // 2nd stall
		ready(255, Button1, StopNormal,     0,  0): "{2:{7:{11:{511:0,767:0,1023:0}}}}", // last stall
		// stages 1-3
		stage(1, StagePrep,       Button1): "{2:{7:{11:{257:1}}}}",
		stage(1, StagePrepEnd,    Button1): "{2:{7:{11:{257:2}}}}",
		stage(1, StageAttach,     Button1): "{2:{7:{11:{257:3}}}}",
		// stage 4
		letDown(1, Button1, 0, 0, 0):       "{2:{7:{11:{257:4,1025:0,1281:0,1537:0}}}}",
		letDown(1, Button1, 128, 0, 0):     "{2:{7:{11:{257:4,1025:128,1281:0,1537:0}}}}",
		letDown(1, Button1, 0, 900, 90):    "{2:{7:{11:{257:4,1025:0,1281:900,1537:90}}}}",
		letDown(2, Button1, 0, 0, 0):       "{2:{7:{11:{258:4,1026:0,1282:0,1538:0}}}}", // 2nd stall
		letDown(255, Button1, 0, 0, 0):     "{2:{7:{11:{511:4,1279:0,1535:0,1791:0}}}}", // last stall
		// stages 5-9
		stage(1, StageMilking,    Button1): "{2:{7:{11:{257:5}}}}",
		stage(1, StageDetach,     Button1): "{2:{7:{11:{257:6}}}}",
		stage(1, StagePreWash,    Button1): "{2:{7:{11:{257:7}}}}",
		stage(1, StageWash,       Button1): "{2:{7:{11:{257:8}}}}",
		stage(1, StageWashDetach, Button1): "{2:{7:{11:{257:9}}}}",
		// modes
		mode(1, Button1, ModeManual):       "{2:{7:{11:{1793:0}}}}",
		mode(1, Button1, ModeAuto):         "{2:{7:{11:{1793:1}}}}",
		mode(2, Button1, ModeManual):       "{2:{7:{11:{1794:0}}}}", // 2nd stall
		mode(255, Button1, ModeManual):     "{2:{7:{11:{2047:0}}}}", // last stall
		// levels
		level(1, StageMilking, Button1, 100,  0,  0): "{2:{7:{11:{2049:5,2305:100,2561:0}}}}",
		level(1, StageMilking, Button1, 200,  0, 15): "{2:{7:{11:{2049:5,2305:200,2561:15}}}}",
		level(1, StageMilking, Button1, 300,  0, 30): "{2:{7:{11:{2049:5,2305:300,2561:30}}}}",
		level(1, StageMilking, Button1, 400,  1,  0): "{2:{7:{11:{2049:5,2305:400,2561:60}}}}",
		level(1, StageMilking, Button1, 500,  1, 30): "{2:{7:{11:{2049:5,2305:500,2561:90}}}}",
		level(1, StageMilking, Button1, 600,  3, 14): "{2:{7:{11:{2049:5,2305:600,2561:194}}}}",
		level(1, StageMilking, Button1, 700,  5,  0): "{2:{7:{11:{2049:5,2305:700,2561:300}}}}",
		level(1, StageMilking, Button1, 800, 10,  0): "{2:{7:{11:{2049:5,2305:800,2561:600}}}}",
		level(1, StageMilking, Button1, 900, 20,  0): "{2:{7:{11:{2049:5,2305:900,2561:1200}}}}",
		level(2, StageMilking, Button1, 100,  0,  0): "{2:{7:{11:{2050:5,2306:100,2562:0}}}}", // 2nd stall
		level(255, StageMilking, Button1, 100, 0, 0): "{2:{7:{11:{2303:5,2559:100,2815:0}}}}", // last stall
		// leds
		leds(1, StageReady, Button1, BlinkSteady, ColorNone):         "{2:{7:{11:{2049:0,2305:0,2561:0}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorDarkGray):     "{2:{7:{11:{2049:0,2305:0,2561:1}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorLightGray):    "{2:{7:{11:{2049:0,2305:0,2561:2}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorWhite):        "{2:{7:{11:{2049:0,2305:0,2561:3}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorRed):          "{2:{7:{11:{2049:0,2305:0,2561:4}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorOrange):       "{2:{7:{11:{2049:0,2305:0,2561:5}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorMagenta):      "{2:{7:{11:{2049:0,2305:0,2561:6}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorLightMagenta): "{2:{7:{11:{2049:0,2305:0,2561:7}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorGreen):        "{2:{7:{11:{2049:0,2305:0,2561:8}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorGreenBlue):    "{2:{7:{11:{2049:0,2305:0,2561:9}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorGreenYellow):  "{2:{7:{11:{2049:0,2305:0,2561:10}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorYellow):       "{2:{7:{11:{2049:0,2305:0,2561:11}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorBlue):         "{2:{7:{11:{2049:0,2305:0,2561:12}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorLightGreen):   "{2:{7:{11:{2049:0,2305:0,2561:13}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorPurple):       "{2:{7:{11:{2049:0,2305:0,2561:14}}}}",
		leds(1, StageReady, Button1, BlinkSteady, ColorCyan):         "{2:{7:{11:{2049:0,2305:0,2561:15}}}}",
		leds(1, StageReady, Button1, BlinkSlow,   ColorNone):         "{2:{7:{11:{2049:0,2305:0,2561:16}}}}",
		leds(1, StageReady, Button1, BlinkMedium, ColorNone):         "{2:{7:{11:{2049:0,2305:0,2561:32}}}}",
		leds(1, StageReady, Button1, BlinkFast,   ColorNone):         "{2:{7:{11:{2049:0,2305:0,2561:48}}}}",
		leds(2, StageReady, Button1, BlinkSteady, ColorNone):         "{2:{7:{11:{2050:0,2306:0,2562:0}}}}", // 2nd stall
		leds(255, StageReady, Button1, BlinkSteady, ColorNone):       "{2:{7:{11:{2303:0,2559:0,2815:0}}}}", // last stall
	} {
		resp, err := UnsolsFunc(can)
		tt.Ok(err)
		s, _ := unsols.RTJson(nil, resp) // filter is nil to pass everything
		tt.Equals(json, s)
	}

	// invalid events
	invalidUnsol2, _  := from.NewCanUnsol(src, base.Unsol(2), nil)
	invalidUnsol5, _  := from.NewCanUnsol(src, base.Unsol(5), nil) // is nil because rejected by lines-base
	invalidUnsol9a, _ := from.NewCanUnsol(src, base.Unsol(9), nil)
	for can, e := range map[*from.Can]error {
		invalidUnsol2:  unsols.ErrorInvalidUnsol,
		invalidUnsol5:  unsols.ErrorInvalidUnsol,
		invalidUnsol9a: unsols.ErrorInvalidStall,

		ready(1, Button1, Stop(10), 0, 0):     unsols.ErrorInvalidStall, // no stop 10
		stage(1, Stage(10), Button1):          unsols.ErrorInvalidStall, // no stage 10
		mode(1, Button1, Mode(0x20)):          unsols.ErrorInvalidStall, // no mode 0x20 
		level(1, Stage(0xa0), Button1, 0, 0, 0): unsols.ErrorInvalidStall, // no stage 10

	} {
		resp, err := UnsolsFunc(can)
		tt.Assert(err!=nil, "nil error for %v", resp)
		tt.Equals(err, e)
	}
}
