package t4s

/*
import (
	"errors"
	"net/url"
	"strconv"
)

type Level struct {
	Stage Stage
	Ohms  uint16
	Min   byte
	Sec   byte
}

func NewLevel(ohmsS string, params url.Values) (*Level, error) {
	level := &Level{}
	if o, e := strconv.Atoi(ohmsS); e != nil {
		return nil, errors.New("ohms wrong format")
	} else if o < 0 || o > 0xFFFF {
		return nil, errors.New("ohms out of range (0,0xffff)")
	} else {
		level.Ohms = uint16(o)
	}
	stage, err := newStage(params.Get("stage"))
	if err != nil {
		return nil, err
	} else {
		level.Stage = stage
	}
	if m, e := strconv.Atoi(params.Get("min")); e != nil {
		return nil, errors.New("minutes wrong format")
	} else if m < 0 || m > 255 {
		return nil, errors.New("minutes out of range (0,255)")
	} else {
		level.Min = byte(m)
	}
	if s, e := strconv.Atoi(params.Get("sec")); e != nil {
		return nil, errors.New("seconds wrong format")
	} else if s < 0 || s > 59 {
		return nil, errors.New("seconds out of range (0,59)")
	} else {
		level.Sec = byte(s)
	}
	return level, nil
}

func (l *Level) Unsol(e *Event) []byte {
	return []byte{
		e.Stall,                        // pos 0
		byte(l.Stage) | byte(e.Button), // pos 1
		byte(l.Ohms),                   // pos 2 ohms-lo
		byte(l.Ohms >> 8),              // pos 3 ohms-hi
		l.Min,                          // pos 4
		l.Sec,                          // pos 5
	}
}*/

