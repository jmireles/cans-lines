package t4s



/*
type Status struct {
	Stage   Stage
	Ready   *Ready
	LetDown *LetDown
}

func NewStatus(params url.Values) (*Status, error) {
	stage, err := newStage(params.Get("stage"))
	if err != nil {
		return nil, err
	}
	switch stage {
	case StageReady:
		if ready, err := NewReady(params); err != nil {
			return nil, err
		} else {
			return &Status{Stage: stage, Ready: ready}, nil
		}
	case StageLetDown:
		if letDown, err := NewLetDown(params); err != nil {
			return nil, err
		} else {
			return &Status{Stage: stage, LetDown: letDown}, nil
		}
	default:
		return &Status{Stage: stage}, nil
	}
}

func (s *Status) Unsol(e *Event) []byte {
	bytes := []byte{
		e.Stall,                        // pos 0
		byte(s.Stage) | byte(e.Button), // pos 1
	}
	if s.Ready != nil {
		bytes = append(bytes, byte(s.Ready.Stop)) // pos 2
		if s.Ready.Stop != StopRestart {
			bytes = append(bytes, s.Ready.Min) // pos 3
			bytes = append(bytes, s.Ready.Sec) // pos 4
			// grams pending pos 5,6
		}
	} else if s.LetDown != nil {
		bytes = append(bytes, s.LetDown.Flags)         // pos 2
		bytes = append(bytes, byte(s.LetDown.Ohms))    // pos 3 ohms-lo
		bytes = append(bytes, byte(s.LetDown.Ohms>>8)) // pos 4 ohms-hi
		bytes = append(bytes, s.LetDown.Time)          // pos 5
	}
	return bytes
}*/

/*
type Ready struct {
	Stop Stop
	Min  byte
	Sec  byte
}

func NewReady(params url.Values) (*Ready, error) {
	stop, err := newStop(params.Get("stop"))
	if err != nil {
		return nil, err
	}
	ready := &Ready{Stop: stop}
	if m, e := strconv.Atoi(params.Get("min")); e != nil {
		return nil, errors.New("minutes wrong format")
	} else if m < 0 || m > 255 {
		return nil, errors.New("minutes out of range")
	} else {
		ready.Min = byte(m)
	}
	if s, e := strconv.Atoi(params.Get("sec")); e != nil {
		return nil, errors.New("seconds wrong format")
	} else if s < 0 || s > 59 {
		return nil, errors.New("seconds out of range")
	} else {
		ready.Sec = byte(s)
	}
	return ready, nil
}*/

/*
type LetDown struct {
	Flags byte
	Ohms  uint16
	Time  byte
}

func NewLetDown(params url.Values) (*LetDown, error) {
	array := []byte(params.Get("flags"))
	if len(array) != 4 {
		return nil, errors.New("flags invalid size")
	}
	letDown := &LetDown{}
	if array[0] == '1' {
		letDown.Flags |= LetDownQuickStart
	}
	if array[1] == '1' {
		letDown.Flags |= LetDownReattach
	}
	if array[2] == '1' {
		letDown.Flags |= LetDownSwingOver
	}
	if array[3] == '1' {
		letDown.Flags |= LetDownLimits
		if o, e := strconv.Atoi(params.Get("ohms")); e != nil {
			return nil, errors.New("ohms wrong format")
		} else if o < 0 || o > 0xFFFF {
			return nil, errors.New("ohms out of range")
		} else {
			letDown.Ohms = uint16(o)
		}
		if t, e := strconv.Atoi(params.Get("time")); e != nil {
			return nil, errors.New("time wrong format")
		} else if t < 0 || t > 0xFF {
			return nil, errors.New("time out of range")
		} else {
			letDown.Time = byte(t)
		}
	} else {
		letDown.Ohms = 900
		letDown.Time = 60
	}
	if letDown.Ohms < 100 {
		letDown.Ohms = 900
	}
	return letDown, nil
}*/

