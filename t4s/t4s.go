package t4s

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/t4"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/api"
)

var (
	ReadWriteMap lines.ReadWriteMap
	ReadOnlyMap  lines.ReadOnlyMap
)

func init() {
	ReadWriteMap = lines.ReadWriteMap(map[base.Get]*lines.ReadWrite{
		t4.Get03_Stalls:    lines.NewReadWrite(t4.Set09_Stalls, t4.Sol03_Stalls), // cfg 1
		t4.Get04_Equipment: lines.NewReadWrite(t4.Set10_Equipment, t4.Sol04_Equipment), // cfg 2
		t4.Get05_Milking:   lines.NewReadWrite(t4.Set11_Milking, t4.Sol05_Milking), // cfg 3
		t4.Get06_Profiles:  lines.NewReadWrite(t4.Set12_Profiles, t4.Sol06_Profiles), // cfg 4
		t4.Get07_Colors:    lines.NewReadWrite(t4.Set13_Colors, t4.Sol07_Colors), // cfg 5
		t4.Get08_Palette:   lines.NewReadWrite(t4.Set14_Pallete, t4.Sol08_Palette), // cfg 6
	})
	ReadOnlyMap = lines.ReadOnlyMap(map[base.Get]*lines.ReadOnly{
		base.Get00_Version: lines.NewReadOnly(base.Sol00_Version),
		base.Get01_Serial:  lines.NewReadOnly(base.Sol31_Serial),
		base.Get02_Status:  lines.NewReadOnly(base.Sol02_Status),
		t4.Get15_Levels:    lines.NewReadOnly(t4.Sol12_Levels),
	})
}

// Masks returns the solicited response and mask for given get/set command
func Masks(comm byte) (sol base.Sol, masks []byte, ok bool) {
	switch comm {
	default:
		return
	}
}

// CfgMasks returns the responses will arrive after sending a particular T4 request
func CfgMasks(req *api.Can) ([]*api.Can, bool) {
	comm := req.Comm
	switch comm {
	default:
		return nil, false
	}
}

type Button byte

const (
	Button1 Button = 0x01
	Button2        = 0x02
	Button3        = 0x03
	Button4        = 0x04
)

type Event struct {
	Stall  byte
	Button Button
}


const (
	LetDownQuickStart byte = 0x01
	LetDownReattach        = 0x02
	LetDownSwingOver       = 0x04
	LetDownLimits          = 0x08
)
