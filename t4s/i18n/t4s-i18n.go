package i18n

import (
	"gitlab.com/jmireles/nav"
)

const (
	BlinkSteady = "steady"
	BlinkSlow   = "slow"
	BlinkMedium = "medium"
	BlinkFast   = "fast"
)

const (
	ColorNone         = "none"
	ColorDarkGray     = "darkGray"
	ColorLightGray    = "lightGray"
	ColorWhite        = "white"
	ColorRed          = "red"
	ColorOrange       = "orange"
	ColorMagenta      = "magenta"
	ColorLightMagenta = "lightMagenta"
	ColorGreen        = "green"
	ColorGreenBlue    = "greenBlue"
	ColorGreenYellow  = "greenYellow"
	ColorYellow       = "yellow"
	ColorBlue         = "blue"
	ColorLightGreen   = "lightGreen"
	ColorPurple       = "purple"
	ColorCyan         = "cyan"
)

const (
	ModeManual = "manual"
	ModeAuto   = "auto"
)

const (
	StageReady      = "ready"
	StagePrep       = "prep"
	StagePrepEnd    = "prepEnd"
	StageAttach     = "attach"
	StageLetDown    = "letDown"
	StageMilking    = "milking"
	StageDetach     = "detach"
	StagePreWash    = "preWash"
	StageWash       = "wash"
	StageWashDetach = "washDetach"
)

const (
	StopNormal      = "normal"
	StopNoMilk      = "noMilk"
	StopUser        = "user"
	StopKickoff     = "kickoff"
	StopGate        = "gate"
	StopMaxTime     = "maxTime"
	StopSecondTurn  = "secondTurn"
	StopRestart     = "restart"
	StopMaxMilkTime = "maxMilkTime"
	StopForced      = "forced"
)

func Stage(val string, lang nav.Lang) string {
	if lang == nav.LangEs {
		switch val {
		case StageReady:
			return "Listo"
		case StagePrep:
			return "Preparación"
		case StagePrepEnd:
			return "Fin Preparación"
		case StageAttach:
			return "Colocación"
		case StageLetDown:
			return "Caída leche"
		case StageMilking:
			return "Ordeña"
		case StageDetach:
			return "Retiro"
		case StagePreWash:
			return "Pre-lavado"
		case StageWash:
			return "Lavado"
		case StageWashDetach:
			return "Retiro lavado"
		default:
			return "Etapa desconocida"
		}
	}
	switch val {
	case StageReady:
		return "Ready"
	case StagePrep:
		return "Preparation"
	case StagePrepEnd:
		return "Preparation end"
	case StageAttach:
		return "Attach"
	case StageLetDown:
		return "Let down"
	case StageMilking:
		return "Milking"
	case StageDetach:
		return "Detach"
	case StagePreWash:
		return "Pre wash"
	case StageWash:
		return "Wash"
	case StageWashDetach:
		return "Wash detach"
	default:
		return "Unknown stage"
	}
}

func Stop(val string, lang nav.Lang) string {
	if lang == nav.LangEs {
		switch val {
		case StopNormal:
			return "Normal"
		case StopNoMilk:
			return "Sin leche"
		case StopUser:
			return "Usuario"
		case StopKickoff:
			return "Patada"
		case StopGate:
			return "Puerta"
		case StopMaxTime:
			return "Max tiempo"
		case StopSecondTurn:
			return "Segunda vuelta"
		case StopRestart:
			return "Reinicio"
		case StopMaxMilkTime:
			return "Max tiempo ordeña"
		case StopForced:
			return "Forzado "
		default:
			return "Paro desconocido"
		}
	}
	switch val {
	case StopNormal:
		return "Normal"
	case StopNoMilk:
		return "No milk"
	case StopUser:
		return "User"
	case StopKickoff:
		return "Kickoff"
	case StopGate:
		return "Gate"
	case StopMaxTime:
		return "Max time"
	case StopSecondTurn:
		return "Second turn"
	case StopRestart:
		return "Restart"
	case StopMaxMilkTime:
		return "Max milk time"
	case StopForced:
		return "Forced"
	default:
		return "Unknown stop"
	}
}
