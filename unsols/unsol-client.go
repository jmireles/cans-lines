package unsols

import (
	"bufio"
	"net/http"

	"gitlab.com/jmireles/cans-base"
)

type ClientFunc	func(resp *http.Response, text string)

type Client struct {
	*http.Client
	baseURL string
	log     base.Log
}

func NewClient(client *http.Client, baseURL string, log base.Log) *Client {
	return &Client{ 
		Client:  client,
		baseURL: baseURL,
		log:     log,
	}
}

func (c *Client) GetSSE(path string, filter *Filter, clientFunc ClientFunc) error {
	if filter != nil {
		path += filter.QueryParams()
	}
	resp, err := c.Get(c.baseURL + path)
	if err != nil {
		return err
	}
	clientFunc(resp, "")
	defer resp.Body.Close()
	if c.log != nil {
		c.log("resp %v", resp)
	}
	sc := bufio.NewScanner(resp.Body)
	for sc.Scan() {
		text := sc.Text()
		if text != "" {
			clientFunc(nil, text)
			if c.log != nil {
				c.log("%v", text)
			}
		}
	}
	return nil
}

func ClientGetSSE(path string, filter *Filter, clientFunc ClientFunc) error {
	if filter != nil {
		path += filter.QueryParams()
	}
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	clientFunc(resp, "")
	defer resp.Body.Close()
	sc := bufio.NewScanner(resp.Body)
	for sc.Scan() {
		text := sc.Text()
		if text != "" {
			clientFunc(nil, text)
		}
	}
	return nil
}
