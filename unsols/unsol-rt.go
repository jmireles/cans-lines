package unsols

import(
	"sort"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/jmireles/cans-base/from"

	"gitlab.com/jmireles/cans-lines"
)

type Ref struct {
    filter *Filter
    sb     *strings.Builder
    keys   int
}

func NewRef(filter *Filter) *Ref {
    return &Ref{
        filter: filter,
        sb:     &strings.Builder{},
    }
}

// A Node forms a part of a tree to store unsolicited values.
// An implemented Node can have nodes as children.
type Node interface {
	// updateNode let a Node to be updated (set/update) with and unsol response Resp.
	// Node's update can call children updates passing the same response, since
	// each node decides what response's fields to use to set keys and values.
    updateNode(e *lines.Resp)
    // Json let a Node to be read. Keys can be filtered and Node's json can
    // call children json passing the same filter and buffer, since
    // each node decides what filter's fields to use to filter keys and values.
    jsonNode(ref *Ref)
}

// Data is the common data for every node. Children nodes are a Map with keys
// as integers. A sorted keys array is maintened to print Json responses consistent. 
type Data struct {
    nodes map[int]Node
    keys  []int
}

// NewData creates an empty node data map and array.
func NewData() *Data {
    return &Data{
        nodes: make(map[int]Node, 0),
        keys:  make([]int, 0),
    }
}

// update updates map's child with the given key. If the node is not found
// a new one is created using the given child function and the sorted keys array is updated.
// The found or created node is returned.
func (d *Data) update(key int, child func() Node) Node {
    node := d.nodes[key]
    if node == nil {
        node = child()
        d.nodes[key] = node
        d.keys = append(d.keys, key)
        sort.Ints(d.keys[:])
    }
    return node
}

// json returns a recursive json with keys always as integers and sorted and without spaces.
// nodes are called recursivelly until and end is found by not calling this method but
// printing another value.
func (d *Data) json(ref *Ref, pass func(k int) bool) int {
    ref.sb.WriteString(`{`)
    comma := ""
    keys := 0
    for _, key := range d.keys {
        if pass(key) {
            keys++
            ref.sb.WriteString(comma)
            if node := d.nodes[key]; node != nil {
                ref.sb.WriteString(strconv.Itoa(key))
                ref.sb.WriteString(":")
                node.jsonNode(ref)
                comma = ","
            }
        }
    }
    ref.sb.WriteString(`}`)
    return keys
}

type RT1 struct {
    respFunc lines.RespFunc
    filter   *Filter
}

func NewRT1(respFunc lines.RespFunc, filter *Filter) *RT1 {
    return &RT1 {
        respFunc: respFunc,
        filter:   filter,
    }
}

func (r *RT1) Json(can *from.Can) (s string) {
    if resp, err := lines.NewResp(can, r.respFunc); err != nil {
        return
    } else {
        s, _ = RTJson(r.filter, resp)
    }
    return
}

func RTJson(filter *Filter, resp *lines.Resp) (s string, ok bool) {
    data := NewData()
    if node := data.update(int(resp.Can.Net), func() Node {
        return NewLines()
    }); node != nil {
        node.updateNode(resp) // next node lines
    }
    ref := NewRef(filter)
    data.json(ref, func(i int) bool {
        return filter.PassNet(i)
    })
    if ref.keys != 0 {
        if s = ref.sb.String(); s != "" {
            ok = true
        }
    }
    return
}

// RT is first Node of the database where to set/update realtime unsol responses.
// The database is a tree formed of nodes.
// RT Node data key is the CAN net and the value is a map of Lines.
// RT's updateNode and Json methods are thread safe for the whole database.
type RT struct {
	*Data
	respFunc lines.RespFunc
    mu       sync.Mutex
}

func NewRT(respFunc lines.RespFunc) *RT {
	return &RT{ 
		Data:     NewData(),
		respFunc: respFunc,
	}
}

func (n *RT) Update(can *from.Can) {
    n.mu.Lock()
    defer n.mu.Unlock()
    if resp, err := lines.NewResp(can, n.respFunc); err == nil {
		n.updateNode(resp)	
	}
}

func (n RT) Json(filter *Filter) string {
    n.mu.Lock()
    defer n.mu.Unlock()
    ref := NewRef(filter)    
    n.jsonNode(ref)
    if ref.keys != 0 {
        return ref.sb.String()
    }
    return ""
}

func (n *RT) Clear() {
    n.mu.Lock()
    defer n.mu.Unlock()
    n.nodes = make(map[int]Node, 0)
    n.keys = make([]int, 0)
}

func (n RT) updateNode(resp *lines.Resp) {
    if node := n.update(int(resp.Can.Net), func() Node {
    	return NewLines()
    }); node != nil {
    	node.updateNode(resp) // next
    }
}

func (n RT) jsonNode(ref *Ref) {
    n.json(ref, func(i int) bool {
    	return ref.filter.PassNet(i)
    })
}


// Lines is the second Node of the realtime database.
// Each key is a CAN line reduced to a nibble, and the values are Boxes nodes.
type Lines struct {
	*Data
}

func NewLines() *Lines {
    return &Lines{ Data: NewData() }
}

func (n Lines) updateNode(resp *lines.Resp) {
    // reduce line from.From (XXXX0000) to to.To (0000XXXX) for storing
    key := int(resp.Can.Line) >> 4
    if node := n.update(key, func() Node {
    	return NewBoxes()
    }); node != nil {
    	node.updateNode(resp)
    }
}

func (n Lines) jsonNode(ref *Ref) {
	n.json(ref, func(key int) bool {
		// reverse reduction in update to filter original line
		return ref.filter.PassLine(key << 4)
	})
}

// Boxes is the third Node of the realtime database.
// Each key is a box number other than zero and the values are Keys nodes.
type Boxes struct {
	*Data
}

func NewBoxes() *Boxes {
    return &Boxes{ Data: NewData() }
}

func (n Boxes) updateNode(resp *lines.Resp) {
	key := int(resp.Can.Box)
    if node := n.update(key, func() Node {
    	return NewKeys()
    }); node != nil {
    	node.updateNode(resp)
    }
}

func (n Boxes) jsonNode(ref *Ref) {
	n.json(ref, func(key int) bool {
		return ref.filter.PassBox(key)
	})
}

// Keys are the fourth Node of the 
// Each key is a Key and Values are Value format
type Keys struct {
	*Data
}

func NewKeys() *Keys {
    return &Keys{ Data: NewData() }
}

func (n Keys) updateNode(resp *lines.Resp) {
	n.Update(resp.Map)
}

func (n Keys) jsonNode(ref *Ref) {
	ref.keys += n.json(ref, func(key int) bool {
		return ref.filter.PassKey(key)
	})
}

func (n Keys) Update(m lines.Map) {
    for k, val := range m {
        i := int(k)
        if node := n.update(i, func() Node {
        	return NewValues()
        }); node != nil {
        	(node.(*Values)).v = val
        }
    }
}

func (n Keys) Json() string {
    ref := NewRef(nil)
	n.jsonNode(ref)
	return ref.sb.String()
}

type Values struct {
    v lines.Value
}

func NewValues() *Values {
    return &Values{}
}

func (v Values) updateNode(resp *lines.Resp) {
	// this update is not used but
	// the (node.(*Values)).v = val int Keys.Update above
}

func (v Values) jsonNode(ref *Ref) {
    ref.sb.WriteString(v.v.String())
}








