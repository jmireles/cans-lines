package unsols

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/p1"
)

func TestUnsols(t *testing.T) {
	tt := base.Test{t}
	pm := from.P1
//	i4 := from.I4
	t4 := from.T4
	src, _ := from.NewSrc(2, pm, 7)
	baseRestart := base.Unsol01_Restart
	alarms, extra := p1.Unsol07_Alarmed, []byte{1,2}

	// NewSSE without extra
	restart, err := from.NewCanUnsol(src, baseRestart, nil)
	tt.Ok(err)
	sse, err := NewSSE(restart)
	tt.Ok(err)
	tt.Equals("2100701", string(sse))
	tt.Equals("2", sse.Net())
	tt.Equals("10", sse.Line())
	tt.Equals("07", sse.Box())
	tt.Equals("01", sse.Unsol())
	tt.Equals("", sse.Extra())

	// NewSSE with extra
	canAlarms, err := from.NewCanUnsol(src, alarms, extra)
	tt.Ok(err)
	sse, err = NewSSE(canAlarms)
	tt.Ok(err)
	tt.Equals("2100707.0102", string(sse))
	tt.Equals("2", sse.Net())
	tt.Equals("10", sse.Line())
	tt.Equals("07", sse.Box())
	tt.Equals("07", sse.Unsol())
	tt.Equals("0102", sse.Extra())

	// Filter: restart should not pass
	for _, filter := range []*Filter{
		&Filter{ Net:3 },
		&Filter{ Net:2, Line:&t4 },
		&Filter{ Net:2, Line:&pm, Box:8 },
		&Filter{ Net:2, Line:&pm, Box:7, Unsol:&alarms },
	} {
		tt.Assert(!filter.PassCAN(restart), "should not pass: filter:%v, can:%v", filter, restart)
	}

	// Filter: restart should pass
	for _, filter := range []*Filter{
		nil,
		&Filter{ }, // net=0, line=none, box=0, unsol=none
		&Filter{ Net:2 }, // net=2
		&Filter{ Line:&pm }, // line=pm
		&Filter{ Box:7 }, // box=7
		&Filter{ Unsol:&baseRestart }, // unsol=restart
		&Filter{ Net:2, Line:&pm, Box:7, Unsol:&baseRestart }, // max coincidence
	} {
		tt.Assert(filter.PassCAN(restart), "should pass: filter:%v, can:%v", filter, restart)
	}

	// Filter query
	for filter, query := range map[*Filter]string {
		&Filter{}:         "",
		&Filter{Net:2}:    "?net=2",
		&Filter{Net:3}:    "?net=3",
		&Filter{Line:&pm}: "?line=16",
		&Filter{Box:7}:    "?box=7",
		&Filter{Unsol:&baseRestart}:   "?unsol=1",
		&Filter{Unsol:&alarms}:        "?unsol=7",
		&Filter{Net:2,Line:&pm,Box:8}: "?net=2&line=16&box=8",
		&Filter{Net:2,Line:&t4}:       "?net=2&line=112",
		&Filter{Net:2,Line:&pm,Box:7,Unsol:&baseRestart}: "?net=2&line=16&box=7&unsol=1",
		&Filter{Net:2,Line:&pm,Box:7,Unsol:&alarms}:      "?net=2&line=16&box=7&unsol=7",
		&Filter{Format:FormatMap, Key:2}:    "?format=1&key=2",
	} {
		tt.Equals(filter.QueryParams(), query)
	}

	rt := NewRT(nil)
	n2pm7, _ := from.NewSrc(2, pm, 7)
	n2pm7restart, _ := from.NewCanUnsol(n2pm7, baseRestart, nil)

	rt.Update(n2pm7restart)
	tt.Equals("", rt.Json(nil)) // without UnsolFunc we get always ""
}
