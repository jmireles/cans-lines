package unsols

import (
    "fmt"
    "net/http"
    "sync"

    "gitlab.com/jmireles/cans-base/from"

    "gitlab.com/jmireles/cans-lines"
)

type Srv struct {
    clients   []chan *lines.Resp // change to *from.Can
    unsolFunc lines.RespFunc
    mu        sync.Mutex
}

func NewSrv() *Srv {
    return &Srv{
        clients: make([]chan *lines.Resp, 0),
    }
}

func (s *Srv) SetFunc(unsolFunc lines.RespFunc) {
    s.unsolFunc = unsolFunc
}

func (s *Srv) Fire(can *from.Can) (*lines.Resp, error) {
    if resp, err := lines.NewResp(can, s.unsolFunc); err != nil {
        return nil, err
    } else {
        s.fire(resp)
        return resp, nil
    }
}

func (s *Srv) EventStream(w http.ResponseWriter, r *http.Request) {
    filter, err := NewFilter(r.URL.Query())
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }
    flusher, ok := w.(http.Flusher)
    if !ok {
        http.Error(w, "Internal error", 500)
        return
    }
    //clientGone := w.(http.CloseNotifier).CloseNotify()
    w.Header().Set("Content-Type", "text/event-stream")
    w.Header().Set("Cache-Control", "no-cache")
    w.Header().Set("Connection", "keep-alive")
    client := s.addClient()
    defer s.removeClient(client)

    flusher.Flush() // report ASAP headers as new client added
    
    for {
        select {

        case resp := <-client:
            if sse, ok := filter.SrvEvent(resp); ok {
                fmt.Fprintf(w, "data: %s\n\n", sse)
                flusher.Flush()
            }

        case <-r.Context().Done():
            return

        //case <-clientGone:
        //    fmt.Printf("Client %v closed", r.RemoteAddr)
        //    return
        }
    }
}

func (s *Srv) addClient() chan *lines.Resp {
    s.mu.Lock()
    defer s.mu.Unlock()
    client := make(chan *lines.Resp)
    s.clients = append(s.clients, client)
    return client 
}

func (s *Srv) removeClient(resp chan *lines.Resp) {
    s.mu.Lock()
    defer s.mu.Unlock()
    close(resp)
    for i, r := range s.clients {
        if r == resp {
            s.clients = append(s.clients[:i], s.clients[i+1:]...)
        }
    }
}

// fire dispatch the given resp to server's current listeners.
func (s *Srv) fire(resp *lines.Resp) {
    s.mu.Lock()
    defer s.mu.Unlock()
    for _, client := range s.clients {
        client<- resp
    }
}

type SrvSSE struct {
    *Srv
    indexPath string
    streamPath string
}

func NewSrvSSE(indexPath, streamPath string) *SrvSSE {
    return &SrvSSE{
        Srv: NewSrv(),
        indexPath:   indexPath,
        streamPath:  streamPath,
    }
}

func (s *SrvSSE) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    switch r.URL.Path {
    case s.indexPath:
        s.index().ServeHTTP(w, r)

    case s.streamPath:
        s.EventStream(w, r)

    default:
        w.WriteHeader(400)
    }
}

// index returns a very simple page where unsolicited events
// are printed in a simple DOM element. The javascript script page creates 
// a source with new EventSource(path) and .onmessage transfer the value to DOM.
func (s *SrvSSE) index() http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        w.Write([]byte(`<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <div>Unsol: <span id="unsol"></span></div>
  <script>
    var source = new EventSource("` + s.streamPath + `");
    source.onmessage = function (event) {
        var unsol = JSON.parse(event.data);
        document.getElementById("unsol").innerHTML = unsol;
    }
  </script>
</body>
</html>`))
    })
}

// SSE is an unsol server sent event in string format than can be travel
// to web pages as browser server send event data. The format is:
//
//  NLBBCC.aabbccddeeffgghh
//
// where
//	N is the net in hex value (1-f)
//	LL is the line in hex.
//	BB is the the box in hex (0x00-0xff)
//	CC is the command in hex (0x00-0xff)
//	and aa,bb,cc,dd,ee,ff,gg,hh are optional eight extra bytes in hex format.
type SSE string

func NewSSE(can *from.Can) (sse SSE, err error) {
    if can == nil {
        err = fmt.Errorf("Can nil")
        return
    }
    if can.Unsol == nil {
        err = fmt.Errorf("Not unsol")
        return
    }
    s := fmt.Sprintf("%s%02x", can.Key(), *can.Unsol)
    if can.Extra != nil {
        // Using dot (.) because # is mangled in event-stream dispatch to browser
        s = fmt.Sprintf("%s.%02x", s, can.Extra)
    }
    sse = SSE(s)
    return
}

func (u SSE) Net() string {
    if len(u) >= 1 {
        return string(u)[:1]
    }
    return ""
}

func (u SSE) Line() string {
    if len(u) >= 3 {
        return string(u)[1:3]
    }
    return ""
}

func (u SSE) Box() string {
    if len(u) >= 5 {
        return string(u)[3:5]
    }
    return ""
}

func (u SSE) Unsol() string {
    if len(u) >= 7 {
        return string(u)[5:7]
    }
    return ""
}

func (u SSE) Extra() string {
    if len(u) >= 8 {
        return string(u)[8:]
    }
    return ""
}




