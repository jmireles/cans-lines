package unsols

import (
	"fmt"

	"gitlab.com/jmireles/cans-base/from"

	"gitlab.com/jmireles/cans-lines"
)

const (
	KeyStatus lines.Key = 0x01 // All
	KeyAlarm  lines.Key = 0x02 // PM
	KeyNotify lines.Key = 0x03 // PM
	
	KeyGate   lines.Key = 0x04 // RS
	KeyReader lines.Key = 0x05 // RS

	KeyZonesEnter        lines.Key = 0x10 // I4 zones 0-15 ->  16- 31 values: ValueClose, ValueOpen
	KeyZonesExit         lines.Key = 0x20 // I4 zones 0-15 ->  32- 47 values: ValueClose, ValueOpen
	KeyZonesPrewash      lines.Key = 0x30 // I4 zones 0-15 ->  48- 63 values: ValueOn, ValueOff
	KeyZonesWash         lines.Key = 0x40 // I4 zones 0-15 ->  64- 79 values: ValueOn, ValueOff
	KeyZonesVacuumPump   lines.Key = 0x50 // I4 zones 0-15 ->  80- 95 values: ValueOn, ValueOff
	KeyZonesSafetySwitch lines.Key = 0x60 // I4 zones 0-15 ->  96-111 values: ValueOn, ValueOff
	KeyZonesNewDay       lines.Key = 0x70 // I4 zones 0-15 -> 112-127 values: ValueOn, ValueNone
	KeyZonesNewLot       lines.Key = 0x80 // I4 zones 0-15 -> 128-133 values: ValueOn, ValueNone
	
	KeyZonesStall        lines.Key = 0x90 // I4 zones 0-15 -> 132-147 values: ValueFirst, ValueSecond
	KeySwing             lines.Key = 0xa0 // I4 values: 0 - 0xffff (smartlite/button)

	keyStallStage             lines.Key = 0x100 // T4 stalls 0-255 -> 0x100 - 0x1ff values: ValueStage
	keyStallStageReadyStop    lines.Key = 0x200 // T4 stalls 0-255 -> 0x200 - 0x2ff values: ValueStop
	keyStallStageReadySecs    lines.Key = 0x300 // T4 stalls 0-255 -> 0x300 - 0x3ff values: 0 - 59*60 + 60
	keyStallStageLetDownFlags lines.Key = 0x400
	keyStallStageLetDownOhms  lines.Key = 0x500
	keyStallStageLetDownTime  lines.Key = 0x600
	keyStallMode              lines.Key = 0x700
	keyStallLevelStage        lines.Key = 0x800
	keyStallLevelOhms         lines.Key = 0x900
	keyStallLevelTime         lines.Key = 0xa00
	keyStallLedsBlink         lines.Key = 0xb00
	keyStallLedsColor         lines.Key = 0xc00
)

const (
	ValueRestart      lines.Value = 1 // PM, RS, T4, RS
	ValueIdle         lines.Value = 2 // PM
	ValueWorking      lines.Value = 3 // PM
	ValueStimulation  lines.Value = 4 // PM
	ValueBaseAlarms   = uint64(0x10000) // PM
	ValueBaseNoPhaseF = uint64(0x20000) // PM
	ValueBaseNoPhaseR = uint64(0x40000) // PM

	ValueNone         lines.Value = 0 // I4
	ValueClose        lines.Value = 1 // I4
	ValueOpen         lines.Value = 2 // I4
	ValueOn           lines.Value = 3 // I4
	ValueOff          lines.Value = 4 // I4
	ValueFirst        lines.Value = 5 // I4
	ValueSecond       lines.Value = 6 // I4
)

type ValueStage lines.Value // for T4
const (
	ValueStageReady ValueStage = 0
	ValueStagePrep       = 1
	ValueStagePrepEnd    = 2
	ValueStageAttach     = 3
	ValueStageLetDown    = 4
	ValueStageMilking    = 5
	ValueStageDetach     = 6
	ValueStagePreWash    = 7
	ValueStageWash       = 8
	ValueStageWashDetach = 9
)

type ValueStop  lines.Value // for T4
const (
	ValueStopNormal ValueStop = 0
	ValueStopNoMilk      = 1
	ValueStopUser        = 2
	ValueStopKickoff     = 3
	ValueStopGate        = 4
	ValueStopMaxTime     = 5
	ValueStopSecondTurn  = 6
	ValueStopRestart     = 7
	ValueStopMaxMilkTime = 8
	ValueStopForced      = 9
)

type ValueMode lines.Value // for T4
const (
	ValueModeManual ValueMode = 0
	ValueModeAuto = 1
)

type ValueBlink lines.Value // for T4
const (
	ValueBlinkSteady ValueBlink = 0
	ValueBlinkSlow   = 1
	ValueBlinkMedium = 2
	ValueBlinkFast   = 3
)

type ValueColor lines.Value
const (
	ValueColorNone ValueColor = 0
	ValueColorDarkGray     = 1
	ValueColorLightGray    = 2
	ValueColorWhite        = 3
	ValueColorRed          = 4
	ValueColorOrange       = 5
	ValueColorMagenta      = 6
	ValueColorLightMagenta = 7
	ValueColorGreen        = 8
	ValueColorGreenBlue    = 9
	ValueColorGreenYellow  = 10
	ValueColorYellow       = 11
	ValueColorBlue         = 12
	ValueColorLightGreen   = 13
	ValueColorPurple       = 14
	ValueColorCyan         = 15
)


type StallMap struct {
	*lines.Map
	stall lines.Key
}

func (m StallMap) set(keyBase lines.Key, value lines.Value) {
	(*m.Map)[keyBase + m.stall] = value
}

func NewStallMap(stall byte) *StallMap {
	return &StallMap{
		Map:   &lines.Map{},
		stall: lines.Key(stall),
	}
}

// Stage sets map stage value iff is not nil
func (m StallMap) Stage(stage *ValueStage) {
	if stage != nil {
		m.set(keyStallStage, lines.Value(*stage))
	}
}

// Ready sets given values stop and seconds in map iff values are not nil
func (m StallMap) Ready(stop *ValueStop, seconds *int) {
	if stop != nil {
		m.set(keyStallStageReadyStop, lines.Value(*stop))
	}
	if seconds != nil {
		m.set(keyStallStageReadySecs, lines.Value(*seconds))
	}
}

// LetDown set given values flags, ohms and time in map iff values are not nil
func (m StallMap) LetDown(flags *byte, ohms *uint16, time *byte) {
	if flags != nil {
		/*
		quickstart := flags | 0x01 != 0
		reattach   := flags | 0x02 != 0
		swingOver  := flags | 0x04 != 0
		limits     := flags | 0x08 != 0
		if limits == false {
			ohms = 900
			time = 60
		}
		*/
		m.set(keyStallStageLetDownFlags, lines.Value(*flags))
	}
	if ohms != nil {
		m.set(keyStallStageLetDownOhms, lines.Value(*ohms))
	}
	if time != nil {
		m.set(keyStallStageLetDownTime, lines.Value(*time))
	}
}

// Mode set given mode value iff value is not nil
func (m StallMap) Mode(mode *ValueMode) {
	if mode != nil {
		m.set(keyStallMode, lines.Value(*mode))
	}
}

// Level set given values stage, ohms and time iff values are not nil
func (m StallMap) Level(stage *ValueStage, ohms *uint16, time *uint16) {
	if stage != nil {
		m.set(keyStallLevelStage, lines.Value(*stage))
	}
	if ohms != nil {
		m.set(keyStallLevelOhms, lines.Value(*ohms))
	}
	if time != nil {
		m.set(keyStallLevelTime, lines.Value(*time))
	}
}

func (m StallMap) Leds(blink *ValueBlink, color *ValueColor) {
	if blink != nil {
		m.set(keyStallLedsBlink, lines.Value(*blink))
	}
	if color != nil {
		m.set(keyStallLedsColor, lines.Value(*color))
	}
}




var (
	// fixed unsol maps single status key 
	MapRestart     = lines.Map{ KeyStatus: ValueRestart }
	MapIdle        = lines.Map{ KeyStatus: ValueIdle }
	MapWorking     = lines.Map{ KeyStatus: ValueWorking }
	MapStimulation = lines.Map{ KeyStatus: ValueStimulation }
	MapAlarmFront  = lines.Map{ KeyStatus: lines.Value(ValueBaseNoPhaseF) }
	MapAlarmRear   = lines.Map{ KeyStatus: lines.Value(ValueBaseNoPhaseR) }
)

var (
	ErrorInvalidAlarms = fmt.Errorf("Invalid alarms")
	ErrorInvalidType   = fmt.Errorf("Invalid type")
	ErrorInvalidUnsol  = fmt.Errorf("Invalid unsol")
	ErrorInvalidZone   = fmt.Errorf("Invalid zone")
	ErrorInvalidStall  = fmt.Errorf("Invalid stall")
)

func Func(req *from.Can) (*lines.Resp, error) {
	if req == nil || req.Unsol == nil {
		return nil, ErrorInvalidUnsol
	}
	return &lines.Resp{
		Can: req,
	}, nil
}

func MapJson(m lines.Map) string {
	keys := NewKeys()
	keys.Update(m)
	return keys.Json()
}
