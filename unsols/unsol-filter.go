package unsols

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"

	"gitlab.com/jmireles/cans-lines"
)

type Format int
const (
	FormatCAN Format = 0
	FormatMap Format = 1
)

// Filter is used to filter unsol events
// Can be
type Filter struct {
    Net    byte
    Line   *from.From
    Box    byte
    Unsol  *base.Unsol
    Key    lines.Key
    Format Format
}

func NewFilter(query url.Values) (*Filter, error) {
	f := &Filter{}
	if p := query.Get("net"); p != "" {
		if i, err := strconv.Atoi(p); err != nil || i < 0 || i > 15 {
			return nil, fmt.Errorf("Invalid net")
		} else {
			f.Net = byte(i)
		}
	}
	if p := query.Get("line"); p != "" {
		if i, err := strconv.Atoi(p); err != nil {
			return nil, fmt.Errorf("Invalid line")
		} else if line, err := from.NewFrom(byte(i)); err != nil {
			return nil, fmt.Errorf("Invalid line")
		} else {
			f.Line = &line
		}
	}
	if p := query.Get("box"); p != "" {
		if i, err := strconv.Atoi(p); err != nil || i < 0 || i > 255 {
			return nil, fmt.Errorf("Invalid box")
		} else {
			f.Box = byte(i)
		}
	}
	switch query.Get("format") {
	case "1":
		f.Format = FormatMap
		if p := query.Get("key"); p != "" {
			if i, err := strconv.Atoi(p); err != nil || i < 0 {
				return nil, fmt.Errorf("Invalid key")
			} else {
				f.Key = lines.Key(i)
			}
		}
	default:
		f.Format = FormatCAN
		if p := query.Get("unsol"); p != "" {
			if i, err := strconv.Atoi(p); err != nil || i < 0 || i > 255 {
				return nil, fmt.Errorf("Invalid unsol")
			} else {
				unsol := base.Unsol(i)
				f.Unsol = &unsol
			}
		}
	}
	return f, nil
}

// QueryParams returns the params for a HTTP request to send this Filter.
// The string returned format is like: "?net=1&line=16&box=7".
func (f *Filter) QueryParams() (s string) {
	q := make([]string, 0)
	if f.Net > 0 {
		q = append(q, fmt.Sprintf("net=%d", int(f.Net)))
	}
	if f.Line != nil {
		q = append(q, fmt.Sprintf("line=%d", int(*f.Line)))
	}
	if f.Box > 0 {
		q = append(q, fmt.Sprintf("box=%d", int(f.Box)))
	}
	if f.Format > 0 {
		q = append(q, fmt.Sprintf("format=%d", int(f.Format)))
	}
	switch f.Format {
	case FormatMap:
		if f.Key > 0 {
			q = append(q, fmt.Sprintf("key=%d", int(f.Key)))
		}
	default: // FormatCAN
		if f.Unsol != nil {
			q = append(q, fmt.Sprintf("unsol=%d", int(*f.Unsol)))
		}
	}

	if len(q) > 0 {
		s = fmt.Sprintf("?%s", strings.Join(q, "&"))
	}
	return
}

func (f *Filter) SrvEvent(resp *lines.Resp) (s string, ok bool) {
	if f.PassCAN(resp.Can) {
		switch f.Format {
	    
	    case FormatMap:
	    	return RTJson(f, resp)
	        
	    default: // FilterCAN
	    	//fmt.Println("FormatCAN")
	        if sse, err := NewSSE(resp.Can); err == nil {
	        	s = string(sse)	
	        	ok = true
	        }
	    }
	}
	return
}



// PassCAN checks the filter matches with can unsol fields, returns:
//  - false when can is nil or has no Unsol command.
//  - false when field's Net other than zero doesn't match can's net.
//  - false when field's Line other nil doesn't match can's line.
//  - false when field's Box other than zero doesn't match can's box.
//  - false when field's Unsol other than nil doesn't match can's unsol command.
//  - true otherwise, including this filter is nil.
func (f *Filter) PassCAN(can *from.Can) bool {
    if can == nil {
        return false
    }
    if can.Unsol == nil {
        return false // can is not unsol type
    }
    if f != nil {
        if f.Net != 0 && f.Net != byte(can.Net) {
            return false // nets don't match
        }
        if f.Line != nil && *f.Line != can.Line {
            return false // lines don't match
        }   
        if f.Box != 0 && f.Box != can.Box {
            return false // boxes don't match 
        }
        if f.Unsol != nil && *f.Unsol != *can.Unsol {
            return false // unsol commands don't match
        }
    }
    return true
}

func (f *Filter) PassNet(net int) bool {
    if f != nil && f.Net != 0 && int(f.Net) != net {
        return false // nets don't match
    }
    return true
}

func (f *Filter) PassLine(line int) bool {
	if f != nil && f.Line != nil && int(*f.Line) != line {
		return false
	}
	return true
}

func (f *Filter) PassBox(box int) bool {
	if f != nil && f.Box != 0 && int(f.Box) != box {
		return false
	}
	return true
}

func (f *Filter) PassKey(key int) bool {
	if f != nil && f.Key != 0 && int(f.Key) != key {
		return false
	}
	return true
}
