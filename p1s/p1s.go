package p1s

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/p1"
	"gitlab.com/jmireles/cans-base/to"
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/p1s/cfgs"
)

var (
	ReadWriteMap lines.ReadWriteMap
	ReadOnlyMap  lines.ReadOnlyMap
)

func init() {
	ReadWriteMap = lines.ReadWriteMap(map[base.Get]*lines.ReadWrite{
		p1.Get03_Config:   lines.NewReadWrite(p1.Set04_Config, p1.Sol03_Config), // cfg 1
		p1.Get07_Calibr:   lines.NewReadWrite(p1.Set08_Calibr, p1.Sol18_Calibr), // cfg 2
		p1.Get09_Options:  lines.NewReadWrite(p1.Set10_Options, p1.Sol21_Options), // cfg 3
		p1.Get11_Profiles: lines.NewReadWrite(p1.Set12_Profiles, p1.Sol22_Profiles), // cfg 4
		p1.Get13_Display:  lines.NewReadWrite(p1.Set14_Display, p1.Sol23_Display), // cfg 5
		p1.Get15_Config:   lines.NewReadWrite(p1.Set16_Config, p1.Sol24_Config), // cfg 6
		p1.Get17_Alarms:   lines.NewReadWrite(p1.Set18_Alarms, p1.Sol25_Alarms), // cfg 7
		p1.Get19_Stalls:   lines.NewReadWrite(p1.Set20_Stalls, p1.Sol27_Stalls), // cfg 8
	})
	ReadOnlyMap = lines.ReadOnlyMap(map[base.Get]*lines.ReadOnly{
		base.Get00_Version: lines.NewReadOnly(base.Sol00_Version),
		base.Get01_Serial:  lines.NewReadOnly(base.Sol31_Serial),
		base.Get02_Status:  lines.NewReadOnly(base.Sol02_Status),
		p1.Get05_Values:    lines.NewReadOnly(
			p1.Sol04_ValCurves,
			p1.Sol12_ValPhasesFront,
			p1.Sol13_ValPhasesRear,
			p1.Sol14_ValLevelsFront,
			p1.Sol15_ValLevelsRear,
		),
		p1.Get06_Averages: lines.NewReadOnly(
			p1.Sol08_AvgPhaseFront,
			p1.Sol09_AvgPhaseRear,
			p1.Sol10_AvgLevelFront,
			p1.Sol11_AvgLevelRear,
		),
	})
}

// Sols returns the expected solicited responses for given get/set commands
func Sols(sols *from.Sols) error {
	switch sols.Comm {
	case byte(p1.Get03_Config), byte(p1.Set04_Config):
		sols.Add(p1.Sol03_Config, [][]byte{ 
			[]byte { 1 },
			[]byte { 2 },
			[]byte { 3 },
			[]byte { 4 },
		})
		return nil
	default:
		return fmt.Errorf("p1s: No sols for comm:%d", sols.Comm)
	}
}


var Cfgs *cfg.Line

func cfgInit(opts *cfg.I18nOpts, units *cfg.I18nUnits) {
	p := byte(to.P1)
	n, err := to.P1.Name()
	if err != nil {
		panic(err)
	}
	newComms := func(get base.Get, set base.Set, sol base.Sol, profiles []int) *cfg.Comms {
		return cfg.NewComms(p, n, byte(get), byte(set), byte(sol), profiles)
	}

	comms03 := newComms(p1.Get03_Config, p1.Set04_Config, p1.Sol03_Config, nil)
	//	cfg.NewComms(Get07_Calibr,   Set08_Calibr,   Resp18_Calibr),
	comms09 := newComms(p1.Get09_Options, p1.Set10_Options, p1.Sol21_Options, nil)
	comms11 := newComms(p1.Get11_Profiles, p1.Set12_Profiles, p1.Sol22_Profiles, []int{5})
	comms13 := newComms(p1.Get13_Display, p1.Set14_Display, p1.Sol23_Display, nil)
	comms15 := newComms(p1.Get15_Config, p1.Set16_Config, p1.Sol24_Config, nil)
	comms17 := newComms(p1.Get17_Alarms, p1.Set18_Alarms, p1.Sol25_Alarms, nil)
	comms19 := newComms(p1.Get19_Stalls, p1.Set20_Stalls, p1.Sol27_Stalls, nil)

	Cfgs = cfg.NewLine([]*cfg.Cfg{
		cfg.NewCfg(comms03, cfgs.New03(opts, units), &i18n.Cfg03),
		//nil,
		cfg.NewCfg(comms09, cfgs.New09(opts, units), &i18n.Cfg09),
		cfg.NewCfg(comms11, cfgs.New11(opts, units), &i18n.Cfg11),
		cfg.NewCfg(comms13, cfgs.New13(opts, units), &i18n.Cfg13),
		cfg.NewCfg(comms15, cfgs.New15(opts, units), &i18n.Cfg15),
		cfg.NewCfg(comms17, cfgs.New17(opts, units), &i18n.Cfg17),
		cfg.NewCfg(comms19, cfgs.New19(opts, units), &i18n.Cfg19),
	})
}

