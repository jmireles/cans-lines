package p1s

import (
	_ "embed"
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/cfg"
)

//go:embed templates/p1-i18n.yaml
var i18nYaml []byte
var i18n i18N

func init() {
	if err := nav.TemplateYamlUnmarshal(i18nYaml, &i18n); err != nil {
		panic(err)
	}
	cfgInit(&i18n.Opts, &i18n.Units)
	i18n.Vals.units(&i18n.Units)
}

type i18N struct {
	Units cfg.I18nUnits `yaml:"units"`
	Vals  i18nVal       `yaml:"vals"`
	Opts  cfg.I18nOpts  `yaml:"opts"`
	Cfg03 cfg.I18n      `yaml:"cfg03"` // old-config
	Cfg07 cfg.I18n      `yaml:"cfg07"` // calibr
	Cfg09 cfg.I18n      `yaml:"cfg09"` // options
	Cfg11 cfg.I18n      `yaml:"cfg11"` // profiles
	Cfg13 cfg.I18n      `yaml:"cfg13"` // display
	Cfg15 cfg.I18n      `yaml:"cfg15"` // new-config
	Cfg17 cfg.I18n      `yaml:"cfg17"` // alarms
	Cfg19 cfg.I18n      `yaml:"cfg19"` // stalls
}

func I18nCfg(command byte, lang nav.Lang) string {
	switch command {
	case 3:
		return i18n.Cfg03.Id.Get(lang)
	case 7:
		return i18n.Cfg07.Id.Get(lang)
	case 9:
		return i18n.Cfg09.Id.Get(lang)
	case 11:
		return i18n.Cfg11.Id.Get(lang)
	case 13:
		return i18n.Cfg13.Id.Get(lang)
	case 15:
		return i18n.Cfg15.Id.Get(lang)
	case 17:
		return i18n.Cfg17.Id.Get(lang)
	case 19:
		return i18n.Cfg19.Id.Get(lang)
	default:
		return "[" + strconv.Itoa(int(command)) + "]"
	}
}

type i18nVal struct {
	A   nav.I18Ns `yaml:"a"`
	B   nav.I18Ns `yaml:"b"`
	C   nav.I18Ns `yaml:"c"`
	D   nav.I18Ns `yaml:"d"`
	VH  nav.I18Ns `yaml:"vh"`
	VL  nav.I18Ns `yaml:"vl"`
	PPM nav.I18Ns `yaml:"ppm"`
	F   nav.I18Ns `yaml:"f"`
	R   nav.I18Ns `yaml:"r"`

	ms     *cfg.I18nUnit
	vacuum *cfg.I18nUnit
	perMin *cfg.I18nUnit
}

func (i *i18nVal) units(u *cfg.I18nUnits) {
	i.ms = u.Get("milliseconds")
	i.vacuum = u.Get("vacuum")
	i.perMin = u.Get("perMinute")
}

func (i *i18nVal) getLabel(val string, lang nav.Lang) *cfg.Label {
	switch val {
	case "A":
		return cfg.NewLabel(val, i.A.Get(lang))
	case "B":
		return cfg.NewLabel(val, i.B.Get(lang))
	case "C":
		return cfg.NewLabel(val, i.C.Get(lang))
	case "D":
		return cfg.NewLabel(val, i.D.Get(lang))
	case "VH":
		return cfg.NewLabel(val, i.VH.Get(lang))
	case "VL":
		return cfg.NewLabel(val, i.VL.Get(lang))
	case "ppm":
		return cfg.NewLabel(val, i.PPM.Get(lang))
	default:
		return cfg.NewLabel("", "")
	}
}

// Return four groups of labels
// 1) seven upper columns: phases A-D, vacuums H and L and ppm
// 2) Single left F/Front for first row of values
// 3) Single left R/Rear for second rows of values
// 4) Seven lower columns with units: four milliseconds, two vacuums and one perMinute
func I18nVals(prefs *nav.Prefs) (v []*cfg.Label, f, r *cfg.Label, u []*cfg.Label) {
	i, lang := i18n.Vals, prefs.Lang
	v = []*cfg.Label{
		cfg.NewLabel("A", i.A.Get(lang)),     // col1
		cfg.NewLabel("B", i.B.Get(lang)),     // col2
		cfg.NewLabel("C", i.C.Get(lang)),     // col3
		cfg.NewLabel("D", i.D.Get(lang)),     // col4
		cfg.NewLabel("VH", i.VH.Get(lang)),   // col5
		cfg.NewLabel("VL", i.VL.Get(lang)),   // col6
		cfg.NewLabel("ppm", i.PPM.Get(lang)), // col7
	}
	f = cfg.NewLabel("F", i.F.Get(lang)) // row1
	r = cfg.NewLabel("R", i.R.Get(lang)) // row2
	ms := i.ms.Get(prefs)
	vacuum := i.vacuum.Get(prefs)
	perMin := i.perMin.Get(prefs)
	u = []*cfg.Label{
		ms, ms, ms, ms, // col1-4
		vacuum, vacuum, // col5-6
		perMin, // col7
	}
	return
}
