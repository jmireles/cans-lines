package p1s

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/p1"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/unsols"
)

// UnsolsFunc implements lines.RespFunc
func UnsolsFunc(req *from.Can) (resp *lines.Resp, err error) {

	resp, err = unsols.Func(req)
	if err != nil {
		return
	}
	switch *req.Unsol {

	case base.Unsol01_Restart:
		// map with single status key with value exactly 1
		resp.Map = unsols.MapRestart
		return

	case p1.Unsol05_Idle:
		// map with single status key with value exactly 2
		resp.Map = unsols.MapIdle
		return
	
	case p1.Unsol06_Working: 
		// map with single status key with value exactly 3
		resp.Map = unsols.MapWorking
		return

	case p1.Unsol07_Alarmed: 
		extra := resp.Can.Extra
		if len(extra) < 2 {
			return nil, unsols.ErrorInvalidAlarms
		}
		// map with single status key with status value >= 0x30000
		resp.Map = lines.Map { 
			unsols.KeyStatus: lines.NewValue(unsols.ValueBaseAlarms, extra[:2]),
		}

	case p1.Unsol16_NoPhaseFront: 
		// map with single status key with value exactly 0x10000
		resp.Map = unsols.MapAlarmFront

	case p1.Unsol17_NoPhaseRear: 
		// map with single status key with value exactly 0x20000
		resp.Map = unsols.MapAlarmRear

	case p1.Unsol20_Stimulation:
		// map with single status key with value exactly 4
		resp.Map = unsols.MapStimulation
	
	case p1.Unsol26_Notify:
		// map with single notify key with value variable
		extra := resp.Can.Extra
		if an, err := NewAlarmsNotify(extra); err != nil {
			return nil, err
		} else {
			base := unsols.ValueBaseAlarms
			if an.NoPhaseF {
				base |= unsols.ValueBaseNoPhaseF
			}
			if an.NoPhaseR {
				base |= unsols.ValueBaseNoPhaseR
			}
			resp.Map = lines.Map {
				unsols.KeyAlarm: lines.NewValue(base, an.Bits),
				unsols.KeyNotify: lines.NewValue(0, []byte{
					byte(an.Dst),
					byte(an.Type),
					an.Value,
				}),
			}
		}

	default:
		err = unsols.ErrorInvalidUnsol
	}
	return
}
