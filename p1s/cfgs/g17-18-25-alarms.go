package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
)

func New17(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		minutes *cfg.I18nUnit
	}{
		minutes: units.Get("minutes"),
	}
	// numbers
	milks := cfg.Number(0, 255, nil, nil)
	time := cfg.Number(0, 255, u.minutes, nil)

	gA := "phone"
	A1 := cfg.Field("pMilks", milks, cfg.Inits{"0"})
	A2 := cfg.Field("pTime", time, cfg.Inits{"0"})

	gB := "audio"
	B1 := cfg.Field("aMilks", milks, cfg.Inits{"0"})
	B2 := cfg.Field("aTime", time, cfg.Inits{"0"})

	// single message in 4 bytes, four parameters p0-p3
	mess1 := cfg.Fields{
		A1, // p0 b0    tx[0]      cMilks
		A2, // p1 b1    tx[1]      cTime
		B1, // p2 b2    tx[2]      cMilks
		B2, // p3 b3    tx[3]      cTime
		//    b4:b7 tx[4]tx[7] -- -- -- -- unused
	}
	rx1 := func(p *cfg.Params, b []byte) {
		A1.Rx1(p, b[0])
		A2.Rx1(p, b[1])
		B1.Rx1(p, b[2])
		B2.Rx1(p, b[3])
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = A1.Tx1(g)
		tx[1] = A2.Tx1(g)
		tx[2] = B1.Tx1(g)
		tx[3] = B2.Tx1(g)
		return tx
	}
	g := []string{"", gA, gB}
	f := map[string]cfg.Fields{
		"": nil, // no profiles
		gA: cfg.Fields{A1, A2},
		gB: cfg.Fields{B1, B2},
	}
	sim := []cfg.Fields{mess1}
	debug := ""
	m := lines.NewMess1(rx1, tx1, nil, debug)

	return cfg.NewGFMS(g, f, m, sim)
}
