package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
)

func New19(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// numbers
	zone := cfg.Number(0, 10, nil, nil)

	gA := "exitGate"
	A1 := cfg.Field("eZone", zone, cfg.Inits{"0"})

	// single message in 1 byte, single parameter p0
	mess1 := cfg.Fields{
		A1, // p0 b0    tx[0] zZone
	}
	rx1 := func(p *cfg.Params, b []byte) {
		A1.Rx1(p, b[0])
		// b[1]-b[7] unused
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = A1.Tx1(g)
		// tx[1-7] unused
		return tx
	}
	g := []string{"", gA}
	f := map[string]cfg.Fields{
		"": nil,
		gA: cfg.Fields{A1},
	}
	debug := ""
	sim := []cfg.Fields{mess1}
	m := lines.NewMess1(rx1, tx1, nil, debug)

	return cfg.NewGFMS(g, f, m, sim)
}
