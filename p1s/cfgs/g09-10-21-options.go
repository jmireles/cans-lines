package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
)

func New09(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		profile *cfg.I18nUnit
		seconds *cfg.I18nUnit
	}{
		profile: units.Get("profile"),
		seconds: units.Get("seconds"),
	}
	// options
	noYes := cfg.Options(
		[]string{"no", "yes"},
		[]byte{0, 1},
		0x01, // mask lowest bit
		opts.Get("noYes"),
		nil, // no units
	)
	unitO := cfg.Options(
		[]string{"kPa", "``Hg"},
		[]byte{0, 1},
		0x01, // mask lowest bit
		nil,  // don't use i18n
		nil,  // no units
	)
	profileUp := cfg.Options(
		[]string{"1", "2", "3", "4", "5"},
		[]byte{0x10, 0x20, 0x30, 0x40, 0x50},
		0xF0, // mask upper nibble
		nil,  // no i18n, use 1,2,3,4,5 as numbers
		u.profile,
	)
	profileDown := cfg.Options(
		[]string{"0", "1", "2", "3", "4", "5"},
		[]byte{0x00, 0x01, 0x02, 0x03, 0x04, 0x05},
		0x0F, // mask lower nibble
		nil,  // no i18n, use 0,1,2,3,4,5 as numbers
		u.profile,
	)
	modeUp := cfg.Options(
		[]string{"always", "firstMilk", "userButton"},
		[]byte{0x10, 0x20, 0x30},
		0xF0, // mask upper nibble
		opts.Get("modeUp9"),
		nil, // no units
	)
	// numbers
	preDelay := cfg.Number(0, 180, u.seconds, nil)
	duration := cfg.Number(0, 30, u.seconds, nil)

	// groups & fields
	gA := "options"
	A1 := cfg.Field("oPWM", noYes, cfg.Inits{"no"})
	A2 := cfg.Field("oRemLamp", noYes, cfg.Inits{"no"})
	A3 := cfg.Field("oVarPuls", noYes, cfg.Inits{"no"})
	A4 := cfg.Field("oDemoM", noYes, cfg.Inits{"no"})
	A5 := cfg.Field("oUnits", unitO, cfg.Inits{"kPa"})
	A6 := cfg.Field("oExitG", noYes, cfg.Inits{"no"})

	gB := "profiles"
	B1 := cfg.Field("pMon", profileUp, cfg.Inits{"1"})
	B2 := cfg.Field("pWash", profileDown, cfg.Inits{"0"})

	gC := "stimulation"
	C1 := cfg.Field("sProf", profileDown, cfg.Inits{"0"})
	C2 := cfg.Field("sMode", modeUp, cfg.Inits{"always"})
	C3 := cfg.Field("sPreD", preDelay, cfg.Inits{"0"})
	C4 := cfg.Field("sDur", duration, cfg.Inits{"0"})

	// single message in 5 bytes
	mess1 := cfg.Fields{
		A1, // p00 b0.00000001 tx[0] oPWM
		A2, // p01 b0.00000010 tx[0] oRemLamp
		A3, // p02 b0.00000100 tx[0] oVarPuls
		A4, // p03 b0.00001000 tx[0] oDemoM
		A5, // p04 b0.00010000 tx[0] oUnits
		A6, // p05 b0.00100000 tx[0] oExitG
		B1, // p06 b1.11110000 tx[1] pMon
		B2, // p07 b1.00001111 tx[1] pWash
		C1, // p08 b2.00001111 tx[2] sProf
		C2, // p09 b2.11110000 tx[2] sMode
		C3, // p10 b3          tx[3] sPreD
		C4, // p11 b4          tx[4] sDur
	}
	rx1 := func(p *cfg.Params, b []byte) {
		A1.Rx1(p, b[0]>>0)
		A2.Rx1(p, b[0]>>1)
		A3.Rx1(p, b[0]>>2)
		A4.Rx1(p, b[0]>>3)
		A5.Rx1(p, b[0]>>4)
		A6.Rx1(p, b[0]>>5)

		B1.Rx1(p, b[1])
		B2.Rx1(p, b[1])

		C1.Rx1(p, b[2])
		C2.Rx1(p, b[2])
		C3.Rx1(p, b[3])
		C4.Rx1(p, b[4])
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = (A1.Tx1(g) << 0) |
			(A2.Tx1(g) << 1) |
			(A3.Tx1(g) << 2) |
			(A4.Tx1(g) << 3) |
			(A5.Tx1(g) << 4) |
			(A6.Tx1(g) << 5)

		tx[1] = B1.Tx1(g) | B2.Tx1(g)

		tx[2] = C1.Tx1(g) | C2.Tx1(g)
		tx[3] = C3.Tx1(g)
		tx[4] = C4.Tx1(g)
		return tx
	}
	g := []string{"", gA, gB, gC}
	f := map[string]cfg.Fields{
		"": nil, // no profiles
		gA: cfg.Fields{A1, A2, A3, A4, A5, A6},
		gB: cfg.Fields{B1, B2},
		gC: cfg.Fields{C1, C2, C3, C4},
	}
	sim := []cfg.Fields{mess1}
	debug := ""
	m1 := lines.NewMess1(rx1, tx1, nil, debug)

	return cfg.NewGFMS(g, f, m1, sim)
}

/*
 * PmOptions single CAN message (5 bytes)
 * BYTE
 * ====
 *  0 : bit 0 : PWM
 *      bit 1 : RemoteLamp
 *      bit 2 : VariablePulsation
 *      bit 3 : DemoMode
 *      bit 4 : Units (0=kPa, 1=Hg) DON'T SHOW?
 *      bit 5 : Exit gate
 *  1 : Monitor/Wash profile  (0-4)  profiles = 1->5
 *      +---------+-------+
 *      | monitor | wash  |
 *      +---------+-------+
 *
 *  2 : Stimulation profile/mode (0-4)
 *      +------+---------+
 *      | mode | profile |
 *      +------+---------+
 *      mode = 0 fix time stimulation
        mode = 1 first milk stimulation
        mode = 2 user button
 *  3 : Stimulation delay       (0- 30) seconds
 *  4 : Stimulation duration    (0-180) seconds
 *
*/

/*
class PmOptions implements Line.Config {
	private void put(byte[] b, JsonO g, JsonO options, JsonO monitor, JsonO wash, JsonO stimulation) {
		//  0, 1, 2, 3, 4, 5, 6 = data
		L.putBits        (g, b, 0, OPTIONS);                          // A, B, C, D, L
		boolean units = (Bytes.b1(b, 0) & 0x10) == 0;
		if (units) { Limits.Pressure.kpa .put(options, F.Pressure); } // E
		else       { Limits.Pressure.hg  .put(options, F.Pressure); } // E
		F.Profile .a.putOption(b, 1, monitor, UpProfile.A);            // F
		F.Profile .a.putOption(b, 1, wash,    DownProfile.A);          // G
		F.Mode    .a.putOption(b, 2, stimulation, UpMode.Always);           // H
		F.Profile .a.putOption(b, 2, stimulation, DownProfile.A);      // I
		F.PreDelay.a.putInt1  (b, 3, stimulation, Limits.STIMULATION_PRE_DELAY); // J
		F.Duration.a.putInt1  (b, 4, stimulation, Limits.STIMULATION_DURATION);  // K
	}
	private byte[] get(JsonO g, JsonO options, JsonO monitor, JsonO wash, JsonO stimulation) {
		byte[] b = new byte[8];
		byte[] c = new byte[1];
		L.getBits        (g, b, 0, OPTIONS, 0);                   // A, B, C, D, L
		if (Limits.Pressure.hg.is(options, F.Pressure))
			b[0] |= 0x10;                                         // E
		F.Profile .a.getOption(b, 1, monitor, UpProfile.A);       // F
		F.Profile .a.getOption(c, 0, wash,    DownProfile.A);     // G
		b[1] |= c[0];
		F.Mode    .a.getOption(b, 2, stimulation, UpMode.Always);      // H
		F.Profile .a.getOption(c, 0, stimulation, DownProfile.A); // I
		b[2] |= c[0];
		F.PreDelay.a.getInt1(b, 3, stimulation, Limits.STIMULATION_PRE_DELAY); // J
		F.Duration.a.getInt1(b, 4, stimulation, Limits.STIMULATION_DURATION);  // K
//System.out.println("PmOptions.get b=" + Bytes.hex(b) + " g=" + g.encode());
		return b;
	}
	private static void tabA(L.Rows r, String tab) { // rest
		P p = P.Options;
		r.addIconHelpCheck (Icon.caratR, Help.Options_PWM,               p, F.PWM,               States.enabled);  // A
		r.addIconHelpCheck (Icon.caratR, Help.Options_RemoteLamp,        p, F.RemoteLamp,        States.present);  // B
		r.addIconHelpCheck (Icon.caratR, Help.Options_VariablePulsation, p, F.VariablePulsation, States.enabled);  // C
		r.addIconHelpCheck (Icon.caratR, Help.Options_DemoMode,          p, F.DemoMode,          States.enabled);  // D
		r.addIconHelpSelect(Icon.Curves, Help.Options_Units,             p, F.Pressure,          Limits.Pressure.map(), null); // E
		r.addIconHelpCheck (Icon.caratR, Help.Options_ExitGate,          p, F.ExitGate,          States.enabled);  // L
	}
	private static void tabB(L.Rows r, String tab, L.Values values) {
		P p = P.Monitor;
		r.addLabel         (Icon.grid,  P.Monitor.name(), false);
		r.addIconHelpSelect(Icon.empty, Help.Monitor_Profile,      p, F.Profile,  UpProfile.map(), null); // F
		p = P.Wash;
		r.addLabel         (Icon.grid,  P.Wash.name(), false);
		r.addIconHelpSelect(Icon.empty, Help.Wash_Profile,         p, F.Profile,  DownProfile.map(), null); // G
		p = P.Stimulation;
		r.addLabel         (Icon.grid,  P.Stimulation.name(),      false);
		r.addIconHelpSelect(Icon.empty, Help.Stimulation_Profile,  p, F.Profile, DownProfile.map(), null); // I
		r.addIconHelpSelect(Icon.empty, Help.Stimulation_Mode,     p, F.Mode,    UpMode.map(), null); // H
		r.addIconHelpButton(Icon.empty, Help.Stimulation_PreDelay,  p, F.PreDelay, Limits.STIMULATION_PRE_DELAY.units()); // J
		r.addIconHelpButton(Icon.empty, Help.Stimulation_Duration,  p, F.Duration, Limits.STIMULATION_DURATION.units());  // K
	}
	enum UpMode implements L.Options
	{
		Always    (0x10),
		FirstMilk (0x20),
		UserButton(0x30);
			j &= 0xF0;
	enum UpProfile implements L.Options
	{
		A(0x10, "profile_1"),
		B(0x20, "profile_2"),
		C(0x30, "profile_3"),
		D(0x40, "profile_4"),
			j &= 0xF0;
	enum DownProfile implements L.Options
	{
		_(0x00, "profile_0"),
		A(0x01, "profile_1"),
		B(0x02, "profile_2"),
		C(0x03, "profile_3"),
		D(0x04, "profile_4"),
		E(0x05, "profile_5");
			j &= 0x0F;
*/
