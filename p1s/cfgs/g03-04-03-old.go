package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/p1s/vac"
)

func New03(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		perMin  *cfg.I18nUnit
		percent *cfg.I18nUnit
		seconds *cfg.I18nUnit
		vacuum  *cfg.I18nUnit
		msec    *cfg.I18nUnit
	}{
		perMin:  units.Get("perMinute"),
		percent: units.Get("percent"),
		seconds: units.Get("seconds"),
		vacuum:  units.Get("vacuum"),
		msec:    units.Get("milliseconds"),
	}
	// options
	act := cfg.Options(
		[]string{ // keys
			"off",
			"constant",
			"network",
			"vso0V",
			"vso24V",
		},
		[]byte{ // values
			0,  // 00000000 off
			1,  // 00000001 constant
			7,  // 00000111 network
			11, // 00001011 vso0V
			27, // 00011011 vso24V
		},
		0x1F, // valid lower five bits
		opts.Get("act3"),
		nil, // no units
	)
	noYes := cfg.Options(
		[]string{
			"no",
			"yes",
		},
		[]byte{
			0,
			1,
		},
		0x01, // valid lowest bit
		opts.Get("noYes"),
		nil, // no units
	)

	// numbers
	ppm := cfg.Number(0, 250, u.perMin, nil)
	rateOn := cfg.Number(40, 80, u.percent, nil)
	rateOnPPM := cfg.Number(0, 20, u.percent, nil)
	avg := cfg.Number(0, 20, nil, nil)
	duration := cfg.Number(0, 30, u.seconds, nil)
	// vacuum in cfg is in 10-bits
	vacHg00, _ := vac.HgInchesCounts(0.0, 10)  //  0'Hg ~  68 counts (vac10min)
	vacHg02, _ := vac.HgInchesCounts(2.0, 10)  //  2'Hg ~ 172 counts
	vacHg07, _ := vac.HgInchesCounts(7.0, 10)  //  7'Hg ~ 432 counts
	vacHg15, _ := vac.HgInchesCounts(15.0, 10) // 15'Hg ~ 848 counts
	vacuumScales := &cfg.Scales{
		Metric:   &cfg.Scale{Factor: vac.C10kpa, Base: vac.C10min, Decimals: 2},
		Imperial: &cfg.Scale{Factor: vac.C10hg, Base: vac.C10min, Decimals: 2},
	}
	vacuumL := cfg.Number(vacHg00, vacHg02, u.vacuum, vacuumScales)
	vacuumH := cfg.Number(vacHg07, vacHg15, u.vacuum, vacuumScales)
	phase := cfg.Number(0, 1000, u.msec, nil)

	gA := "pulsator"
	A1 := cfg.Field("pAct", act, cfg.Inits{"constant"})
	A2 := cfg.Field("pPPM", ppm, cfg.Inits{"60"})
	A3 := cfg.Field("pRon", rateOn, cfg.Inits{"60"})

	gB := "monitor"
	B1 := cfg.Field("mAct", act, cfg.Inits{"constant"})
	B2 := cfg.Field("mPPM", ppm, cfg.Inits{"60"})
	B3 := cfg.Field("mRonPPM", rateOnPPM, cfg.Inits{"5"})
	B4 := cfg.Field("mAvg", avg, cfg.Inits{"3"})
	B5 := cfg.Field("mHVmax", vacuumH, cfg.Inits{"13.01"})
	B6 := cfg.Field("mHVmin", vacuumH, cfg.Inits{"11.01"})
	B7 := cfg.Field("mLVmax", vacuumL, cfg.Inits{"0.50"})
	B8 := cfg.Field("mRep", noYes, cfg.Inits{"yes"})

	gC := "frontRear"
	C1 := cfg.Field("frRon", rateOn, cfg.Inits{"60"})
	C2 := cfg.Field("frA", phase, cfg.Inits{"40"})
	C3 := cfg.Field("frB", phase, cfg.Inits{"400"})
	C4 := cfg.Field("frC", phase, cfg.Inits{"40"})
	C5 := cfg.Field("frD", phase, cfg.Inits{"400"})

	gD := "stimulation"
	D1 := cfg.Field("sAct", noYes, cfg.Inits{"no"})
	D2 := cfg.Field("sDur", duration, cfg.Inits{"0"})

	ids := []byte{0, 1, 2, 3}
	mess1 := cfg.Fields{
		//                tx[0] 1
		A1, // p0 b00.00001111 tx[1] pAct
		A2, // p1 b01          tx[2] pPPM
		A3, // p2 b02          tx[3] pRon
		D1, // p3 b03.10000000 tx[4] sAct
		B8, // p4 b03.01000000   [4] mRep
		B1, // p5 b03.00001111   [4] mAct
		B2, // p6 b04          tx[5] mPPM
		C1, // p7 b05          tx[6] frRon
		B4, // p8 b06          tx[7] mAvg
	}
	rx1 := func(p *cfg.Params, b []byte) {
		mess1[0].Rx1(p, b[0])
		mess1[1].Rx1(p, b[1])
		mess1[2].Rx1(p, b[2])
		mess1[3].Rx1(p, b[3]>>7)
		mess1[4].Rx1(p, b[3]>>6)
		mess1[5].Rx1(p, b[3]>>0)
		mess1[6].Rx1(p, b[4])
		mess1[7].Rx1(p, b[5])
		mess1[8].Rx1(p, b[6])
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = ids[0]
		tx[1] = mess1[0].Tx1(g)
		tx[2] = mess1[1].Tx1(g)
		tx[3] = mess1[2].Tx1(g)
		tx[4] = (mess1[3].Tx1(g) << 7) |
			(mess1[4].Tx1(g) << 6) |
			(mess1[5].Tx1(g) << 0)
		tx[5] = mess1[6].Tx1(g)
		tx[6] = mess1[7].Tx1(g)
		tx[7] = mess1[8].Tx1(g)
		return tx
	}

	_2x := true // means first high, then low (big-endian)
	mess2 := cfg.Fields{
		//           tx[0]      2
		B3, // p0 b07     tx[1]      mRonPPM
		C2, // p1 b08:b09 tx[2]tx[3] frA
		C3, // p2 b10:b11 tx[4]tx[5] frB
		C4, // p3 b12:b13 tx[6]tx[7] frC
	}
	rx2 := func(p *cfg.Params, b []byte) {
		mess2[0].Rx1(p, b[7])
		mess2[1].Rx2(p, b[8], b[9], _2x)
		mess2[2].Rx2(p, b[10], b[11], _2x)
		mess2[3].Rx2(p, b[12], b[13], _2x)
	}
	tx2 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = ids[1]
		tx[1] = mess2[0].Tx1(g)
		tx[2], tx[3] = mess2[1].Tx2(g, _2x)
		tx[4], tx[5] = mess2[2].Tx2(g, _2x)
		tx[6], tx[7] = mess2[3].Tx2(g, _2x)
		return tx
	}

	mess3 := cfg.Fields{
		//         tx[0]=3
		C5, // p0 b14:b15 tx[1]tx[2] frD
		B5, // p1 b16:b17 tx[3]tx[4] mHVmax
		B6, // p2 b18:b19 tx[5]tx[6] mHVmin
		B7, // p3 b20:b21 tx[7]----- mLVmax
	}
	rx3 := func(p *cfg.Params, b []byte) {
		mess3[0].Rx2(p, b[14], b[15], _2x)
		mess3[1].Rx2(p, b[16], b[17], _2x)
		mess3[2].Rx2(p, b[18], b[19], _2x)
		mess3[3].Rx2(p, b[20], b[21], _2x)
	}
	tx3 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = ids[2]
		tx[1], tx[2] = mess3[0].Tx2(g, _2x)
		tx[3], tx[4] = mess3[1].Tx2(g, _2x)
		tx[5], tx[6] = mess3[2].Tx2(g, _2x)
		tx[7], _ = mess3[3].Tx2(g, _2x) // hi part only! low part will be in mess4[0]
		return tx
	}

	mess4 := cfg.Fields{
		//            tx[0]=4
		B7, // p0 ---:b21  -----tx[1] mLVmax Repetead also in mess3 !
		D2, // p1 b22      tx[2]      sDur
	}
	rx4 := func(p *cfg.Params, b []byte) {
		// skip mess4[0] since was updated in rx3 already mLVmax was set in map in rx3 (part b21)
		mess4[1].Rx1(p, b[22])
	}
	tx4 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = ids[3]
		_, tx[1] = mess4[0].Tx2(g, _2x) // Only low part! since hi part was in mess3[3]
		tx[2] = mess4[1].Tx1(g)
		// Left tx[3-7] in zeroes
		return tx
	}
	g := []string{"", gA, gB, gC, gD}
	f := map[string]cfg.Fields{
		"": nil, // no profiles
		gA: cfg.Fields{A1, A2, A3},
		gB: cfg.Fields{B1, B2, B3, B4, B5, B6, B7, B8},
		gC: cfg.Fields{C1, C2, C3, C4, C5},
		gD: cfg.Fields{D1, D2},
	}

	rxs := []cfg.MessRxFunc{rx1, rx2, rx3, rx4}
	txs := []cfg.MessTxFunc{tx1, tx2, tx3, tx4}
	debug := "" // empty to disable debug printfs
	sim := []cfg.Fields{mess1, mess2, mess3, mess4}

	m := lines.NewMessN(ids, rxs, txs, nil, debug)
	return cfg.NewGFMS(g, f, m, sim)
}
