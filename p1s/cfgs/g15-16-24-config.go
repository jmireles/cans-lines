package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/p1s/vac"
)

func New15(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		perMin  *cfg.I18nUnit
		percent *cfg.I18nUnit
		seconds *cfg.I18nUnit
		vacuum  *cfg.I18nUnit
		msec    *cfg.I18nUnit
	}{
		perMin:  units.Get("perMinute"),
		percent: units.Get("percent"),
		seconds: units.Get("seconds"),
		vacuum:  units.Get("vacuum"),
		msec:    units.Get("milliseconds"),
	}
	// options
	act := cfg.Options(
		[]string{ // keys
			"constant",
			"network",
			"vso0V",
			"vso24V",
		},
		[]byte{ // values
			0,  // 00000000 constant
			6,  // 00000110 network
			10, // 00001010 vso0V
			26, // 00011010 vso24V
		},
		0x1E, // valid lower five bits
		opts.Get("act15"),
		nil, // no units
	)
	noYes := cfg.Options(
		[]string{
			"no",
			"yes",
		},
		[]byte{
			0,
			1,
		},
		0x01, // valid lowest bit
		opts.Get("noYes"),
		nil, // no units
	)

	// numbers
	avg := cfg.Number(3, 20, nil, nil)
	rateOnPPM := cfg.Number(0, 20, u.percent, nil)
	phase := cfg.Number(0, 1000, u.msec, nil)
	// vacuum in cfg is in 10-bits
	vacHg00, _ := vac.HgInchesCounts(0.0, 10)  //  0'Hg ~  68 counts (vac10min)
	vacHg02, _ := vac.HgInchesCounts(2.0, 10)  //  2'Hg ~ 172 counts
	vacHg07, _ := vac.HgInchesCounts(7.0, 10)  //  7'Hg ~ 432 counts
	vacHg15, _ := vac.HgInchesCounts(15.0, 10) // 15'Hg ~ 848 counts
	vScales := &cfg.Scales{
		Metric:   &cfg.Scale{Factor: vac.C10kpa, Base: vac.C10min, Decimals: 2},
		Imperial: &cfg.Scale{Factor: vac.C10hg, Base: vac.C10min, Decimals: 2},
	}
	vacuumL := cfg.Number(vacHg00, vacHg02, u.vacuum, vScales)
	vacuumH := cfg.Number(vacHg07, vacHg15, u.vacuum, vScales)

	gA := "general"
	A1 := cfg.Field("gAct", act, cfg.Inits{"constant"})
	A2 := cfg.Field("gPuls", noYes, cfg.Inits{"no"})
	A3 := cfg.Field("gAvg", avg, cfg.Inits{"3"})
	A4 := cfg.Field("gRonPPM", rateOnPPM, cfg.Inits{"5"})

	gB := "phases"
	B1 := cfg.Field("pAmin", phase, cfg.Inits{"15"})
	B2 := cfg.Field("pAmax", phase, cfg.Inits{"160"})
	B3 := cfg.Field("pCmin", phase, cfg.Inits{"30"})
	B4 := cfg.Field("pCmax", phase, cfg.Inits{"160"})

	gC := "vacuums"
	C1 := cfg.Field("vHVmax", vacuumH, cfg.Inits{"13.01"})
	C2 := cfg.Field("vHVmin", vacuumH, cfg.Inits{"11.01"})
	C3 := cfg.Field("vLVmax", vacuumL, cfg.Inits{"0.50"})

	_2x := true // means first high, then low (big-endian)
	mess1 := cfg.Fields{
		//                  tx[0]      1
		A1, // p00 b00.00011110 tx[1]      gAct
		A2, // p01 b00.00000001 tx[1]      gPuls
		A3, // p02 b01          tx[2]      gAvg
		A4, // p03 b02          tx[3]      gRonPPM
		B1, // p04 b03:b04      tx[4]tx[5] pAmin
		B2, // p05 b05:b06      tx[6]tx[7] pAmax
	}
	rx1 := func(p *cfg.Params, b []byte) {
		A1.Rx1(p, b[0])
		A2.Rx1(p, b[0])
		A3.Rx1(p, b[1])
		A4.Rx1(p, b[2])
		B1.Rx2(p, b[3], b[4], _2x)
		B2.Rx2(p, b[5], b[6], _2x)
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = 1 // ID of message 1/3
		tx[1] = A1.Tx1(g) | A2.Tx1(g)
		tx[2] = A3.Tx1(g)
		tx[3] = A4.Tx1(g)
		tx[4], tx[5] = B1.Tx2(g, _2x)
		tx[6], tx[7] = B2.Tx2(g, _2x)
		return tx
	}

	mess2 := cfg.Fields{
		//             tx[0]      2
		B3, // p06 b07:b08 tx[1]tx[2] pCmin
		B4, // p07 b09:b10 tx[3]tx[4] pCmax
		C1, // p08 b11:b12 tx[5]tx[6] vHVmax
		//     b13     tx[7]      --     unused
	}
	rx2 := func(p *cfg.Params, b []byte) {
		B3.Rx2(p, b[7], b[8], _2x)
		B4.Rx2(p, b[9], b[10], _2x)
		C1.Rx2(p, b[11], b[12], _2x)
		// b[13] unused
	}
	tx2 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = 2 // ID of message 2/3
		tx[1], tx[2] = B3.Tx2(g, _2x)
		tx[3], tx[4] = B4.Tx2(g, _2x)
		tx[5], tx[6] = C1.Tx2(g, _2x)
		// tx[7] is 0 unused
		return tx
	}

	mess3 := cfg.Fields{
		//             tx[0]      3
		C2, // p09 b14:b15 tx[1]tx[2] vHVmin
		C3, // p10 b16:b17 tx[3]tx[4] vLVmax
		//     b18:b19 tx[5]tx[6] -- --  unused
		//     b20     tx[7]      --     unused
	}
	rx3 := func(p *cfg.Params, b []byte) {
		C2.Rx2(p, b[14], b[15], _2x)
		C3.Rx2(p, b[16], b[17], _2x)
		// b[18][19][20] unused
	}
	tx3 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = 3
		tx[1], tx[2] = C2.Tx2(g, _2x)
		tx[3], tx[4] = C3.Tx2(g, _2x)
		//  tx[5] = tx[6] = tx[7] = 0 unused
		return tx
	}

	g := []string{"", gA, gB, gC}
	f := map[string]cfg.Fields{
		"": nil,
		gA: cfg.Fields{A1, A2, A3, A4},
		gB: cfg.Fields{B1, B2, B3, B4},
		gC: cfg.Fields{C1, C2, C3},
	}

	ids := []byte{1, 2, 3}
	rxs := []cfg.MessRxFunc{rx1, rx2, rx3}
	txs := []cfg.MessTxFunc{tx1, tx2, tx3}
	sim := []cfg.Fields{mess1, mess2, mess3}
	debug := "" // empty to disable debug printfs

	m := lines.NewMessN(ids, rxs, txs, nil, debug)
	return cfg.NewGFMS(g, f, m, sim)
}
