package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
)

func New11(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		perMin  *cfg.I18nUnit
		percent *cfg.I18nUnit
	}{
		perMin:  units.Get("perMinute"),
		percent: units.Get("percent"),
	}
	// options
	profile := cfg.Options(
		[]string{"1", "2", "3", "4", "5"},
		[]byte{0, 1, 2, 3, 4},
		0x0F,
		nil,
		nil,
	)
	// numbers
	ppm := cfg.Number(0, 250, u.perMin, nil)
	rateOn := cfg.Number(40, 80, u.percent, nil)

	gP := "_"
	P1 := cfg.Field("_Prof", profile, cfg.Inits{"1", "2", "3", "4", "5"})
	tx0 := func(g interface{}) []byte { // profile
		return []byte{
			P1.Tx1(g),
		}
	}

	gA := "ppmRates"
	A1 := cfg.Field("pPPM", ppm, cfg.Inits{"61", "62", "63", "64", "65"})
	A2 := cfg.Field("pRateF", rateOn, cfg.Inits{"51", "52", "53", "54", "55"})
	A3 := cfg.Field("pRateR", rateOn, cfg.Inits{"51", "52", "53", "54", "55"})

	// single message in 4 bytes, four parameters p0-p3
	mess1 := cfg.Fields{
		P1, // profile
		// p0 b0    tx[0]      profile external
		A1, // p1 b1    tx[1]      pPPM
		A2, // p2 b2    tx[2]      pRateF
		A3, // p3 b3    tx[3]      pRateR
		//    b4:b7 tx[4]tx[7] -- -- -- -- unused
	}
	rx1 := func(p *cfg.Params, b []byte) {
		P1.Rx1(p, b[0])
		A1.Rx1(p, b[1])
		A2.Rx1(p, b[2])
		A3.Rx1(p, b[3])
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = P1.Tx1(g)
		tx[1] = A1.Tx1(g)
		tx[2] = A2.Tx1(g)
		tx[3] = A3.Tx1(g)
		return tx
	}
	g := []string{gP, gA}
	f := map[string]cfg.Fields{
		gP: cfg.Fields{P1},
		gA: cfg.Fields{A1, A2, A3},
	}
	sim := []cfg.Fields{mess1}
	debug := ""
	m1 := lines.NewMess1(rx1, tx1, tx0, debug)

	return cfg.NewGFMS(g, f, m1, sim)
}
