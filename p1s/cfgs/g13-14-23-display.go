package cfgs

import (
	"gitlab.com/jmireles/nav/cfg"

	"gitlab.com/jmireles/cans-lines"
)

func New13(opts *cfg.I18nOpts, units *cfg.I18nUnits) *cfg.GFMS {
	// units
	u := struct {
		seconds *cfg.I18nUnit
		percent *cfg.I18nUnit
	}{
		seconds: units.Get("seconds"),
		percent: units.Get("percent"),
	}
	// options
	backlite := cfg.Options(
		[]string{"always", "whenMilking", "whenAlarm"},
		[]byte{0x00, 0x10, 0x20},
		0xF0, // mask upper nibble
		opts.Get("backlite13"),
		nil, // no units
	)
	brightness := cfg.Options(
		[]string{"0", "20", "40", "60", "80", "100"},
		[]byte{0, 1, 2, 3, 4, 5},
		0x0F,
		nil,
		u.percent,
	)
	// numbers
	duration := cfg.Number(0, 30, u.seconds, nil)

	// groups & fields
	gA := "durations"
	A1 := cfg.Field("dGraph", duration, cfg.Inits{"0"})
	A2 := cfg.Field("dVacuum", duration, cfg.Inits{"0"})
	A3 := cfg.Field("dRonPPM", duration, cfg.Inits{"0"})
	A4 := cfg.Field("dPhases", duration, cfg.Inits{"0"})
	A5 := cfg.Field("dMilkT", duration, cfg.Inits{"0"})
	A6 := cfg.Field("dAlarms", duration, cfg.Inits{"0"})

	gB := "screen"
	B1 := cfg.Field("sBackL", backlite, cfg.Inits{"always"})
	B2 := cfg.Field("sBright", brightness, cfg.Inits{"0"})

	// single message in 7 bytes
	mess1 := cfg.Fields{
		A1, // p00 b0          tx[0] dGraph
		A2, // p01 b1          tx[1] dVacuum
		A3, // p02 b2          tx[2] dRonPPM
		A4, // p03 b3          tx[3] dPhases
		A5, // p04 b4          tx[4] dMilk
		A6, // p05 b5          tx[5] dAlarms

		B1, // p06 b6.11110000 tx[6] sBackL
		B2, // p07 b6.00001111 tx[6] sBright
	}
	rx1 := func(p *cfg.Params, b []byte) {
		A1.Rx1(p, b[0])
		A2.Rx1(p, b[1])
		A3.Rx1(p, b[2])
		A4.Rx1(p, b[3])
		A5.Rx1(p, b[4])
		A6.Rx1(p, b[5])

		B1.Rx1(p, b[6])
		B2.Rx1(p, b[6])
	}
	tx1 := func(g interface{}) []byte {
		tx := make([]byte, 8)
		tx[0] = A1.Tx1(g)
		tx[1] = A2.Tx1(g)
		tx[2] = A3.Tx1(g)
		tx[3] = A4.Tx1(g)
		tx[4] = A5.Tx1(g)
		tx[5] = A6.Tx1(g)

		tx[6] = B1.Tx1(g) | B2.Tx1(g)
		return tx
	}

	g := []string{"", gA, gB}
	f := map[string]cfg.Fields{
		"": nil, // no profile
		gA: cfg.Fields{A1, A2, A3, A4, A5, A6},
		gB: cfg.Fields{B1, B2},
	}
	sim := []cfg.Fields{mess1}
	debug := ""
	m1 := lines.NewMess1(rx1, tx1, nil, debug)

	return cfg.NewGFMS(g, f, m1, sim)
}

/* BYTE  Screen           Values      TAB/param
 * ====  ===============  ==========  =========
 *  0    PulsationGraph   (0-30) sec  A
 *  1    VacuumHeight     (0-30) sec  B
 *  2    RateOnPPM        (0-30) sec  C
 *  3    Phases           (0-30) sec  D
 *  4    MilkingTime      (0-30) sec  E
 *  5    ProblemScreen    (0-30) sec  F
 *  6    Backlite         ()
 *       Brightness       ()
 *  7    ---
 *
public class PmDisplay implements Line.Config {
	private void put(byte[] b, JsonO g, JsonO tab) {
		F.PulsationGraph.a.putInt1  (b, 0, tab, Limits.DISPLAY_TIME); // A1
		F.VacuumHeight  .a.putInt1  (b, 1, tab, Limits.DISPLAY_TIME); // A2
		F.RateOnPPM     .a.putInt1  (b, 2, tab, Limits.DISPLAY_TIME); // A3
		F.Phases        .a.putInt1  (b, 3, tab, Limits.DISPLAY_TIME); // A4
		F.MilkingTime   .a.putInt1  (b, 4, tab, Limits.DISPLAY_TIME); // A5
		F.ProblemScreen .a.putInt1  (b, 5, tab, Limits.DISPLAY_TIME); // A6
		F.Backlite      .a.putOption(b, 6, tab, Backlite.AllTheTime); // A7
		F.Brightness    .a.putOption(b, 6, tab, Brightness.A);        // A8
	private byte[] get(JsonO g) {
		byte[] b = new byte[8];
		byte[] c = new byte[1];
		JsonO tab = P.Time.getObject(g);
		F.PulsationGraph.a.getInt1(b, 0, tab, Limits.DISPLAY_TIME); // A1
		F.VacuumHeight  .a.getInt1(b, 1, tab, Limits.DISPLAY_TIME); // A2
		F.RateOnPPM     .a.getInt1(b, 2, tab, Limits.DISPLAY_TIME); // A3
		F.Phases        .a.getInt1(b, 3, tab, Limits.DISPLAY_TIME); // A4
		F.MilkingTime   .a.getInt1(b, 4, tab, Limits.DISPLAY_TIME); // A5
		F.ProblemScreen .a.getInt1(b, 5, tab, Limits.DISPLAY_TIME); // A6
		F.Backlite      .a.getOption(b, 6, tab, Backlite.AllTheTime);
		F.Brightness    .a.getOption(c, 0, tab, Brightness.A);
		b[6] |= c[0];
		return b;
	static void paramsPage(L.Rows r, String tab, L.Values v) {
		P p = P.Time;
		L.U u = Limits.DISPLAY_TIME.units();
		r.addLabel(Icon.caratR, Help.config("Display_ScreenDurations"), false);
		r.addIconHelpButton(Icon.empty, Help.Display_PulsationGraph, p, F.PulsationGraph, u); // A1
		r.addIconHelpButton(Icon.empty, Help.Display_VacuumHeight,   p, F.VacuumHeight,   u); // A2
		r.addIconHelpButton(Icon.empty, Help.Display_RateOnPPM,      p, F.RateOnPPM,      u); // A3
		r.addIconHelpButton(Icon.empty, Help.Display_Phases,         p, F.Phases,         u); // A4
		r.addIconHelpButton(Icon.empty, Help.Display_MilkingTime,    p, F.MilkingTime,    u); // A5
		r.addIconHelpButton(Icon.empty, Help.Display_ProblemScreen,  p, F.ProblemScreen,  u); // A6
		r.addLabel(Icon.caratD, "Backlite", false);
		r.addIconHelpSelect(Icon.empty, Help.Display_Backlite,       p, F.Backlite,   Backlite.map(), null);   // A7
		r.addIconHelpSelect(Icon.empty, Help.Display_Brightness,     p, F.Brightness, Brightness.map(), L.U.Percent); // A8
	}
	static L.Popup paramPopup(String tab, String p, String f, L.Popup popup) {
		if (F.Backlite  .name().equals(f)) { return popup.selectList(p + "." + f, Backlite.map()); } // A7
		if (F.Brightness.name().equals(f)) { return popup.selectList(p + "." + f, Brightness.map()); } // A8
		return popup.integer(Limits.DISPLAY_TIME); // A1-A6
	}
	enum Backlite implements L.Options {
		AllTheTime,    // 0000xxxx
		OnWhenMilking, // 0001xxxx
		OnWhenAlarm;   // 0010xxxx
	enum Brightness implements L.Options {
		A(  0), //  0%
		B( 20), //  6%
		C( 40), // 13%
		D( 60), // 20%
		E( 80), // 26%
		F(100);
*/
