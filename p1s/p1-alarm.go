package p1s

import (
	"gitlab.com/jmireles/cans-lines/unsols"
)

type alarm struct {
	noPhase bool
	bits    byte
}

func (a *alarm) set(noPhase bool, bits byte) {
	a.noPhase = noPhase
	a.bits = bits
}

func (a *alarm) noPhaseByte() byte {
	if a.noPhase {
		return 0
	} else {
		return 1
	}
}

const (
	AlarmAS string = "Hose disconnected. No vacuum present. Bad coil pulsator."
	AlarmFS        = "Fresh air restricted/vacuum. Piston not sealing. Hole inside liner."
	AlarmBS        = "Vacuum too low, check air leaks."
	AlarmCS        = "Vacuum too high, check vacuum regulator."
	AlarmDS        = "Cow stepping on hose, hose clogged."
	AlarmES        = "Pulsator dirty or sticking, fresh air restriction."
)

type Alarm int

// Alarms priorities
const (
	AlarmA    Alarm = 6
	AlarmF    Alarm = 5
	AlarmB    Alarm = 4
	AlarmC    Alarm = 3
	AlarmD    Alarm = 2
	AlarmE    Alarm = 1
	AlarmNone Alarm = 0
)

const (
	AlarmMaskPPMRate     = 0x80 // AlarmE
	AlarmMaskHiVacuumMax = 0x40 // AlarmC
	AlarmMaskHiVacuumMin = 0x20 // AlarmB
	AlarmMaskLoVacuumMax = 0x10 // AlarmF
	AlarmMaskPhaseAMin   = 0x08 // AlarmD
	AlarmMaskPhaseBMin   = 0x04 // AlarmE
	AlarmMaskPhaseCMin   = 0x02 // AlarmD
	AlarmMaskPhaseDMin   = 0x01 // AlarmE
)

// AlarmsParse returns the greater alarm of both phases
func AlarmsParse(alarms []byte) (a Alarm, front, rear byte) {
	if alarms == nil {
		a = AlarmNone
	} else if len(alarms) < 2 {
		a = AlarmNone
	} else {
		front = alarms[0]
		rear = alarms[1]
		f := AlarmParse(front)
		r := AlarmParse(rear)
		if f <= r {
			a = f
		} else {
			a = r
		}
	}
	return
}

func AlarmParse(alarms byte) (a Alarm) {
	if alarms == 0xFF {
		return AlarmA
	}
	if (alarms & AlarmMaskLoVacuumMax) != 0 {
		return AlarmF
	}
	if (alarms & AlarmMaskHiVacuumMin) != 0 {
		return AlarmB
	}
	if (alarms & AlarmMaskHiVacuumMax) != 0 {
		return AlarmC
	}
	if (alarms & (AlarmMaskPhaseAMin | AlarmMaskPhaseCMin)) != 0 {
		return AlarmD
	}
	if (alarms & (AlarmMaskPhaseBMin | AlarmMaskPhaseDMin | AlarmMaskPPMRate)) != 0 {
		return AlarmE
	}
	return AlarmNone
}

func AlarmBits(alarms byte) (b []byte) {
	if alarms == 0 {
		return
	}
	if alarms == 0xff {
		b = []byte{'N'}
		return
	}
	b = make([]byte, 0)
	if (alarms & AlarmMaskLoVacuumMax) != 0 {
		b = append(b, 'L')
	}
	if (alarms & AlarmMaskHiVacuumMin) != 0 {
		b = append(b, 'h')
	}
	if (alarms & AlarmMaskHiVacuumMax) != 0 {
		b = append(b, 'H')
	}
	if (alarms & AlarmMaskPhaseAMin) != 0 {
		b = append(b, 'A')
	}
	if (alarms & AlarmMaskPhaseCMin) != 0 {
		b = append(b, 'C')
	}
	if (alarms & AlarmMaskPhaseBMin) != 0 {
		b = append(b, 'B')
	}
	if (alarms & AlarmMaskPhaseDMin) != 0 {
		b = append(b, 'D')
	}
	if (alarms & AlarmMaskPPMRate) != 0 {
		b = append(b, 'P')
	}
	return b
}

type AlarmsDst byte
const (
	AlarmsDstMobile AlarmsDst = 1
	AlarmsDstAudio            = 2
)

type AlarmsType byte
const (
	AlarmsTypeMilkings AlarmsType = 1
	AlarmsTypeMinutes             = 2
)

type AlarmsNotify struct {
	Dst      AlarmsDst
	Type     AlarmsType
	Value    byte
	Bits     []byte
	NoPhaseF bool
	NoPhaseR bool
}

func NewAlarmsNotify(extra []byte) (an *AlarmsNotify, err error) {
	if len(extra) < 6 {
		err = unsols.ErrorInvalidAlarms
		return
	}
	type_ := extra[0]
	an = &AlarmsNotify{
		Value: extra[1], // number of milkings/minutes
		Bits: []byte {
			extra[2], // bits for front
			extra[3], // bits for rear
		},
		NoPhaseF: extra[4] == 1,
		NoPhaseR: extra[5] == 1,
	}
	switch type_ {
	case 1:
		an.Dst = AlarmsDstMobile
		an.Type = AlarmsTypeMilkings

	case 2:
		an.Dst = AlarmsDstMobile
		an.Type = AlarmsTypeMinutes

	case 3:
		an.Dst = AlarmsDstAudio
		an.Type = AlarmsTypeMilkings

	case 4:
		an.Dst = AlarmsDstAudio
		an.Type = AlarmsTypeMinutes

	default:
		err = unsols.ErrorInvalidType
	}
	return
}
