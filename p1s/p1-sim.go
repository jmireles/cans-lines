package p1s

import (
	"errors"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/p1"
)

type State int

const (
	StateIdle State = iota
	StateWorking
	StateAlarmed
	StateStimulation
)

type Sim struct {
	Status *simUnsolStatus
	Values *simRespValues
}

func NewSim() *Sim {
	return &Sim{
		&simUnsolStatus{StateIdle, &alarm{}, &alarm{}},
		&simRespValues{},
	}
}

type simUnsolStatus struct {
	state   State
	alarmsF *alarm
	alarmsR *alarm
}

func (s *simUnsolStatus) Idle() (comm base.Unsol, extra []byte, err error) {
	if s.state == StateIdle {
		err = errors.New("already in the same state")
	} else {
		s.set(StateIdle)
		comm = p1.Unsol05_Idle
	}
	return
}

func (s *simUnsolStatus) Stimulation() (comm base.Unsol, extra []byte, err error) {
	if s.state == StateStimulation {
		err = errors.New("already in the same state")
	} else {
		s.set(StateStimulation)
		comm = p1.Unsol20_Stimulation
	}
	return
}

func (s *simUnsolStatus) NoPhaseFront() (comm base.Unsol, extra []byte, err error) {
	if s.alarmsF.noPhase {
		err = errors.New("already in the same state")
	} else {
		s.set(StateAlarmed)
		s.alarmsF.set(true, 0)
		comm = p1.Unsol16_NoPhaseFront
	}
	return 
}

func (s *simUnsolStatus) NoPhaseRear() (comm base.Unsol, extra []byte, err error) {
	if s.alarmsR.noPhase {
		err = errors.New("already in the same state")
	} else {
		s.set(StateAlarmed)
		s.alarmsR.set(true, 0)
		comm = p1.Unsol17_NoPhaseRear
	}
	return
}

func (s *simUnsolStatus) BitsWorking(front, rear byte) (comm base.Unsol, extra []byte, err error) {
	if front == 0 && rear == 0 {
		if s.state == StateWorking {
			err = errors.New("already in the same state")
		} else {
			s.set(StateWorking)
			comm = p1.Unsol06_Working
		}
		return 
	}
	newBits := false
	if front != s.alarmsF.bits {
		newBits = true
		s.alarmsF.set(false, front)
	}
	if rear != s.alarmsR.bits {
		newBits = true
		s.alarmsR.set(false, rear)
	}
	if !newBits {
		err = errors.New("already in the same state")
	} else {
		s.set(StateAlarmed)
		comm = p1.Unsol07_Alarmed
		extra = []byte{
			s.alarmsF.bits, // pos 0
			s.alarmsR.bits, // pos 1
		}
	}
	return
}

func (s *simUnsolStatus) Notify(_type, value byte) (comm base.Unsol, extra []byte, err error) {
	if s.state != StateAlarmed {
		err = errors.New("cannot notify non-alarmed state")
	} else {
		comm = p1.Unsol26_Notify
		extra = []byte{
			_type,
			value,
			s.alarmsF.bits,
			s.alarmsR.bits,
			s.alarmsF.noPhaseByte(),
			s.alarmsR.noPhaseByte(),
		}
	}
	return 
}

func (s *simUnsolStatus) set(state State) {
	s.state = state
	if state != StateAlarmed {
		s.alarmsF.set(false, 0)
		s.alarmsR.set(false, 0)
	}
}

type simRespValues struct {

}

func (v *simRespValues) Phases(front bool, a, b, c, d uint16) (comm base.Sol, extra []byte) {
	if front {
		comm = p1.Sol12_ValPhasesFront
	} else {
		comm = p1.Sol13_ValPhasesRear
	}
	extra = ValuesPhasesBytesSim(a, b, c, d)
	return
}

func (v *simRespValues) Levels(front bool, vh, vl, ppm uint16) (comm base.Sol, extra []byte) {
	if front {
		comm = p1.Sol14_ValLevelsFront
	} else {
		comm = p1.Sol15_ValLevelsRear
	}
	extra = ValuesLevelsBytesSim(vh, vl, ppm)
	return
}
