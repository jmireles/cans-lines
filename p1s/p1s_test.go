package p1s

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/p1"

	"gitlab.com/jmireles/cans-lines/p1s/vac"
	"gitlab.com/jmireles/cans-lines/unsols"
)

func TestUnsols(t *testing.T) {
	tt := &base.Test{t}
	src, _ := from.NewSrc(2, from.P1, 7)
	// valid pm unsols (not notify)
	restart, _   := from.NewCanUnsol(src, base.Unsol01_Restart, nil)
	idle, _      := from.NewCanUnsol(src, p1.Unsol05_Idle, nil)
	work, _      := from.NewCanUnsol(src, p1.Unsol06_Working, nil)
	stim, _      := from.NewCanUnsol(src, p1.Unsol20_Stimulation, nil)
	alarms12,  _ := from.NewCanUnsol(src, p1.Unsol07_Alarmed, []byte{1,2})
	alarmsMax, _ := from.NewCanUnsol(src, p1.Unsol07_Alarmed, []byte{255,255})
	front, _     := from.NewCanUnsol(src, p1.Unsol16_NoPhaseFront, nil)
	rear, _      := from.NewCanUnsol(src, p1.Unsol17_NoPhaseRear, nil)
	for can, json := range map[*from.Can]string {
		restart:   "{2:{1:{7:{1:1}}}}",
		idle:      "{2:{1:{7:{1:2}}}}",
		work:      "{2:{1:{7:{1:3}}}}",
		stim:      "{2:{1:{7:{1:4}}}}",
		alarms12:  "{2:{1:{7:{1:66049}}}}",  // f=1, r=2
		alarmsMax: "{2:{1:{7:{1:131071}}}}", // f=r=255
		front:     "{2:{1:{7:{1:131072}}}}", // noPhaseF
		rear:      "{2:{1:{7:{1:262144}}}}", // noPhaseR
	} {
		resp, err := UnsolsFunc(can)
		tt.Ok(err)
		s, _ := unsols.RTJson(nil, resp) // filter is nil to pass everything
		tt.Equals(json, s)
	}
	// valid pm unsols notify
	for notify, json := range map[string]string {
		"1,0,0,0,0,0": "{2:{1:{7:{2:65536,3:257}}}}", // mobile milkings value=0
		"2,0,0,0,0,0": "{2:{1:{7:{2:65536,3:513}}}}", // mobile minutes value=0
		"3,0,0,0,0,0": "{2:{1:{7:{2:65536,3:258}}}}", // audio milkings value=0
		"4,0,0,0,0,0": "{2:{1:{7:{2:65536,3:514}}}}", // audio minutes value=0
		"1,1,0,0,0,0": "{2:{1:{7:{2:65536,3:65793}}}}", // mobile milkings value=1

		"1,0,255,0,0,0":   "{2:{1:{7:{2:65791,3:257}}}}",  // mobile milkings value=0 f=255
		"1,0,255,255,0,0": "{2:{1:{7:{2:131071,3:257}}}}", // mobile milkings value=0 f=r=255
		"1,0,0,0,1,0":     "{2:{1:{7:{2:196608,3:257}}}}",
		"1,0,0,0,0,1":     "{2:{1:{7:{2:327680,3:257}}}}",
		"1,0,255,255,1,0": "{2:{1:{7:{2:262143,3:257}}}}", // mobile milkings value=0 f=r=255,npf
		"1,0,255,255,1,1": "{2:{1:{7:{2:524287,3:257}}}}", // mobile milkings value=0 f=r=255,npf,npr
	} {
		extra := make([]byte, 0)
		for _, s := range strings.Split(notify, ",") {
			i, _ := strconv.Atoi(s)
			extra = append(extra, byte(i))
		}
		can, _ := from.NewCanUnsol(src, p1.Unsol26_Notify, extra)
		resp, err := UnsolsFunc(can)
		tt.Ok(err)
		s, _ := unsols.RTJson(nil, resp) // filter is nil to pass everything
		tt.Equals(json, s)
	}

	invalidUnsol2, _  := from.NewCanUnsol(src, base.Unsol(2), nil) // 2 is not valid unsol
	invalidAlarmsA, _ := from.NewCanUnsol(src, p1.Unsol07_Alarmed, nil)
	invalidAlarmsB, _ := from.NewCanUnsol(src, p1.Unsol07_Alarmed, []byte{1})
	invalidNotify1, _ := from.NewCanUnsol(src, p1.Unsol26_Notify, nil)
	invalidNotify2, _ := from.NewCanUnsol(src, p1.Unsol26_Notify, []byte{1,0,0,0,0})
	invalidNotify3, _ := from.NewCanUnsol(src, p1.Unsol26_Notify, []byte{5,0,0,0,0,0})

	for can, e := range map[*from.Can]error {
		invalidUnsol2:  unsols.ErrorInvalidUnsol,
		invalidAlarmsA: unsols.ErrorInvalidAlarms,
		invalidAlarmsB: unsols.ErrorInvalidAlarms,
		invalidNotify1: unsols.ErrorInvalidAlarms,
		invalidNotify2: unsols.ErrorInvalidAlarms,
		invalidNotify3: unsols.ErrorInvalidType,
	} {
		resp, err := UnsolsFunc(can)
		tt.Assert(err!=nil, "unexpected nil error for %v", resp)
		tt.Equals(err, e)
	}
}

func TestVacuum(t *testing.T) {
	tt := &base.Test{t}
	// kPa to counts
	for kpas, counts := range map[float32][]int {
		-4.44: []int {   0,    0 }, // kPa min
		0:     []int {  17,   68 }, // kPa = 0
		10:    []int {  55,  221 }, // kPa = 10
		44:    []int { 185,  743 }, // kPa = 44
		62.22: []int { 255, 1023 }, // kPa max
	} {
		counts8, err := vac.KPaCounts(kpas, 8)
		tt.Ok(err)
		tt.Equals(counts[0], counts8)
		counts10, err := vac.KPaCounts(kpas, 10)
		tt.Ok(err)
		tt.Equals(counts[1], counts10)
	}
	// "Hg to counts
	for hgs, counts := range map[float32][]int {
		-1.31: []int {  0,     0 }, // "Hg min
		0:     []int {  17,   68 }, // "Hg = 0
		10:    []int { 147,  588 }, // "Hg = 10
		13:    []int { 185,  744 }, // "Hg = 13
		18.37: []int { 255, 1023 }, // "Hg max
	} {
		counts8, err := vac.HgInchesCounts(hgs, 8)
		tt.Ok(err)
		tt.Equals(counts[0], counts8)
		counts10, err := vac.HgInchesCounts(hgs, 10)
		tt.Ok(err)
		tt.Equals(counts[1], counts10)
	}
	// 8-bit counts: { kpa, inHg }
	for counts8, vacuum := range map[int][]string {
		0:   []string{ "-4.44", "-1.31" }, // counts8 = min vacuum
		17:  []string{  "0.00",  "0.00" }, // counts8 = zero vacumm
		55:  []string{  "9.93",  "2.93" }, // counts8 aprox 10 kPa
		147: []string{ "33.99", "10.04" }, // counts8 aprox 10 "Hg
		185: []string{ "43.92", "12.97" }, // counts8 aprox 13 "Hg
		255: []string{ "62.22", "18.37" }, // counts8 max (0xff)
	} {
		kpa, err := vac.KPa(counts8, 8)
		tt.Ok(err)
		tt.Equals(vacuum[0], fmt.Sprintf("%.2f", kpa))
		inHg, err := vac.HgInches(counts8, 8)
		tt.Ok(err)
		tt.Equals(vacuum[1], fmt.Sprintf("%.2f", inHg))
	}
	// 10-bit counts: { kpa, inHg }
	for counts10, vacuum := range map[int][]string { 
		0:    []string{ "-4.43", "-1.31" }, // counts10 min vacuum
		68:   []string{  "0.00",  "0.00" }, // counts10 zero vacuum
		221:  []string{  "9.97",  "2.94" }, // counts10 aprox 10 kPa
		588:  []string{ "33.88", "10.00" }, // counts10 aprox 10 "Hg
		744:  []string{ "44.04", "13.01" }, // counts10 aprox 13 "Hg
		1023: []string{ "62.22", "18.37" }, // counts10 max (0x3ff)
	} {
		kpa, err := vac.KPa(counts10, 10)
		tt.Ok(err)
		tt.Equals(vacuum[0], fmt.Sprintf("%.2f", kpa))
		inHg, err := vac.HgInches(counts10, 10)
		tt.Ok(err)
		tt.Equals(vacuum[1], fmt.Sprintf("%.2f", inHg))
	}
}

type testCfg struct {
	data   [][]byte
	params map[string]string
}

func TestCfg03(t *testing.T) {
	tt := &base.Test{t}
	command := byte(3)
	for _, exp := range []testCfg {
		testCfg{ 
			data: [][]byte{
				[]byte{0, 0, 0, 0, 0, 0, 0, 0},
				[]byte{1, 0, 0, 0, 0, 0, 0, 0},
				[]byte{2, 0, 0, 0, 0, 0, 0, 0},
				[]byte{3, 0, 0, 0, 0, 0, 0, 0},
			},
			params: map[string]string {
				"frA":     "0", 
				"frB":     "0", 
				"frC":     "0", 
				"frD":     "0", 
				"frRon":   "40", 
				"mAct":    "off",
				"mAvg":    "0",
				"mHVmax":  "7.00",
				"mHVmin":  "7.00",
				"mLVmax":  "0.00",
				"mPPM":    "0",
				"mRep":    "no",
				"mRonPPM": "0",
				"pAct":    "off",
				"pPPM":    "0",
				"pRon":    "40",
				"sAct":    "no",
				"sDur":    "0",
			},
		},
		testCfg{
			data: [][]byte{
				[]byte{0x00, 0x01, 0x3c, 0x28, 0x41, 0x3c, 0x28, 0x14},
				[]byte{0x01, 0x14, 0x03, 0xe8, 0x00, 0x13, 0x01, 0x84},
				[]byte{0x02, 0x01, 0x10, 0x03, 0xb8, 0x03, 0xb8, 0x00},
				[]byte{0x03, 0xac, 0x28, 0x03, 0xb8, 0x03, 0xb8, 0x00},
			},
			params: map[string]string {
				"frA":     "1000",
				"frB":     "19",
				"frC":     "388",
				"frD":     "272",
				"frRon":   "40",
				"mAct":    "constant",
				"mAvg":    "20",
				"mHVmax":  "15.01",
				"mHVmin":  "15.01",
				"mLVmax":  "2.00",
				"mPPM":    "60",
				"mRep":    "yes",
				"mRonPPM": "20", 
				"pAct":    "constant",
				"pPPM":    "60",
				"pRon":    "40",
				"sAct":    "no",
				"sDur":    "30",
			},
		},
		testCfg{
			data: [][]byte{
				[]byte{ 0,  1,  60, 60,  65,  60,  60,  3 },
				[]byte{ 1,  5,   0, 40,   1, 144,   0, 40 },
				[]byte{ 2,  1, 144,  2, 232,   2, 128,  0 },
				[]byte{ 3, 94,   0,  0,   0,   0,   0,  0 },
			},
			params: map[string]string {
		       "frA":     "40",
		       "frC":     "40",
		       "frD":     "400",
		       "frB":     "400",
		       "frRon":   "60",
		       "mAct":    "constant",
		       "mAvg":    "3",
		       "mHVmax":  "13.01",
		       "mHVmin":  "11.01",
		       "mLVmax":  "0.50",
		       "mRep":    "yes",
		       "mRonPPM": "5",
		       "pAct":    "constant",
		       "pPPM":    "60",
		       "pRon":    "60",
		       "mPPM":    "60",
		       "sAct":    "no",
		       "sDur":    "0",
			},
		},
	} {
		var cfgResp []byte
		for _, data := range exp.data {
			cfg, err := Cfgs.Resp(command)
			tt.Ok(err)
			if cfg.RespParse(data, &cfgResp) { // complete
				params, err := cfg.MessRead(cfgResp)
				tt.Ok(err)
				gotParams := make(map[string]string, 0)
				for _, sParam := range strings.Split(params.String(), " ") {
					if sParam != "" {
						kv := strings.Split(sParam, "=")
						tt.Equals(2, len(kv))
						gotParams[kv[0]] = kv[1]
					}
				}
				tt.Equals(exp.params, gotParams)
			}
		}
	}
}
/*
By reading simulation cfg we get:
----
By saving cfg to simulation we get: Constant -> pAct -> Off

txSim (10) map[frRon:[60] mAct:[constant] mAvg:[3] mPPM:[60] mRep:[yes] n:[0] pAct:[constant] pPPM:[60] pRon:[60] sAct:[no]]
      (8) [1 1 60 60 65 60 60 3]
resp  (8) [1 1 60 60 65 60 60 3] complete=false
      (33) [3 1 1 60 60 65 60 60 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
txSim (5) map[frA:[40] frB:[400] frC:[40] mRonPPM:[5] n:[1]]
      (8) [2 5 0 40 1 144 0 40]
resp  (8) [2 5 0 40 1 144 0 40] complete=false
      (33) [3 1 1 60 60 65 60 60 3 2 5 0 40 1 144 0 40 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
txSim (5) map[frD:[400] mHVmax:[13.01] mHVmin:[11.01] mLVmax:[0.50] n:[2]]
      (8) [3 1 144 2 232 2 128 0]
resp  (8) [3 1 144 2 232 2 128 0] complete=false
      (33) [3 1 1 60 60 65 60 60 3 2 5 0 40 1 144 0 40 3 1 144 2 232 2 128 0 0 0 0 0 0 0 0 0]
txSim (3) map[mLVmax:[0.50] n:[3] sDur:[0]]
      (8) [4 94 0 0 0 0 0 0]
resp  (8) [4 94 0 0 0 0 0 0] complete=true
      (33) [3 1 1 60 60 65 60 60 3 2 5 0 40 1 144 0 40 3 1 144 2 232 2 128 0 4 94 0 0 0 0 0 0]
read  (33) [3 1 1 60 60 65 60 60 3 2 5 0 40 1 144 0 40 3 1 144 2 232 2 128 0 4 94 0 0 0 0 0 0]
      (28) [1 60 60 65 60 60 3 5 0 40 1 144 0 40 1 144 2 232 2 128 0 94 0 0 0 0 0 0]
       mRep=Yes sDur=0 pRon=60 sAct=No mAct=Constant mPPM=60 frRon=60 mRonPPM=5 frA=40 mHVmin=11.01 pPPM=60 mLVmax=0.50 frC=40 frD=400 frB=400 mAvg=3 mHVmax=13.01 pAct=Constant
------
save  map[frA:40 frB:400 frC:40 frD:400 frRon:60 mAct:1 mAvg:3 mHVmax:744 mHVmin:640 mLVmax:94 mPPM:60 mRep:1 mRonPPM:5 pAct:0 pPPM:60 pRon:60 sAct:0 sDur:0]
      [[1 0 60 60 65 60 60 3] [2 5 0 40 1 144 0 40] [3 1 144 2 232 2 128 0] [4 94 0 0 0 0 0 0]]
rxSim set=4 resp=3 [[1 1 4 1 0 60 60 65 60 60 3] [1 1 4 2 5 0 40 1 144 0 40] [1 1 4 3 1 144 2 232 2 128 0] [1 1 4 4 94 0 0 0 0 0 0]]
rxSim [3 1 0 60 60 65 60 60 3 2 5 0 40 1 144 0 40 3 1 144 2 232 2 128 0 4 94 0 0 0 0 0 0]
read  (33) [3 1 0 60 60 65 60 60 3 2 5 0 40 1 144 0 40 3 1 144 2 232 2 128 0 4 94 0 0 0 0 0 0]
      (28) [0 60 60 65 60 60 3 5 0 40 1 144 0 40 1 144 2 232 2 128 0 94 0 0 0 0 0 0]
       pRon=60 mRep=yes mAvg=3 frD=400 pPPM=60 mAct=constant mHVmin=11.01 mLVmax=0.50 mPPM=60 mRonPPM=5 frC=40 mHVmax=13.01 pAct=off sAct=no frRon=60 frA=40 frB=400 sDur=0
------
*/



