package p1s

import (
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/p1"
)

func HistUnsolJson(comm base.Unsol, data []byte) string {
	var sb strings.Builder
	switch comm {
	
	case base.Unsol01_Restart:
		sb.WriteString(`{"u":"O"}`)

	case p1.Unsol05_Idle:
		sb.WriteString(`{"u":"I"}`)
	
	case p1.Unsol06_Working:
		sb.WriteString(`{"u":"W"}`)
	
	case p1.Unsol07_Alarmed:
		sb.WriteString(`{"u":"A"`)
		if front := AlarmParse(data[0]); front != AlarmNone {
			sb.WriteString(`,"f":` + strconv.Itoa(int(front)))
		}
		if rear := AlarmParse(data[1]); rear != AlarmNone {
			sb.WriteString(`,"r":` + strconv.Itoa(int(rear)))
		}
		sb.WriteString(`}`)
	
	case p1.Unsol16_NoPhaseFront:
		sb.WriteString(`{"u":"A","f":` + strconv.Itoa(int(AlarmA)))
	
	case p1.Unsol17_NoPhaseRear:
		sb.WriteString(`{"u":"A","r":` + strconv.Itoa(int(AlarmA)))
	
	case p1.Unsol20_Stimulation:
		sb.WriteString(`{"u":"S"}`)
	
	case p1.Unsol26_Notify:
		sb.WriteString(`{"u":"notif"}`) // TODO add more fields
	
	default:
		sb.WriteString(`{}`)
	}
	return sb.String()
}

func HistSolJson(comm base.Sol, data []byte) string {
	var sb strings.Builder
	switch comm {

	case base.Sol00_Version:
		sb.WriteString(`{"ver":` + strconv.Itoa(int(data[0])) + `}`)
	case base.Sol02_Status:
		sb.WriteString(`{"sta":true}`) // TODO
	case p1.Sol03_Config:
		sb.WriteString(`{"cfg":"config0"}`)

	case base.Sol30_IapReady:
		sb.WriteString(`{"iap":"ready"}`)
	case base.Sol31_Serial:
		sb.WriteString(`{"ser":true}`)

	case p1.Sol04_ValCurves:
		sb.WriteString(`{"val":"vac","f":0x0000, "r":0x0000}`)
	case p1.Sol08_AvgPhaseFront:
		sb.WriteString(`{"avg":"phase","f":0}`)
	case p1.Sol09_AvgPhaseRear:
		sb.WriteString(`{"avg":"phase","r":0}`)
	case p1.Sol10_AvgLevelFront:
		sb.WriteString(`{"avg:"level","f":0}`)
	case p1.Sol11_AvgLevelRear:
		sb.WriteString(`{"avg:"level","r":0}`)
	case p1.Sol12_ValPhasesFront:
		sb.WriteString(`{"val:"phase","f":0}`)
	case p1.Sol13_ValPhasesRear:
		sb.WriteString(`{"val:"phase","r":0}`)
	case p1.Sol14_ValLevelsFront:
		sb.WriteString(`{"val:"level","f":0}`)
	case p1.Sol15_ValLevelsRear:
		sb.WriteString(`{"val:"level","r":0}`)

	case p1.Sol18_Calibr:
		sb.WriteString(`{"cfg":"calibr"}`)
	case p1.Sol21_Options:
		sb.WriteString(`{"cfg":"options"}`)
	case p1.Sol22_Profiles:
		sb.WriteString(`{"cfg":"profiles"}`)
	case p1.Sol23_Display:
		sb.WriteString(`{"cfg":"display"}`)
	case p1.Sol24_Config:
		sb.WriteString(`{"cfg":"config"`)

		sb.WriteString(`}`)
	case p1.Sol25_Alarms:
		sb.WriteString(`{"cfg":"alarms"}`)
	case p1.Sol27_Stalls:
		sb.WriteString(`{"cfg":"stalls"}`)

	default:
		sb.WriteString(`{}`)
	}
	return sb.String()
}
