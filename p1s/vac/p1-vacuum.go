package vac

import (
	"errors"
)

// 1 inch of mercury equals 3.386388 kPa
//private static final double ONE_HG_INCH_KPAS = 3.386388;

const (
	/* These constants are used to convert from 8 bits to Hg inches.
	 * To convert to kPa we use k = 62.22222 * (8bits - offset) / 238,
	 * where offset must be 17. Finally to convert from kPa to Hg
	 * inches we divide by 3.38638. */
	C8min = 17
	C8hg  = float32(62.2222 / (238 * 3.386388)) // 0.0772
	C8kpa = float32(62.2222 / 238)

	/* These constants are used to convert from 10 bits to Hg inches.
	 * To convert to kPa we use k = 62.2222 * (10bits - offset) / 955,
	 * where offset must be 68. Finally to convert from kPa to Hg
	 * inches we divide 3.386388. */
	C10min = 68
	C10hg  = float32(62.2222 / (955 * 3.386388)) // 0.01924
	C10kpa = float32(62.2222 / 955)
)

const JSF = `
// JSF start: gitlab.com/jmireles/rt-cans/lines/p1/vac/p1-vaccum.go JSF
const Vacuum = function() 
{
const vac8min = 17;
const vac8hg = (62.2222 / (238 * 3.386388));
const vac10min = 68;
const vac10hg  = (62.2222 / (955 * 3.386388));
return {
	hgInches: (counts, bits)=> {
		if (bits == 8) {
			return vac8hg * ((0xFF & counts) - vac8min);
		} else if (bits == 10) {
			return vac10hg * (counts - vac10min);
		}
		return 0;
	},
	hgInchesCounts: (hgInches, bits)=> {
		if (bits == 8) {
			return parseInt(vac8min + 0.5 + hgInches / vac8hg);
		} else if (bits == 10) {
			return parseInt(vac10min + 0.5 + hgInches / vac10hg);
		}
		return 0;
	}
}
}
// JSF end
`

var bitsErr = errors.New("Invalid bits [8|10]")

// inches = 0.0772025 * (counts - 17) for 8 bits counts
func HgInches(counts int, bits int) (float32, error) {
	if bits == 8 {
		return C8hg * float32(int(0xFF&counts) - C8min), nil
	} else if bits == 10 {
		return C10hg * float32(int(0x3FF&counts) - C10min), nil
	} else {
		return 0, bitsErr
	}
}

func KPa(counts int, bits int) (float32, error) {
	if bits == 8 {
		return C8kpa * float32(int(0xFF&counts) - C8min), nil
	} else if bits == 10 {
		return C10kpa * float32(int(0x3FF&counts) - C10min), nil
	} else {
		return 0, bitsErr
	}
}

func HgInchesCounts(hgInches float32, bits int) (int, error) {
	if bits == 8 {
		return int(float32(C8min) + 0.5 + hgInches/C8hg), nil
	} else if bits == 10 {
		return int(float32(C10min) + 0.5 + hgInches/C10hg), nil
	} else {
		return 0, bitsErr
	}
}

func KPaCounts(kpa float32, bits int) (int, error) {
	if bits == 8 {
		return int(C8min + 0.5 + kpa/C8kpa), nil
	} else if bits == 10 {
		return int(C10min + 0.5 + kpa/C10kpa), nil
	} else {
		return 0, bitsErr
	}
}
