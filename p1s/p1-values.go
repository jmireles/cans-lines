package p1s

import (
	"strconv"
)

func ValuesPhasesInts(data []byte) (a, b, c, d uint16) {
	if len(data) >= 8 {
		a = uint16(data[0])<<8 + uint16(data[1])
		b = uint16(data[2])<<8 + uint16(data[3])
		c = uint16(data[4])<<8 + uint16(data[5])
		d = uint16(data[6])<<8 + uint16(data[7])
	}
	return
}

func ValuesPhasesBytesSim(a, b, c, d uint16) []byte {
	return []byte{
		byte(a >> 8), byte(a),
		byte(b >> 8), byte(b),
		byte(c >> 8), byte(c),
		byte(d >> 8), byte(d),
	}
}

func ValuesLevelsInts(data []byte) (vh, vl, ppm uint16) {
	if len(data) >= 6 {
		vh = uint16(data[0])<<8 + uint16(data[1])
		vl = uint16(data[2])<<8 + uint16(data[3])
		ppm = uint16(data[4])<<8 + uint16(data[5])
	}
	return
}

func ValuesLevelsHgInches(data []byte) (vh string, vl string, ppm string) {
	h, l, p := ValuesLevelsInts(data)
	vh = strconv.Itoa(int(h))
	vl = strconv.Itoa(int(l))
	// any: divide ppm counts over 10 and present float 1 decimal:
	ppm = strconv.FormatFloat(float64(float32(p)/10.0), 'f', 1, 32)
	return
}

func ValuesLevelsBytesSim(vh, vl, ppm uint16) []byte {
	return []byte{
		byte(vh >> 8), byte(vh),
		byte(vl >> 8), byte(vl),
		byte(ppm >> 8), byte(ppm),
		0, 0,
	}
}

type ValuesSampler struct {
	n     int
	front []byte
	rear  []byte
}

const samples = 120

func NewValuesSampler() *ValuesSampler {
	return &ValuesSampler{
		front: make([]byte, samples),
		rear:  make([]byte, samples),
	}
}

// Resp accepts data to feed front/rear buffers to complete a Waveforms.
// When data is size 8, the buffers are updated:
//  f[p+0], f[p+1], f[p+2], f[p+3] = data[0], data[2], data[4], data[6]
//  r[p+0], r[p+1], r[p+2], r[p+3] = data[1], data[3], data[5], data[7]
//  p += 4
// where f=front, r=rear and p is the current pointer (up to 120).
// Waveforms (not nil) are returned in two conditions:
// First, when data[0] equals 245, returning:
//	[][]byte{ f[:p], r[:p] }
// Second when p reaches 120 or more, returning:
//	[][]byte{ f[:120], r[:120] }
// After a complete waveform is returned the pointer is reset to 0 so
// this method continues feeding front/rear buffer to look for a new Waveforms.
func (s *ValuesSampler) Resp(data []byte) [][]byte {
	if len(data) < 8 {
		return nil
	}
	if data[0] == 245 {
		max := s.n
		s.n = 0
		return [][]byte{
			s.front[:max],
			s.rear[:max],
		}
	}
	if s.n+3 < samples {
		s.front[s.n+0] = data[0]
		s.front[s.n+1] = data[2]
		s.front[s.n+2] = data[4]
		s.front[s.n+3] = data[6]
		s.rear[s.n+0] = data[1]
		s.rear[s.n+1] = data[3]
		s.rear[s.n+2] = data[5]
		s.rear[s.n+3] = data[7]
		s.n += 4
	}
	if s.n >= samples {
		// response calling updates pages by return
		s.n = 0
		return [][]byte{
			s.front[:],
			s.rear[:],
		}
	}
	return nil
}
