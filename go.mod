module gitlab.com/jmireles/cans-lines

go 1.17

require (
	gitlab.com/jmireles/cans-base v0.0.0-20221026212030-68f3fbcc089e
	gitlab.com/jmireles/nav v0.0.0-20220414153102-07819c91ef04
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
