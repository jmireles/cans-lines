package i4s

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/i4"

	"gitlab.com/jmireles/cans-lines"
)

var (
	ReadWriteMap lines.ReadWriteMap
	ReadOnlyMap  lines.ReadOnlyMap
)

func init() {
	ReadWriteMap = lines.ReadWriteMap(map[base.Get]*lines.ReadWrite{
		i4.Get03_Config: lines.NewReadWrite(i4.Set04_Config, i4.Sol03_Config), // cfg 1
	})
	ReadOnlyMap = lines.ReadOnlyMap(map[base.Get]*lines.ReadOnly{
		base.Get00_Version: lines.NewReadOnly(base.Sol00_Version),
		base.Get01_Serial:  lines.NewReadOnly(base.Sol31_Serial),
		base.Get02_Status:  lines.NewReadOnly(base.Sol02_Status),
	})
}

// Masks returns the solicited response and mask for given get/set command
func Masks(comm byte) (sol base.Sol, masks []byte, ok bool) {
	switch comm {
	default:
		return
	}
}

type Type byte

const (
	TypeNone         Type = 0
	TypeEnter        Type = 1
	TypeExit         Type = 2
	TypePreWash      Type = 8
	TypeWash         Type = 3
	TypeVacuumPump   Type = 4
	TypeSafetySwitch Type = 5
	TypeNewDay       Type = 6
	TypeNewLot       Type = 7
	TypeStall        Type = 15
)

func (t Type) UnsolJson(sb *strings.Builder) {
	sb.WriteString(`{"u":"` + t.String() + `"}`)
}

func (t Type) String() string {
	switch t {
	case TypeNone:
		return typeNone
	case TypeEnter:
		return typeEnter
	case TypeExit:
		return typeExit
	case TypePreWash:
		return typePreWash
	case TypeWash:
		return typeWash
	case TypeVacuumPump:
		return typeVacuumPump
	case TypeSafetySwitch:
		return typeSafetySwitch
	case TypeNewDay:
		return typeNewDay
	case TypeNewLot:
		return typeNewLot
	case TypeStall:
		return typeStall
	default:
		return "?"
	}
}


type State string

const (
	StateNone   State = "none"
	StateClosed State = "closed"
	StateOpen   State = "open"
	StateOn     State = "on"
	StateOff    State = "off"
	StateChange State = "change"
	StateFirst  State = "first"
	StateSecond State = "second"
)

var Types = []string{
	typeNone,               // 1 none
	typeEnter, typeExit, // 2 gates
	typePreWash, typeWash, // 2 washes
	typeVacuumPump,         // 1 vacuum pump
	typeSafetySwitch,       // 1 safety switch
	typeNewDay, typeNewLot, // 2 news
	typeStall, // 1 swing stall
}

const (
	typeNone         = "none"
	typeEnter        = "enter"
	typeExit         = "exit"
	typePreWash      = "preWash"
	typeWash         = "wash"
	typeVacuumPump   = "vacuumPump"
	typeSafetySwitch = "safetySwitch"
	typeNewDay       = "newDay"
	typeNewLot       = "newLot"
	typeStall        = "stall"
)

func NewType(typeS string) (t Type, err error) {
	switch typeS {
	case typeNone:
		t = TypeNone
	case typeEnter:
		t = TypeEnter
	case typeExit:
		t = TypeExit
	case typePreWash:
		t = TypePreWash
	case typeWash:
		t = TypeWash
	case typeVacuumPump:
		t = TypeVacuumPump
	case typeSafetySwitch:
		t = TypeSafetySwitch
	case typeNewDay:
		t = TypeNewDay
	case typeNewLot:
		t = TypeNewLot
	case typeStall:
		t = TypeStall
	default:
		err = fmt.Errorf("Unknown type")
	}
	return
}

type Input struct {
	Zone  byte
	Logic bool
	Value bool
}

func NewInput(zone, logic, value string) (input *Input, err error) {
	if z, e := strconv.Atoi(zone); e != nil {
		err = fmt.Errorf("zone wrong format")
	} else if z < 0 || z > 0x0F {
		err = fmt.Errorf("zone out of range")
	} else {
		input = &Input{Zone: byte(z)}
		switch logic {
		case "true", "True", "1":
			input.Logic = true
		case "false", "False", "0":
			input.Logic = false
		default:
			err = fmt.Errorf("logic wrong format")
			return
		}
		switch value {
		case "true", "True", "1":
			input.Value = true
		case "false", "False", "0":
			input.Value = false
		default:
			err = fmt.Errorf("value wrong format")
			return
		}
	}
	return
}

func (i *Input) UnsolJson(type_ Type, sb *strings.Builder) {
	/*state := type_.State(i.Logic, i.Value)
	sb.WriteString(`{`)
	sb.WriteString(`"u":"` + type_.String() + `"`)
	sb.WriteString(`,"zone":` + strconv.Itoa(int(i.Zone)))
	sb.WriteString(`,"state":"` + string(state) + `"`)
	sb.WriteString(`}`)*/
}

type Swing struct {
	Smartlite byte
	Button    byte
}

func NewSwing(params url.Values) (swing *Swing, err error) {
	if smartlite, e := strconv.Atoi(params.Get("smartlite")); e != nil {
		err = fmt.Errorf("smartlite invalid number")
	} else if smartlite < 0 || smartlite > 255 {
		err = fmt.Errorf("smartlite out of range (0,255)")
	} else if button, e := strconv.Atoi(params.Get("button")); e != nil {
		err = fmt.Errorf("button invalid number")
	} else if button < 1 || button > 4 {
		err = fmt.Errorf("button out of range (1,4)")
	} else {
		swing = &Swing{byte(smartlite), byte(button)}
	}
	return
}

func (s *Swing) UnsolJson(type_ Type, sb *strings.Builder) {
	sb.WriteString(`{`)
	sb.WriteString(`"u":"` + type_.String() + `"`)
	sb.WriteString(`,"smartlite":` + strconv.Itoa(int(s.Smartlite)))
	sb.WriteString(`,"button":` + strconv.Itoa(int(s.Button)))
	sb.WriteString(`}`)
}
