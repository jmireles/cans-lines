package i4s

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/i4"

	"gitlab.com/jmireles/cans-lines/unsols"
)

func TestUnsol(t *testing.T) {
	tt := base.Test{t}
	src, _ := from.NewSrc(3, from.I4, 13)
	// well formed
	restart, _  := from.NewCanUnsol(src, base.Unsol01_Restart, nil)
	
	// change1 for gates enter,exit and wash and prewash on,off:
	changeZone := func(t Type, zone, bits byte) *from.Can {
		can, _  := from.NewCanUnsol(src, i4.Unsol05_Change, []byte{
			byte(t), 
			byte((zone << 4) + bits),
		})
		return can
	}
	changeZoneStall := func(zone, bits, smartlite, button byte) *from.Can {
		can, _ := from.NewCanUnsol(src, i4.Unsol05_Change, []byte{
			byte(TypeStall),
			byte((zone << 4) + bits),
			smartlite,
			button,
		})
		return can
	}
	// enter,exit values:
	closeA, closeB := byte(0x01), byte(0x02) // bits 2,1 different
	openA, openB   := byte(0x00), byte(0x03) // bits 2,1 equal
	// prewash, wash values:
	onA, onB   := byte(0x01), byte(0x02) // bits 2,1 different
	offA, offB := byte(0x00), byte(0x03) // bits 2,1 equal
	// stall swing
	firstA, firstB   := byte(0x01), byte(0x02) // bits 2,1 different
	secondA, secondB := byte(0x00), byte(0x03) // bits 2,1 equal

	// valid events:
	for can, json := range map[*from.Can]string {
		restart: "{3:{8:{13:{1:1}}}}",
		changeZone(TypeNone, 0, 0): "", // none returns nothing

		// GATES states 1,2 closed/open

		// enter zones: 16-31.
		changeZone(TypeEnter, 0, closeA): "{3:{8:{13:{16:1}}}}",
		changeZone(TypeEnter, 0, openA):  "{3:{8:{13:{16:2}}}}",
		changeZone(TypeEnter, 1, closeB): "{3:{8:{13:{17:1}}}}",
		changeZone(TypeEnter, 15, openB): "{3:{8:{13:{31:2}}}}",
		// exit zones: 32-47. states: 1,2.
		changeZone(TypeExit, 0, closeA):  "{3:{8:{13:{32:1}}}}",
		changeZone(TypeExit, 15, closeB): "{3:{8:{13:{47:1}}}}",

		// SIGNALS states 3,4 on/off
		
		// preWash zones: 48-63.
		changeZone(TypePreWash, 0, onA):   "{3:{8:{13:{48:3}}}}",
		changeZone(TypePreWash, 0, offA):  "{3:{8:{13:{48:4}}}}",
		changeZone(TypePreWash, 1, onB):   "{3:{8:{13:{49:3}}}}",
		changeZone(TypePreWash, 15, offB): "{3:{8:{13:{63:4}}}}",
		// wash zones: 64-79:
		changeZone(TypeWash, 0, onA):   "{3:{8:{13:{64:3}}}}",
		changeZone(TypeWash, 15, offB): "{3:{8:{13:{79:4}}}}",
		// vacuum pump zones: 80-95.
		changeZone(TypeVacuumPump, 0, onA):     "{3:{8:{13:{80:3}}}}",
		changeZone(TypeVacuumPump, 15, offA):   "{3:{8:{13:{95:4}}}}",
		// safety switch zones: 96-111.
		changeZone(TypeSafetySwitch, 0, onA):   "{3:{8:{13:{96:3}}}}",
		changeZone(TypeSafetySwitch, 15, offA): "{3:{8:{13:{111:4}}}}",
		
		// NEW state 3,0 on/none

		// new day zones: 112-127.
		changeZone(TypeNewDay, 0, onA):   "{3:{8:{13:{112:3}}}}",
		changeZone(TypeNewDay, 15, offB): "{3:{8:{13:{127:0}}}}",
		// new lot zones: 128,143.
		changeZone(TypeNewLot, 0, onB):   "{3:{8:{13:{128:3}}}}",
		changeZone(TypeNewLot, 15, offA): "{3:{8:{13:{143:0}}}}",

		// SWING states 5,6 first/second
		
		// swing zones: 144-159, state 5
		changeZoneStall(0, firstA, 0, 0):   "{3:{8:{13:{144:5,160:0}}}}",
		changeZoneStall(15, firstB, 0, 1):  "{3:{8:{13:{159:5,160:256}}}}",
		// swing zones: 144-159, state 6
		changeZoneStall(0, secondA, 1, 0):  "{3:{8:{13:{144:6,160:1}}}}",
		changeZoneStall(15, secondB, 1, 1): "{3:{8:{13:{159:6,160:257}}}}",
	} {
		resp, err := UnsolsFunc(can)
		tt.Ok(err)
		s, _ := unsols.RTJson(nil, resp) // filter is nil to pass everything
		tt.Equals(json, s)
	}

	// invalid events
	invalidUnsol2, _  := from.NewCanUnsol(src, base.Unsol(2), nil)
	invalidUnsol6, _  := from.NewCanUnsol(src, base.Unsol(6), nil) // is nil because rejected by lines-base
	invalidType, _    := from.NewCanUnsol(src, base.Unsol(5), nil)
	invalidType9, _   := from.NewCanUnsol(src, base.Unsol(5), []byte{ 9 })
	invalidType14, _  := from.NewCanUnsol(src, base.Unsol(5), []byte{ 14 })
	invalidType16, _  := from.NewCanUnsol(src, base.Unsol(5), []byte{ 16 })
	invalidType255, _ := from.NewCanUnsol(src, base.Unsol(5), []byte{ 255 })

	invalidZoneEnter, _  := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeEnter) })
	invalidZoneExit, _   := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeExit) })
	invalidZonePre, _    := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypePreWash) })
	invalidZoneWash, _   := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeWash) })
	invalidZoneVacuum, _ := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeVacuumPump) })
	invalidZoneSafety, _ := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeSafetySwitch) })
	invalidZoneDay, _    := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeNewDay) })
	invalidZoneLot, _    := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeNewLot) })

	invalidZoneStall, _  := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeStall) })
	invalidZoneStall0, _ := from.NewCanUnsol(src, base.Unsol(5), []byte{ byte(TypeStall),0 })

	for can, e := range map[*from.Can]error {
		invalidUnsol2:  unsols.ErrorInvalidUnsol,
		invalidUnsol6:  unsols.ErrorInvalidUnsol,

		invalidType:    unsols.ErrorInvalidType,
		invalidType9:   unsols.ErrorInvalidType,
		invalidType14:  unsols.ErrorInvalidType,
		invalidType16:  unsols.ErrorInvalidType,
		invalidType255: unsols.ErrorInvalidType,

		invalidZoneEnter:  unsols.ErrorInvalidZone,
		invalidZoneExit:   unsols.ErrorInvalidZone,
		invalidZonePre:    unsols.ErrorInvalidZone,
		invalidZoneWash:   unsols.ErrorInvalidZone,
		invalidZoneVacuum: unsols.ErrorInvalidZone,
		invalidZoneSafety: unsols.ErrorInvalidZone,
		invalidZoneDay:    unsols.ErrorInvalidZone,
		invalidZoneLot:    unsols.ErrorInvalidZone,
		invalidZoneStall:  unsols.ErrorInvalidZone,
		invalidZoneStall0: unsols.ErrorInvalidZone,
	} {
		resp, err := UnsolsFunc(can)
		tt.Assert(err!=nil, "nil error for %v", resp)
		tt.Equals(err, e)
	}
}

