package i4s

import (
	"net/url"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/i4"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/unsols"
)

// UnsolsFunc implements lines.RespFunc
func UnsolsFunc(req *from.Can) (resp *lines.Resp, err error) {
	resp, err = unsols.Func(req)
	if err != nil {
		return
	}
	switch *req.Unsol {

	case base.Unsol01_Restart:
		// map with single status key with value exactly 1
		resp.Map = unsols.MapRestart

	case i4.Unsol05_Change: 
		extra := resp.Can.Extra
		if len(extra) == 0 {
			err = unsols.ErrorInvalidType
			return
		}

		switch extra[0] {
		case 0: 
			return // none change
		case 1: 
			err = unsolZone(resp, TypeEnter)
		case 2:
			err = unsolZone(resp, TypeExit)
		case 3:
			err = unsolZone(resp, TypeWash)
		case 8:
			err = unsolZone(resp, TypePreWash)
		case 4:
			err = unsolZone(resp, TypeVacuumPump)
		case 5:
			err = unsolZone(resp, TypeSafetySwitch)
		case 6:
			err = unsolZone(resp, TypeNewDay)
		case 7:
			err = unsolZone(resp, TypeNewLot)
		case 15:
			err = unsolZoneStall(resp)
		default:
			err = unsols.ErrorInvalidType
		}
	
	default:
		err = unsols.ErrorInvalidUnsol
	}
	return
}

// unsolState set a single key with a value off/on
func unsolZone(resp *lines.Resp, t Type) error {
	extra := resp.Can.Extra
	if len(extra) < 2 {
		return unsols.ErrorInvalidZone
	}
	resp.Map = lines.Map{}
	zone := lines.Key(extra[1] >> 4) // pos 1
	val := unsolValue(t, extra[1])
	switch t {
	case TypeEnter:
		resp.Map[unsols.KeyZonesEnter + zone] = val

	case TypeExit:
		resp.Map[unsols.KeyZonesExit + zone] = val
	
	case TypePreWash:
		resp.Map[unsols.KeyZonesPrewash + zone] = val
	
	case TypeWash:
		resp.Map[unsols.KeyZonesWash + zone] = val
	
	case TypeVacuumPump:
		resp.Map[unsols.KeyZonesVacuumPump + zone] = val
	
	case TypeSafetySwitch:
		resp.Map[unsols.KeyZonesSafetySwitch + zone] = val
	
	case TypeNewDay:
		resp.Map[unsols.KeyZonesNewDay + zone] = val
	
	case TypeNewLot:		
		resp.Map[unsols.KeyZonesNewLot + zone] = val
	
	default:
		return unsols.ErrorInvalidType
	}
	return nil
}

func unsolZoneStall(resp *lines.Resp) error {
	extra := resp.Can.Extra
	if len(extra) < 4 {
		return unsols.ErrorInvalidZone
	}
	zone := lines.Key(extra[1] >> 4) // pos 1
	val := unsolValue(TypeStall, extra[1])
	smartlite := extra[2] // pos 2
	button := extra[3]    // pos 3

	resp.Map = lines.Map{}
	resp.Map[unsols.KeyZonesStall + zone] = val
	resp.Map[unsols.KeySwing] = lines.NewValue(0, []byte{
		smartlite,
		button,
	})
	return nil
}


func unsolValue(t Type, extra byte) lines.Value {
	logic := extra & 0x02 != 0
	value := extra & 0x01 != 0
	switch t {
	case TypeNone:
		return unsols.ValueNone
	// Two types of gate
	case TypeEnter, TypeExit:
		if logic != value {
			return unsols.ValueClose
		} else {
			return unsols.ValueOpen
		}
	// Four types of states
	case TypePreWash, TypeWash, TypeVacuumPump, TypeSafetySwitch:
		if logic != value {
			return unsols.ValueOn
		} else {
			return unsols.ValueOff
		}
	// Two types of event
	case TypeNewDay, TypeNewLot:
		if logic != value {
			return unsols.ValueOn
		} else {
			return unsols.ValueNone
		}
	// One type of swing
	case TypeStall:
		if logic != value {
			return unsols.ValueFirst
		} else {
			return unsols.ValueSecond
		}
	default:
		return unsols.ValueNone
	}
}






















func UnsolJson(comm base.Unsol, data []byte) string {
	var sb strings.Builder
	switch comm {
	
	case base.Unsol01_Restart:
		sb.WriteString(`{"u":"O"}`)
	
	case i4.Unsol05_Change:
		if change := NewUnsolChange(data); change == nil {
			sb.WriteString(`{"u":null}`)
		} else {
			change.Json(&sb)
		}
	
	default:
		sb.WriteString(`{}`)
	}
	return sb.String()
}

type Change struct {
	Type  Type
	Input *Input
	Swing *Swing
}

func NewChange(typeS, zone, logic, value string, params url.Values) (*Change, error) {
	if t, err := NewType(typeS); err != nil {
		// 1 Invalid type
		return nil, err
	} else if t == TypeNone {
		// 2 Valid none type
		return &Change{Type: t}, nil
	} else if input, err := NewInput(zone, logic, value); err != nil {
		// 3 Invalid input zone, logic and value
		return nil, err
	} else {
		if t != TypeStall {
			// 4 valid other than swing type
			return &Change{Type: t, Input: input}, nil
		} else if swing, err := NewSwing(params); err != nil {
			// 5 Invalid swing params
			return nil, err
		} else {
			// 6 valid swing
			return &Change{Type: t, Input: input, Swing: swing}, nil
		}
	}
}

func (c *Change) Unsol() (base.Unsol, []byte) {
	bytes := []byte{
		byte(c.Type), // pos 0
	}
	if c.Input != nil {
		flags := byte(c.Input.Zone) << 4
		if c.Input.Logic {
			flags |= 0x02
		}
		if c.Input.Value {
			flags |= 0x01
		}
		bytes = append(bytes, flags) // pos 1
	}
	if c.Swing != nil {
		bytes = append(bytes, c.Swing.Smartlite) // pos 2
		bytes = append(bytes, c.Swing.Button)    // pos 3
	}
	return i4.Unsol05_Change, bytes
}

type UnsolChange struct {
	Change *Change
}

func NewUnsolChange(data []byte) *UnsolChange {
	if data == nil {
		return nil
	} else if len(data) < 1 {
		return nil // no data[0] with type
	}
	t := Type(data[0]) // pos
	change := &Change{Type: t}
	if t == TypeNone {
		return &UnsolChange{change}
	} else if len(data) < 2 {
		return nil // no data[1] with zone,logic,value
	}
	// input for all types except none
	zone := data[1] >> 4       // pos 1
	logic := data[1]&0x02 != 0 // pos 1
	value := data[1]&0x01 != 0 // pos 1
	change.Input = &Input{Zone: zone, Logic: logic, Value: value}
	if t == TypeStall {
		if len(data) < 4 {
			return nil // no data[2,3] with smartlite,button
		}
		// swing for type stall
		smartlite := data[2] // pos 2
		button := data[3]    // pos 3
		change.Swing = &Swing{Smartlite: smartlite, Button: button}
	}
	return &UnsolChange{change}
}

func (u *UnsolChange) Json(sb *strings.Builder) {
	c := u.Change
	if c.Input != nil {
		c.Input.UnsolJson(c.Type, sb)
	} else if c.Swing != nil {
		c.Swing.UnsolJson(c.Type, sb)
	} else {
		c.Type.UnsolJson(sb)
	}
}

func (u *UnsolChange) Type() Type {
	return u.Change.Type
}

func (u *UnsolChange) Gate() (zone byte, open bool, t Type, ok bool) {
	t = u.Change.Type
	switch t {
	case TypeEnter, TypeExit:
		i := u.Change.Input
		if i == nil {
			ok = false
			return
		}
		zone = i.Zone
		open = i.Value == i.Logic
		ok = true
	}
	return
}

func (u *UnsolChange) Wash() (zone byte, on bool, t Type, ok bool) {
	t = u.Change.Type
	switch t {
	case TypePreWash, TypeWash:
		i := u.Change.Input
		if i == nil {
			ok = false
			return
		}
		zone = i.Zone
		on = i.Value != i.Logic
		ok = true
	}
	return
}
