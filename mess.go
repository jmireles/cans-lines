package lines

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/cfg"
)

type mess struct {
	tx0   cfg.MessTxFunc
	debug string
}

func (m mess) ProfileReq(params map[string]int) []byte {
	if m.tx0 != nil {
		tx := m.tx0(params)
		if m.debug != "" {
			fmt.Printf("%s 0 ProfTx %v\n", m.debug, tx)
		}
		return tx
	}
	return nil
}

//
// mess1 is configurations which require a single 8-bytes message.
//
type mess1 struct {
	rx1 cfg.MessRxFunc
	tx1 cfg.MessTxFunc
	mess
}

func NewMess1(rx1 cfg.MessRxFunc, tx1 cfg.MessTxFunc, tx0 cfg.MessTxFunc, debug string) mess1 {
	return mess1{
		rx1,
		tx1,
		mess{tx0, debug},
	}
}

func (m mess1) SimTx(params url.Values) (tx []byte, err error) {
	tx = m.tx1(params)
	if m.debug != "" {
		fmt.Printf("%s 1 SimTx %v %v\n", m.debug, params, tx)
	}
	return
}

func (m mess1) Resp(resp byte, part []byte, whole *[]byte) bool {
	complete := m.cfg8To9(resp, part, whole)
	if m.debug != "" {
		fmt.Printf("%s 2 resp %v %v %v %v\n", m.debug, resp, part, *whole, complete)
	}
	return complete
}

func (m mess1) Read(rx []byte, prefs *nav.Prefs) (*cfg.Params, error) {
	if b, err := single9To8(rx); err != nil {
		return nil, err
	} else {
		if m.debug != "" {
			fmt.Printf("%s 3 read %v %v\n", m.debug, rx, b)
		}
		p := cfg.NewParams(prefs)
		m.rx1(p, b)
		return p, nil
	}
	//return nil, nil
}

func (m mess1) Save(params map[string]int) ([][]byte, error) {
	bytes := [][]byte{
		m.tx1(params),
	}
	if m.debug != "" {
		fmt.Printf("%s 4 save %v %v\n", m.debug, params, bytes)
	}
	return bytes, nil
}

func (m mess1) SimRx(line, set, resp byte, bytes [][]byte) (*cfg.Params, error) {
	var rx []byte
	for _, twelve := range bytes {
		if eight, err := sim12To8(twelve, line, set); err != nil {
			return nil, err
		} else if m.cfg8To9(resp, eight, &rx) {
			return m.Read(rx, nil) // prefs=nil means simulation params
		}
	}
	return nil, errors.New("Invalid rx size:" + strconv.Itoa(len(rx)))
}

// Cfg8To9 creates rx buffer and puts 9 bytes
// at first position command and the rest with data
// which should be 8 bytes in length
func (m mess1) cfg8To9(comm byte, data []byte, rx *[]byte) bool {
	if len(data) < 8 {
		return false
	}
	*rx = make([]byte, 9)
	(*rx)[0] = comm
	copy((*rx)[1:], data)
	return true
}

//
// messN is configurations which require four 8-bytes messages.
//
type messN struct {
	ids []byte
	rxs []cfg.MessRxFunc
	txs []cfg.MessTxFunc
	mess
}

func NewMessN(ids []byte, rxs []cfg.MessRxFunc, txs []cfg.MessTxFunc, tx0 cfg.MessTxFunc, debug string) messN {
	n := len(ids)
	if n != 3 && n != 4 {
		panic(errors.New("ids length != 3 and != 4"))
	}
	if n != len(rxs) {
		panic(errors.New("sizes rxs != ids"))
	}
	if n != len(txs) {
		panic(errors.New("sizes txs != ids"))
	}
	return messN{
		ids,
		rxs,
		txs,
		mess{tx0, debug},
	}
}

// 1. When user clicks on old-cfg get buttons and a simulation page is around
// simulation is requested and this setSim is called four times with parameters by query
// setSim returns for each request 8 bytes converted to simulate real PM responses
func (m messN) SimTx(params url.Values) (tx []byte, err error) {
	// in simulations always n starts "0"
	switch len(m.ids) {
	case 3:
		switch params.Get("n") {
		case "0":
			tx = m.txs[0](params)
		case "1":
			tx = m.txs[1](params)
		case "2":
			tx = m.txs[2](params)
		default:
			err = errors.New("n not [0,1,2]")
		}
	case 4:
		switch params.Get("n") {
		case "0":
			tx = m.txs[0](params)
		case "1":
			tx = m.txs[1](params)
		case "2":
			tx = m.txs[2](params)
		case "3":
			tx = m.txs[3](params)
		default:
			err = errors.New("n not [0,1,2,3]")
		}
	}
	if m.debug != "" {
		fmt.Printf("txSim %s (%d) %v\n", m.debug, len(params), params)
		fmt.Printf("      %s (%d) %v\n", m.debug, len(tx), tx)
	}
	return
}

// 2. resp parses response parts (8 bytes each) and writes on whole buffer
//    Here four responses comprise a whole buffer of 33 bytes
//    Four responses should match same resp code (here = 0x03) and have marks 1,2,3,4
//    A complete response is send to a channel (the whole buffer)
//    When true is complete rt-pm p1ReqPostCfgRead is called which will call read (below).
func (m messN) Resp(resp byte, part []byte, all *[]byte) bool {
	var complete bool
	switch len(m.ids) {
	case 3:
		complete = m.cfg8To25(resp, part, all)
	case 4:
		complete = m.cfg8To33(resp, part, all)
	}
	if m.debug != "" {
		fmt.Printf("resp  %s (%d) %v\n", m.debug, len(part), part)
		fmt.Printf("      %s (%d) %v all=%v\n", m.debug, len(*all), *all, complete)
	}
	return complete
}

// 3. Channel release of 33 bytes buffer is parsed here to
//    convert to params to be used in configuration edition page
func (m messN) Read(all []byte, prefs *nav.Prefs) (*cfg.Params, error) {

	var b []byte
	var err error
	switch len(m.ids) {
	case 3:
		if b, err = m.cfg25To21(all); err != nil {
			// TODO check get matches "3" ???
			return nil, err
		}
	case 4:
		if b, err = m.cfg33To28(all); err != nil {
			// TODO check get matches "3" ???
			return nil, err
		}
	}
	p := cfg.NewParams(prefs)
	for _, rx := range m.rxs {
		rx(p, b)
	}
	if m.debug != "" {
		fmt.Printf("read  %s (%d) %v\n", m.debug, len(all), all)
		fmt.Printf("      %s (%d) %v\n", m.debug, len(b), b)
		fmt.Printf("      %s %v\n", m.debug, p.String())
		fmt.Printf("------\n")
	}
	return p, nil
}

// 4. Parameters in above 3. can be edited.
//    Here are converted to four parts
func (m messN) Save(params map[string]int) ([][]byte, error) {
	bytes := make([][]byte, len(m.txs))
	for p, tx := range m.txs {
		bytes[p] = tx(params)
	}
	if m.debug != "" {
		fmt.Printf("save  %s %v\n", m.debug, params)
		fmt.Printf("      %s %v\n", m.debug, bytes)
	}
	return bytes, nil
}

// 5. When simulator receives saves (from 4 above), browser request
//    this server to get parameters from bytes. Here rxSim converts
//    and return parameters without i18n and fixed to imperial units
//    read (see 3. above) is called
func (m messN) SimRx(line, set, resp byte, bytes [][]byte) (*cfg.Params, error) {
	var all []byte
	for _, twelve := range bytes {
		if eight, err := sim12To8(twelve, line, set); err != nil {
			return nil, err
		} else {
			var complete bool
			switch len(m.ids) {
			case 3:
				complete = m.cfg8To25(resp, eight, &all)
			case 4:
				complete = m.cfg8To33(resp, eight, &all)
			}
			if complete {
				if m.debug != "" {
					fmt.Printf("SimRx %s set=%d resp=%d %v\n", m.debug, set, resp, bytes)
					fmt.Printf("      %s %v\n", m.debug, all)
				}
				return m.Read(all, nil) // prefs=nil means simulation params
			}
		}
	}
	return nil, errors.New("Invalid all size:" + strconv.Itoa(len(all)))
}

func (m messN) cfg8To25(get byte, part []byte, all *[]byte) bool {
	if len(part) < 8 {
		return false
	}
	if part[0] == m.ids[0] { // 1st part and marked
		*all = make([]byte, 25)
		(*all)[0] = get
		copy((*all)[1:], part)
	}
	if len(*all) == 25 && (*all)[0] == get { // first part already received
		if part[0] == m.ids[1] {
			copy((*all)[9:], part) // 2nd part received and marked
		} else if part[0] == m.ids[2] {
			copy((*all)[17:], part) // 3rd part received and marked
		}
		// check all parts were received
		if (*all)[1] != m.ids[0] {
			return false // 1st not received yet (maybe redundant here)
		}
		if (*all)[9] != m.ids[1] {
			return false // 2nd part not received yet
		}
		if (*all)[17] != m.ids[2] {
			return false // 3rd part not received yet
		}
		return true // four parts received and marked ok
	}
	return false
}

func (m messN) cfg8To33(get byte, part []byte, all *[]byte) bool {
	if len(part) < 8 {
		return false
	}
	if part[0] == m.ids[0] { // 1st part and marked
		*all = make([]byte, 33)
		(*all)[0] = get
		copy((*all)[1:], part)
	}
	if len(*all) == 33 && (*all)[0] == get { // first part already received
		if part[0] == m.ids[1] {
			copy((*all)[9:], part) // 2nd part received and marked
		} else if part[0] == m.ids[2] {
			copy((*all)[17:], part) // 3rd part received and marked
		} else if part[0] == m.ids[3] {
			copy((*all)[25:], part) // 4th part received and marked
		}
		// check all parts were received
		if (*all)[1] != m.ids[0] {
			return false // 1st not received yet (maybe redundant here)
		}
		if (*all)[9] != m.ids[1] {
			return false // 2nd part not received yet
		}
		if (*all)[17] != m.ids[2] {
			return false // 3rd part not received yet
		}
		if (*all)[25] != m.ids[3] {
			return false // 4th part not received yet
		}
		return true // four parts received and marked ok
	}
	return false
}

// Converts 33 bytes array:
// [-][a][A][B][C][D][E][F][G]
//    [b][H][I][J][K][L][M][N]
//    [c][O][P][Q][R][S][T][U]
//    [d][V][W][X][Y][Z][a][b]
//
// Into 28 bytes array:
// [A][B][C][D][E][F][G]
// [H][I][J][K][L][M][N]
// [O][P][Q][R][S][T][U]
// [V][W][X][Y][Z][a][b]
//
// If and only if bytes [a][b][c][d] are found exactly with values
// a, b, c, d. Otherwise an error is returned

func (m messN) cfg25To21(rx []byte) ([]byte, error) {
	if len(rx) != 25 {
		return nil, errors.New("rx len != 25")
	}
	if rx[1] != m.ids[0] {
		return nil, errors.New("rx 1st missing")
	}
	if rx[9] != m.ids[1] {
		return nil, errors.New("rx 2nd missing")
	}
	if rx[17] != m.ids[2] {
		return nil, errors.New("rx 3rd missing")
	}
	out := make([]byte, 21)
	copy(out[0:], rx[2:9])
	copy(out[7:], rx[10:17])
	copy(out[14:], rx[18:25])
	return out, nil
}

func (m messN) cfg33To28(rx []byte) ([]byte, error) {
	if len(rx) != 33 {
		return nil, errors.New("rx len != 33")
	}
	if rx[1] != m.ids[0] {
		return nil, errors.New("rx 1st missing")
	}
	if rx[9] != m.ids[1] {
		return nil, errors.New("rx 2nd missing")
	}
	if rx[17] != m.ids[2] {
		return nil, errors.New("rx 3rd missing")
	}
	if rx[25] != m.ids[3] {
		return nil, errors.New("rx 4th missing")
	}
	out := make([]byte, 28)
	copy(out[0:], rx[2:9])
	copy(out[7:], rx[10:17])
	copy(out[14:], rx[18:25])
	copy(out[21:], rx[26:33])
	return out, nil
}

func single9To8(rx []byte) ([]byte, error) {
	if len(rx) != 9 {
		return nil, errors.New("rx len != 9")
	}
	out := make([]byte, 8)
	copy(out[:], rx[1:])
	return out, nil
}

// sim12To8 returns last 8 bytes from a 12 byte array
// iff bytes[0] matches line and bytes[3] matches command
// bytes[1] is for id source
// bytes[2] is id destination (always zero computer)
func sim12To8(bytes []byte, line byte, comm byte) ([]byte, error) {
	if len(bytes) != 12 {
		return nil, errors.New("len(bytes) != 12")
	}
	if bytes[0] != line {
		return nil, errors.New("bytes[0] != line " + strconv.Itoa(int(line)))
	}
	if bytes[3] != comm {
		return nil, errors.New("bytes[3] != command " + strconv.Itoa(int(comm)))
	}
	eight := bytes[4:]
	return eight, nil
}
