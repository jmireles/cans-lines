package boxes

import (
	"bytes"
	"fmt"
	"sort"
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/p1"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/all"
	"gitlab.com/jmireles/cans-lines/unsols"
)

var (
	testIds = []byte { // unsorted ids
		17,13,8,9,10,11,7,6,
	}
	testKeys = []string { // sorted keys
		"3106","3107","3108","3109","310a","310b","310d","3111",
	}
	n2, _ = base.NewNet(2)
	n3, _ = base.NewNet(3)
	n2pm7, _ = from.NewSrc(2, from.P1, 7)
	n2pm8, _ = from.NewSrc(2, from.P1, 8)
	n3pm7, _ = from.NewSrc(3, from.P1, 7)
	n3pm8, _ = from.NewSrc(3, from.P1, 8)

	extras = [][]byte{
		[]byte{1, 0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71},
		[]byte{2, 0x12, 0x22, 0x32, 0x42, 0x52, 0x62, 0x72},
		[]byte{3, 0x13, 0x23, 0x33, 0x43, 0x53, 0x63, 0x73},
		[]byte{4, 0x14, 0x24, 0x34, 0x44, 0x54, 0x64, 0x74},
	}
)

func TestBoxes(t *testing.T) {
	tt := base.Test{t}
	net := byte(3)
	line := from.P1
	boxes, err := NewBoxes(net, line, testIds)
	tt.Ok(err)
	sorted := testIds[:]
	sort.Slice(sorted, func(i, j int) bool {
		return sorted[i] < sorted[j]
	})
	for i, id := range sorted {
		box, ok := boxes[id]
		tt.Assert(ok, "Missing key for id:%d", id)
		tt.Equals(testKeys[i], box.Key)
		tt.Assert(box.Src != nil, "Missing src for id:%d", id)
		tt.Equals(net, byte(box.Src.Net))
		tt.Equals(line, box.Src.Line)
		tt.Equals(id, box.Src.Box)
	}
	for _, box := range []byte { 
		1, 4, 18, 59, 200, 
	} {
		_, ok := boxes[box]
		tt.Assert(!ok, "Unexpected key for box:%d", box)
	}
	// reject repeated ids
	boxes, err = NewBoxes(net, line, []byte{ 1,1,2,2,3,3,3,4,4,5 })
	tt.Ok(err)
	tt.Equals(5, len(boxes))
}

func TestRespSols(t *testing.T) {
	tt := base.Test{t}
	sols := NewRespSols(all.SolsFunc)

	box := NewBox(n2pm7)
	// readonly version write
	version := []byte{ 44 }
	err := sols.Write(box, base.Sol00_Version, version)
	tt.Ok(err)
	// 1 readonly version stored
	err = testLen(sols, 1)
	tt.Ok(err)
	resp := []byte{ 0x10,0,7,0 }
	for key, sol := range map[string][]byte {
			"210000700": append(resp, version...),
		} {
		err := testStored(sols, key, sol)
		tt.Ok(err)
	}
	// readonly version get response
	get, err := sols.Get(box, base.Get00_Version, nil)
	tt.Ok(err)
	for i, exp := range [][]byte {
		append(resp, version...),
	} {
		err := testGet(get[i], exp)
		tt.Ok(err)
	}
	// readonly serial write
	serial := []byte { 1,2,3 }
	err = sols.Write(box, base.Sol31_Serial, serial)
	tt.Ok(err)
	// 1 readonly serial stored (1+1)
	err = testLen(sols, 2)
	tt.Ok(err)
	resp = []byte{ 0x10,0,7,31 }
	for key, exp := range map[string][]byte {
			"21000071f": append(resp, serial...),
		} {
		err := testStored(sols, key, exp)
		tt.Ok(err)
	}
	// readonly serial gets response
	gets, err := sols.Get(box, base.Get01_Serial, nil)
	tt.Ok(err)
	for i, exp := range [][]byte {
		append(resp, serial...),
	} {
		err := testGet(gets[i], exp)
		tt.Ok(err)
	}

	// readwrite cfg set
	for _, extra := range extras {
		_, err := sols.Set(box, p1.Set04_Config, extra)
		tt.Ok(err)
	}
	// 4 readwrite cfg stored (1+1+4)
	err = testLen(sols, 6)
	tt.Ok(err)
	baseB := []byte{ 0x10,0,7,3 }
	for key, sol := range map[string][]byte {
			"21000070301": append(baseB, extras[0]...),
			"21000070302": append(baseB, extras[1]...),
			"21000070303": append(baseB, extras[2]...),
			"21000070304": append(baseB, extras[3]...),
		} {
		err := testStored(sols, key, sol)
		tt.Ok(err)
	}
	// readwrite cfg gets response
	gets, err = sols.Get(box, p1.Get03_Config, nil)
	tt.Ok(err)
	for i, extra := range extras {
		err := testGet(gets[i], append(baseB, extra...))
		tt.Ok(err)
		//t.Log(gets[i])
	}
	// Set solicited serial to all
	boxes, _ := NewBoxes(3, from.P1, testIds)
	for _, id := range testIds {
		box, _ := boxes[id]
		serial := []byte { 
			byte(  1 + id), 
			byte(100 + id), 
			byte(199 + id),
		}
		err := sols.Write(box, base.Sol31_Serial, serial)
		tt.Ok(err)
	}
	// 8 serials stored (1+1+4+8)
	err = testLen(sols, 14)
	tt.Ok(err)
	// Check serial sols
	// key format NLL00BBCC N=net LL=line, BB=box, CC=command
	for key, exp := range map[string][]byte { 
		"31000061f": []byte{ 16, 0,  6, 31,  7, 106, 205 },
		"31000071f": []byte{ 16, 0,  7, 31,  8, 107, 206 },
		"31000081f": []byte{ 16, 0,  8, 31,  9, 108, 207 },
		"31000091f": []byte{ 16, 0,  9, 31, 10, 109, 208 },
		"310000a1f": []byte{ 16, 0, 10, 31, 11, 110, 209 },
		"310000b1f": []byte{ 16, 0, 11, 31, 12, 111, 210 },
		"310000d1f": []byte{ 16, 0, 13, 31, 14, 113, 212 },
		"31000111f": []byte{ 16, 0, 17, 31, 18, 117, 216 },
	} {
		err := testStored(sols, key, exp)
		tt.Ok(err)
	}
}

func TestRespUnsols(t *testing.T) {
	tt := base.Test{t}
	i47, _ := from.NewSrc(3, from.I4, 7)
	ne3, _ := from.NewSrc(4, from.P1, 7)
	for comm, resp := range map[interface{}]Resp {
		base.Sol00_Version:   NewRespSol(n3pm7, base.Sol00_Version),
		base.Unsol01_Restart: NewRespUnsol(n3pm7, base.Unsol01_Restart),
		base.Sol02_Status:    NewRespSol(n3pm7, base.Sol02_Status),
	} {
		oks := []Resp {
			NewRespNet(n3pm7.Net), // without line,box,sol
			NewRespLine(n3pm7.Net, n3pm7.Line), // without box,sol
			NewRespSrc(n3pm7), // without sol
			resp, // same all
		}
		for _, ok := range oks {
			if !resp.Match(ok) {
				t.Fatalf("Don't match base:%s ok:%s", resp, ok)
			}
		}
		bads := []Resp {
			NewRespSrc(n3pm8), // bad box
			NewRespLine(n3pm7.Net, i47.Line), // bad line
			NewRespNet(ne3.Net), // bad net
		}
		switch comm.(type) {
		case base.Sol:
			sol := comm.(base.Sol)
			bads = append(bads,
				NewRespSol(ne3, sol), // bad net, same sol
				NewRespSol(i47, sol), // bad line, same sol
				NewRespSol(n3pm8, sol), // bad box, same sol
			)
		case base.Unsol:
			unsol := comm.(base.Unsol)
			bads = append(bads, 
				NewRespUnsol(ne3, unsol), // bad net, same unsol
				NewRespUnsol(i47, unsol), // bad line, same unsol
				NewRespUnsol(n3pm8, unsol), // bad box, same unsol
			)
		}
		for _, bad := range bads {
			if resp.Match(bad) {
				t.Fatalf("Match base:%s bad:%s", resp, bad)
			}
		}
	}

	unsols := NewRespUnsols()
	resps := map[string]Resp {
		// n2
		"n2":      NewRespNet(n2),
		"n2pm":    NewRespLine(n2, from.P1),
		"n2pm7":   NewRespSrc(n2pm7),
		"n2pm7_7": NewRespUnsol(n2pm7, p1.Unsol07_Alarmed),
		// n3
		"n3":      NewRespNet(n3),
		"n3pm":    NewRespLine(n3, from.P1),
		"n3pm7":   NewRespSrc(n3pm7),
		"n3pm7_1": NewRespUnsol(n3pm7, base.Unsol01_Restart),
		"n3pm7_5": NewRespUnsol(n3pm7, p1.Unsol05_Idle),
		"n3pm7_6": NewRespUnsol(n3pm7, p1.Unsol06_Working),
		"n3pm7_7": NewRespUnsol(n3pm7, p1.Unsol07_Alarmed),
	}
	rxs := make(map[string][]string, 0)
	for k, _ := range resps {
		rxs[k] = make([]string, 0)
	}
	for k, resp := range resps {
		k2 := k
		unsols.Listener(resp, func(net base.Net, value base.Data) {
			rxs[k2] = append(rxs[k2], fmt.Sprintf("%1x%02x", net, value))
		})
	}
	// n2pm7
	unsols.Fire(NewBox(n2pm7), base.Unsol01_Restart, nil)
	// n3pm7
	unsols.Fire(NewBox(n3pm7), base.Unsol01_Restart, nil)
	unsols.Fire(NewBox(n3pm7), p1.Unsol05_Idle, nil)
	unsols.Fire(NewBox(n3pm7), p1.Unsol06_Working, nil)
	// n3pm8
	unsols.Fire(NewBox(n3pm8), base.Unsol01_Restart, nil)
	unsols.Fire(NewBox(n3pm8), p1.Unsol07_Alarmed, []byte{1,2})

	rxsExp := map[string][]string {
	 	"n2":     []string{"210000701"},
	 	"n2pm":   []string{"210000701"},
	 	"n2pm7":  []string{"210000701"},
	 	"n2pm7_7":[]string{},
	 	"n3":     []string{"310000701","310000705","310000706","310000801","3100008070102"},
	 	"n3pm":   []string{"310000701","310000705","310000706","310000801","3100008070102"},
	 	"n3pm7":  []string{"310000701","310000705","310000706"},
	 	"n3pm7_1":[]string{"310000701"},
	 	"n3pm7_5":[]string{"310000705"},
	 	"n3pm7_6":[]string{"310000706"},
	 	"n3pm7_7":[]string{},
	}
	for k, exp := range rxsExp {
		got := rxs[k]
		tt.Equals(len(got), len(exp))
		for i, e := range exp {
			tt.Equals(e, got[i])
		}
	}
	//t.Log(rxs)
}

func TestSim1(t *testing.T) {
	tt := base.Test{t}
	sim := NewSim(all.SolsFunc)

	oneBox, _ := NewBoxes(2, from.P1, []byte{ 7 })
	pm7, _ := oneBox[7]

	version := []byte{ 44 }
	serial := []byte{ 1,2,3 }

	// Keep box off
	// readonly version write (box off)
	_, err := sim.SolsWrite(pm7, base.Sol00_Version, version)
	tt.Ok(err)
	// nothing stored yet (since box off)
	err = testLen(sim.sols, 0)
	tt.Ok(err)

	t.Run("BoxOn", func(t *testing.T) {
		// Turn on box
		tt.Equals([]string{ "2107" }, sim.Power(pm7, true))
		// readonly version write again (box on)
		_, err := sim.SolsWrite(pm7, base.Sol00_Version, version)
		tt.Ok(err)
		// one stored (0+1)
		err = testLen(sim.sols, 1)
		tt.Ok(err)
		resp := []byte{ 0x10,0,7,0 }
		resp1 := append(resp, version...)
		for key, exp := range map[string][]byte {
				"210000700": resp1,
			} {
			err := testStored(sim.sols, key, exp)
			tt.Ok(err)
		}
		// readonly version gets (box on)
		gets, err := sim.SolsGet(pm7, base.Get00_Version, nil)
		tt.Ok(err)
		for i, exp := range [][]byte {
			resp1,
		} {
			err := testGet(gets[i], exp)
			tt.Ok(err)
		}
		// readonly serial write
		_, err = sim.SolsWrite(pm7, base.Sol31_Serial, serial)
		tt.Ok(err)
		// readonly serial store (1+1)
		err = testLen(sim.sols, 2)
		tt.Ok(err)
		start := []byte{ 0x10,0,7,31 }
		resp = append(start, serial...)
		for key, exp := range map[string][]byte {
				"21000071f": resp,
			} {
			err := testStored(sim.sols, key, exp)
			tt.Ok(err)
		}
		// readonly serial gets response
		gets, err = sim.SolsGet(pm7, base.Get01_Serial, nil)
		tt.Ok(err)
		for i, exp := range [][]byte {
			resp,
		} {
			err := testGet(gets[i], exp)
			tt.Ok(err)
		}
		// readwrite cfg write four times
		for _, extra := range extras {
			ok, err := sim.SolsSet(pm7, p1.Set04_Config, extra)
			tt.Ok(err)
			tt.Assert(ok, "not turned on?")
		}
		// readwrite cfg store (1+1+4)
		if err := testLen(sim.sols, 6); err != nil {
			t.Fatal(err)
		} else {
			base := []byte{ 0x10,0,7,3 }
			for key, sol := range map[string][]byte {
					"21000070301": extras[0],
					"21000070302": extras[1],
					"21000070303": extras[2],
					"21000070304": extras[3],
				} {
				exp := append(base, sol...)
				if err := testStored(sim.sols, key, exp); err != nil {
					t.Fatal(err)
				}
			}
			// readwrite cfg gets response
			if gets, err := sim.SolsGet(pm7, p1.Get03_Config, nil); err != nil {
				t.Fatal(err)
			} else {
				for i, extra := range extras {
					exp := append(base, extra...)
					if err := testGet(gets[i], exp); err != nil {
						t.Fatal(err)
					}
				}
			}
		}
		// send unsol without unsolFunc
		resps, err := sim.UnsolResps(pm7, base.Unsol01_Restart, nil)
		tt.Assert(err != nil, "Nil err")
		tt.Equals("Invalid unsolFunc", err.Error())
		tt.Assert(resps == nil, "%v", resps)

		// send unsols with unsolsFunc
		var expMapJson string
		sim.SetUnsolFunc(func(can *from.Can) (*lines.Resp, error) {
			resp, err := all.UnsolsFunc(can)
			tt.Equals(int(2), int(can.Net))
			tt.Equals(expMapJson, unsols.MapJson(resp.Map))
			return resp, err
		})
		type U struct { unsol base.Unsol; extra []byte; mapJson string; can string }
		for _, u := range []U {
			U{ base.Unsol01_Restart,    nil,         "{1:1}",      "2100701"      },
			U{ p1.Unsol05_Idle,         nil,         "{1:2}",      "2100705"      },
			U{ p1.Unsol06_Working,      nil,         "{1:3}",      "2100706"      },
			U{ p1.Unsol07_Alarmed,      []byte{1,2}, "{1:66049}",  "2100707#0102" },
			U{ p1.Unsol16_NoPhaseFront, nil,         "{1:131072}", "2100710"      }, 
			U{ p1.Unsol17_NoPhaseRear,  nil,         "{1:262144}", "2100711"      },
			U{ p1.Unsol20_Stimulation,  nil,         "{1:4}",      "2100714"      },
		} {
			expMapJson = u.mapJson // set here to be used in comparison in UnsolFunc above
			resps, err := sim.UnsolResps(pm7, u.unsol, u.extra)
			tt.Ok(err)
			tt.Assert(resps != nil, "Resps nil")
			resp := resps.Resps
			tt.Equals(1, len(resp))
			r := resp[0]
			tt.Equals(200, r.StatusCode)
			tt.Equals(1, len(r.Cans))
			tt.Equals(u.can, r.Cans[0].String())
		}
	})

	t.Run("BoxOff", func(t *testing.T) {
		// Turn off box
		tt.Equals(0, len(sim.Power(pm7, false)))

		// version
		write, _ := sim.SolsWrite(pm7, base.Sol00_Version, version)
		tt.Assert(!write, "wrong write: box off")
		tt.Ok(testLen(sim.sols, 6))
		_, err := sim.SolsGet(pm7, base.Get00_Version, nil)
		tt.Assert(err != nil, "err should be box off")

		// serial
		on, _ := sim.SolsWrite(pm7, base.Sol31_Serial, serial)
		tt.Assert(!on, "wrong ok: box off")
		tt.Ok(testLen(sim.sols, 6))
		 _, err = sim.SolsGet(pm7, base.Get01_Serial, nil)
		tt.Assert(err != nil, "err should be box off")

		// cfg
		for _, extra := range extras {
			set, err := sim.SolsSet(pm7, p1.Set04_Config, extra)
			tt.Assert(err != nil, "err should be box off")
			tt.Assert(!set, "wrong set: box off")
		}
		tt.Ok(testLen(sim.sols, 6))
		_, err = sim.SolsGet(pm7, p1.Get03_Config, nil)
		tt.Assert(err != nil, "err should be box off")
		// send unsol
		// with unsolFunc already, resps==nil for box off
		// TODO correct below!
		resps, err := sim.UnsolResps(pm7, base.Unsol01_Restart, nil)
		tt.Ok(err)
		tt.Assert(resps == nil, "resps should be nil (box is off)")
	})
}

func TestSim2(t *testing.T) {
	sim := NewSim(all.SolsFunc)

	boxes, _ := NewBoxes(3, from.P1, testIds)

	// Turn on all boxes
	var ons []string
	for _, id := range testIds {
		if box, ok := boxes[id]; !ok {
			t.Fatalf("Not found box for id:%d", id)
		} else {
			ons = sim.Power(box, true)
		}
	}
	// Check all keys
	for i, exp := range testKeys {
		if got := ons[i]; got != exp {
			t.Fatalf("Expected:%s, got:%s", exp, got)
		}
	}
	// Set solicited serial to all
	for _, id := range testIds {
		box, _ := boxes[id]
		serial := []byte { 
			byte(  0 + id), 
			byte(100 + id), 
			byte(200 + id),
		}
		if _, err := sim.SolsWrite(box, base.Sol31_Serial, serial); err != nil {
			t.Fatal(err)
		}
	}
	// Check serial sols
	// key format NLL00BBCC N=net LL=line, BB=box, CC=command
	for k, exp := range map[string][]byte { 
		"31000061f": []byte{ 16, 0,  6, 31,  6, 106, 206 },
		"31000071f": []byte{ 16, 0,  7, 31,  7, 107, 207 },
		"31000081f": []byte{ 16, 0,  8, 31,  8, 108, 208 },
		"31000091f": []byte{ 16, 0,  9, 31,  9, 109, 209 },
		"310000a1f": []byte{ 16, 0, 10, 31, 10, 110, 210 },
		"310000b1f": []byte{ 16, 0, 11, 31, 11, 111, 211 },
		"310000d1f": []byte{ 16, 0, 13, 31, 13, 113, 213 },
		"31000111f": []byte{ 16, 0, 17, 31, 17, 117, 217 },
	} {
		if got := sim.sols.data[Resp(k)]; bytes.Compare(exp, got) != 0 {
			t.Fatalf("Expected:%v, got:%v", exp, got)
		}
	}
}

// testLen returns descriptive error for unexpected array length
func testLen(sols *RespSols, exp int) error {
	if got := len(sols.data); exp != got {
		return fmt.Errorf("expected:%d, got:%d", exp, got)
	}
	return nil
}

// testStored returns descriptive error for unexpected different value
func testStored(sols *RespSols, key string, exp []byte) error {
	if got := sols.data[Resp(key)]; bytes.Compare(exp, got) != 0 {
		return fmt.Errorf("expected:%02x, got:%02x. key:%s", exp, got, key)
	}
	return nil
}

// testGet returns descriprive error for any unexpected array difference
func testGet(got, exp []byte) error {
	if bytes.Compare(exp, got) != 0 {
		return fmt.Errorf("expected:%02x, got:%02x", exp, got)
	}
	return nil
}
