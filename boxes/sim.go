package boxes

import (
	"fmt"
	"sync"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/api"
)

// Sim simulate a field with equipment
type Sim struct {
	powered   Powered
	sols      *RespSols
	unsolFunc lines.RespFunc
	mu        sync.Mutex
}

// NewSim builds a Sim with a given solsFunc which receives a request and
// returns a sols
func NewSim(solsFunc from.SolsFunc) *Sim {
	return &Sim{
		powered: NewPowered(),
		sols:    NewRespSols(solsFunc),
	}
}

// Sol returns the solicited item data stored with given key as string.
func (s *Sim) Sol(key string) base.Data {
	return s.sols.data[Resp(key)]
}

// Sols returns the number of solicited items stored.
func (s *Sim) Sols() int {
	s.mu.Lock()
	defer s.mu.Unlock()
	return len(s.sols.data)	
}

// Power turns off/on the box in boxes map.
// Only boxes powered on can be written and read by messages.
// Returns the keys of boxes already are on.
// Power is thread safe.
func (s *Sim) Power(box *Box, on bool) []string {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.powered.Set(box, on)
}

// SolsWrite will write in RespSols map the given value (readonly values) iff
// given box is turned on and box/sol/extra combinaton has no errors.
// First returned param is false/true for box turned off/on.
// Second returned param is for possible box/sol/extra errors.
// SolsWrite is thread safe.
func (s *Sim) SolsWrite(box *Box, sol base.Sol, extra []byte) (bool, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.powered.IsOff(box) {
		return false, nil
	}
	if err := s.sols.Write(box, sol, extra); err != nil {
		return true, err
	}
	return true, nil
}

// SolsGet reads from sols map the response associated with given get command.
// First, the box is checked for on state and then associated item is read.
// The value returned was written by SolsWrite or SolsSet methods.
func (s *Sim) SolsGet(box *Box, get base.Get, extra []byte) ([]base.Data, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.powered.IsOff(box) {
		return nil, fmt.Errorf("Off")
	}
	return s.sols.Get(box, get, extra)
}

// SolsSet writes in sols map the response associated with given set command.
// First, the box is checked for on state and associtated item is stored.
// The item's value can be responded with SolsGet method.
func (s *Sim) SolsSet(box *Box, set base.Set, extra []byte) (bool, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.powered.IsOff(box) {
		return false, fmt.Errorf("Off")
	}
	return s.sols.Set(box, set, extra)
}

func (s *Sim) SetUnsolFunc(unsolFunc lines.RespFunc) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.unsolFunc = unsolFunc
}

func (s *Sim) UnsolResps(box *Box, unsol base.Unsol, extra []byte) (*api.Resps, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.unsolFunc == nil { 
		// report invalid unsolFunc before box off
		return nil, fmt.Errorf("Invalid unsolFunc")
	}
	if s.powered.IsOff(box) {
		return nil, nil
	}
	src := box.Src

	if can, err := from.NewCanUnsol(src, unsol, extra); err != nil {
		// errors like line/command mismatch
		return nil, err 
	} else if resp, err := s.unsolFunc(can); err != nil {
		// dispatching to unsol database failed, line/command/extra/key mismatchs
		return nil, err
	} else if can, err := api.NewCanUnsol(src, unsol, extra); err != nil {
		return nil, err
	} else {
		_ = resp
		return &api.Resps{
			Resps: []*api.Resp{
				&api.Resp{
					StatusCode: 200,
					Cans: []*api.Can{
						can, // TODO return unsol.Resp instead (can+unsolMap)?
					},
				},
			},
		}, nil
	}
}
