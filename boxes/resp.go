package boxes

import (
	"bytes"
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/to"
)

// Resp is a key used to identify responses
// Contains always net and optionally several bytes
// like src-line, src-box and src-command. 
// Reduced resps can be compared with Match and accepted for filtering purposes.
// TODO: src-data for "profile"
type Resp string

// NewRespSol builds a Resp containing src's net, line, box and given sol command.
func NewRespSol(src *from.Src, sol base.Sol) Resp {
	return NewResp(src.Net, src.Line.RespSolBytes(src.Box, sol, nil))
}

// NewRespUnsol builds a Resp containing src's net, line, box and given unsol command.
func NewRespUnsol(src *from.Src, unsol base.Unsol) Resp {
	return NewResp(src.Net, src.Line.RespUnsolBytes(src.Box, unsol, nil))
}

// NewRespSrc builds a Resp containing src's net, line, box.
func NewRespSrc(src *from.Src) Resp {
	return NewResp(src.Net, []byte{
		byte(src.Line),
		0,
		src.Box,
	})
}

// NewRespLine builds a Resp containing given net and src-line.
func NewRespLine(net base.Net, line from.From) Resp {
	return NewResp(net, []byte{
		byte(line),
	})
}

// NewRespNet builds a Resp containing only given net.
func NewRespNet(net base.Net) Resp {
	return NewResp(net, nil)
}

// NewResp returns a string with at least one char, the net followed by
// double char for every byte in message array. All chars are hexadecimal numbers.
func NewResp(net base.Net, data []byte) Resp {
	if data != nil {
		return Resp(fmt.Sprintf("%1d%02x", byte(net), data))
	} else {
		return Resp(fmt.Sprintf("%1d", byte(net)))
	}
}

// Match compares this response with another
func (resp Resp) Match(another Resp) bool {
	if r, o := len(resp), len(another); r > o {
		return resp[:o] == another
	} else if r == o {
		return resp == another
	} else {
		return resp == another[:r]
	}
}

// RespSols is a map with keys as Resp and values as base.Data
// The map is used to store read-only configurations, so get-type commands
// can read the map's item valus and set-type commands can write them.
// RespSols methods ARE NOT thread safe.
type RespSols struct {
	solsFunc from.SolsFunc
	data     map[Resp]base.Data
}

// NewRespSols builds a RespSols empty.
func NewRespSols(solsFunc from.SolsFunc) *RespSols {
	return &RespSols {
		solsFunc: solsFunc,
		data:     make(map[Resp]base.Data, 0),
	}
}

// write set or update readonly items.
// Item's key and value are calculated from box/sol/extra given params.
// Writen items values can be read with Get
func (r *RespSols) Write(box *Box, sol base.Sol, extra []byte) error {
	if key, value, err := box.SolKeyValue(sol, extra); err != nil {
		return err
	} else {
		r.data[key] = value
		return nil
	}
}

func (r *RespSols) Get(box *Box, get base.Get, extra []byte) ([]base.Data, error) {
	if dst, err := box.GetDst(); err != nil {
		// Weird error when box src definition line does not have equivalent for dst
		return nil, err
	} else if req, err := to.NewCanGet(dst, get, extra); err != nil {
		// Error about get/extra mismatches
		return nil, err
	} else if sols, err := r.solsFunc(req); err != nil {
		// Error about not programmed lines for relate gets/sols commands extras
		return nil, err
	} else if data, err := sols.Data(); err != nil {
		// Error about any sols data mismatch
		return nil, err
	} else {
		return r.get(box.Net(), data), nil
	}
}

func (r *RespSols) Set(box *Box, set base.Set, extra []byte) (bool, error) {
	if dst, err := box.GetDst(); err != nil {
		// Weird error when box src definition line does not have equivalent for dst
		return false, err
	} else if req, err := to.NewCanSet(dst, set, extra); err != nil {
		// Error about get/extra mismatches
		return false, err
	} else if sols, err := r.solsFunc(req); err != nil {
		// Error about not programmed lines for relate gets/sols commands extras
		return false, err
	} else if data, err := sols.Data(); err != nil {
		// Error about any sols data mismatch
		return false, err
	} else {
		return r.set(box.Net(), extra, data), nil
	}
}

// get reads the map for one or more matching items with given net and data.
// All found items values are returned as an array of values ordered as given data is.
func (r *RespSols) get(net base.Net, data []base.Data) []base.Data {
	sols := make([]base.Data, 0)
	for _, d := range data {
		key := NewResp(net, []byte(d))
		if value, ok := r.data[key]; ok {
			sols = append(sols, value)
		}
	}
	return sols
}

// set writes the map with one or more items with given net, extra bytes and data
func (r *RespSols) set(net base.Net, extra []byte, data []base.Data) bool {
	for _, d := range data {
		solE := []byte(d[4:])
		reqE := []byte(extra[:len(solE)])
		if bytes.Compare(solE, reqE) == 0 {
			value := make([]byte, 4 + len(extra))
			// set: line,dst,src,comm
			copy(value, d)
			// set: extras
			copy(value[4:], extra)
			r.data[NewResp(net, []byte(d))] = value
			return true
		}
	}
	return false
}

// RespUnsols is a map with keys of Resp and values of a function 
// for firing unsol events to listeners set when Resp key matches.
// RespUnsols methods ARE NOT thread safe.
type RespUnsols map[Resp]func(base.Net, base.Data)

// NewRespUnsols creates an empty RespUnsols
func NewRespUnsols() RespUnsols {
	return make(map[Resp]func(base.Net, base.Data))
}

// Fire will call values functions if given key matched de map's keys.
func (r *RespUnsols) Fire(box *Box, unsol base.Unsol, extra []byte) error {
	key, value, err := box.UnsolKeyValue(unsol, extra)
	if err != nil {
		return err
	}
	for k, f := range *r {
		if key.Match(k) {
			if f != nil {
				f(box.Net(), value)
			} else {
				delete(*r, k)
			}
		}
	}
	return nil
}

// Listener adds or removes the given function f.
// If given function is nil and the map contains the given key, the item is deleted.
// If given function is not nil corresponding map's item is added or updated.
func (r *RespUnsols) Listener(key Resp, f func(base.Net, base.Data)) {
	if f != nil {
		(*r)[key] = f
	} else {
		delete(*r, key)
	}
}
