package boxes

import (
	"errors"
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/to"
)

type Box struct {
	Key string
	Dst *to.Dst
	Src *from.Src
}

// NewBox returns a string of four chars for boxes of type to.Dst and from.Src
// first char is the net, second char is the condensed line (same for to or from)
// and last two chars it the box id. All chars are hexadecimal numbers.
func NewBox(box interface{}) *Box {
	switch box.(type) {
	case *to.Dst:
		dst := box.(*to.Dst)
		return &Box{
			Key: fmt.Sprintf("%1x%1x%02x", dst.Net, dst.Line & 0xff, dst.Box),
			Dst: dst,
		}
	case *from.Src:
		src := box.(*from.Src)
		return &Box{
			Key: fmt.Sprintf("%1x%1x%02x", src.Net, src.Line >> 4, src.Box),
			Src: src,
		}
	}
	return nil
}

func (b *Box) SolKeyValue(sol base.Sol, extra []byte) (key Resp, value base.Data, err error) {
	if src := b.Src; src == nil {
		err = errors.New("No src defined")
	} else {
		key = NewRespSol(src, sol)
		value, err = base.NewData(src.Line.RespSolBytes(src.Box, sol, extra))
	}
	return
}

func (b *Box) UnsolKeyValue(unsol base.Unsol, extra []byte) (key Resp, value base.Data, err error) {
	if src := b.Src; src == nil {
		err = errors.New("No src defined")
	} else {
		key = NewRespUnsol(src, unsol)
		value, err = base.NewData(src.Line.RespUnsolBytes(src.Box, unsol, extra))
	}
	return
}

func (b *Box) GetDst() (*to.Dst, error) {
	if b.Dst != nil {
		return b.Dst, nil
	} else if b.Src != nil {
		if line, err := to.NewTo(byte(b.Src.Line) >> 4); err != nil {
			return nil, err
		} else {
			return to.NewDst(byte(b.Src.Net), line, b.Src.Box)
		}
	} else {
		return nil, errors.New("No dst/src defined")
	}
}

func (b *Box) Net() base.Net {
	if b.Src != nil {
		return b.Src.Net
	} else if b.Dst != nil {
		return b.Dst.Net
	} else {
		return base.Net(0)
	}
}

type Boxes map[byte]*Box

// NewBoxes build a map of Box with key 
func NewBoxes(net byte, line from.From, ids []byte) (Boxes, error) {
	boxes := make(map[byte]*Box, 0)
	for _, box := range ids {
		if _, ok := boxes[box]; !ok {
			if src, err := from.NewSrc(net, line, box); err != nil {
				return nil, err
			} else {
				boxes[box] = NewBox(src)
			}
		}
	}
	return Boxes(boxes), nil
}

func (boxes Boxes) Box(box byte) (*Box, bool) {
	b, ok := boxes[box]
	return b, ok
}

