package boxes

import (
	"sort"
)

type Powered map[string]bool

func NewPowered() Powered {
	return make(map[string]bool)
}

func (p *Powered) Set(box *Box, on bool) []string {
	if box != nil {
		(*p)[box.Key] = on
	}
	keys := make([]string, 0)
	for k, v := range *p {
		if v {
			keys = append(keys, string(k))
		}
	}
	sort.Strings(keys)
	return keys
}

// IsOff return false (Box is on)
// IFF when given box is valid (not nil)
// AND when item with box's key exists in this map
// AND when item's value is true.
// Returns true otherwise
func (p *Powered) IsOff(box *Box) bool {
	if box == nil {
		return true
	} else if state, ok := (*p)[box.Key]; !ok {
		return true
	} else {
		return !state
	}
}

