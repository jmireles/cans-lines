package rss

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/line/rs"

	"gitlab.com/jmireles/cans-lines"
)

var (
	ReadWriteMap lines.ReadWriteMap
	ReadOnlyMap  lines.ReadOnlyMap
)

func init() {
	ReadWriteMap = lines.ReadWriteMap(map[base.Get]*lines.ReadWrite{
		rs.Get03_Config: lines.NewReadWrite(rs.Set04_Config, rs.Sol03_Config), // cfg 1
	})
	ReadOnlyMap = lines.ReadOnlyMap(map[base.Get]*lines.ReadOnly{
		base.Get00_Version: lines.NewReadOnly(base.Sol00_Version),
		base.Get01_Serial:  lines.NewReadOnly(base.Sol31_Serial),
		base.Get02_Status:  lines.NewReadOnly(base.Sol02_Status),
	})
}

// Masks returns the solicited response and mask for given get/set command
func Masks(comm byte) (sol base.Sol, masks []byte, ok bool) {
	switch comm {
	default:
		return
	}
}

type RSs struct {
	net byte
	//gets *Gets
}

