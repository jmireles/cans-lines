package rss

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/rs"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/unsols"
)

// UnsolsFunc implements lines.RespFunc
func UnsolsFunc(req *from.Can) (resp *lines.Resp, err error) {
	resp, err = unsols.Func(req)
	if err != nil {
		return
	}
	switch *req.Unsol {

	case base.Unsol01_Restart:
		// map with single status key with value exactly 1
		resp.Map = unsols.MapRestart

	case rs.Unsol06_Stall:
		extra := resp.Can.Extra
		if len(extra) < 2 {
			err = unsols.ErrorInvalidType
			return
		}
		gate := extra[0]
		reader := extra[1]
		resp.Map = lines.Map{
			unsols.KeyGate:   lines.Value(gate),
			unsols.KeyReader: lines.Value(reader),
		}
	default:
		err = unsols.ErrorInvalidUnsol
	}
	return
}

// Parlor
//   REQ
//     get=3 (no data)
//     set=4 (data see 3 bytes below)
//   RESP
//     get=3, data=
//       byte 0: (char) order, bit 0 = ascending/descending.
//       byte 1: (char) stalls
//       byte 2: (char) distance (positive)

// Tag
//   REQ
//     get=5, data=
//       byte 0: stall (display as "profile")
//     set=7
//        bytes 0-7:  [1] [stall]   [ASCII-1] [ASCII-2] [ASCII-3] [ASCII-4] [ASCII-5] [ASCII-6]
//        bytes 8-15: [2] [ASCII-7] [ASCII-8] [0]       [0]       [0]       [0]       [0]
//   RESP
//     get=4, data=
//       bytes(0-7): ASCII display as XX XX XX XX (read only no set)

// Program (warning command protect with confirmation dialog)
//   REQ
//     get=6, data=
//       byte 0: start-stall (display as "profile")
//       byte 1: end-stall (display as "final")
//     set=NOT USED
//  RESP
//     get=5, data=
//       byte 0: (value 0=program problems, value=1 ok)

// Gate/Reader
//   UNSOL
//     unsol=6, data=
//       byte 0: gate stall (display in parlor view as a yellow circle)
//       byte 1: reader stall
//       (both are saved in history)

/*func UnsolStallParse(data []byte) (stall byte, reader byte, err error) {
	if data == nil {
		err = errors.New("empty data")
		return
	}
	if len(data) < 2 {
		err = errors.New("data size lower than 2")
		return
	}
	stall = data[0]
	reader = data[1]
	return
}*/
