package rss

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/rs"

	"gitlab.com/jmireles/cans-lines/unsols"
)

func TestUnsol(t *testing.T) {
	tt := base.Test{t}
	src, _ := from.NewSrc(4, from.RS, 10)
	// well formed
	restart, _  := from.NewCanUnsol(src, base.Unsol01_Restart, nil)
	
	// change1 for gates enter,exit and wash and prewash on,off:
	change := func(gate, reader byte) *from.Can {
		can, _  := from.NewCanUnsol(src, rs.Unsol06_Stall, []byte{
			gate, 
			reader,
		})
		return can
	}

	// valid events:
	for can, json := range map[*from.Can]string {
		restart:          "{4:{6:{10:{1:1}}}}",
		change(0, 0):     "{4:{6:{10:{4:0,5:0}}}}",
		change(1, 0):     "{4:{6:{10:{4:1,5:0}}}}",
		change(0, 1):     "{4:{6:{10:{4:0,5:1}}}}",
		change(255, 255): "{4:{6:{10:{4:255,5:255}}}}",
	} {
		resp, err := UnsolsFunc(can)
		tt.Ok(err)
		s, _ := unsols.RTJson(nil, resp) // filter is nil to pass everything
		tt.Equals(json, s)
	}

	// invalid events
	invalidUnsol2, _  := from.NewCanUnsol(src, base.Unsol(2), nil)
	invalidUnsol5, _  := from.NewCanUnsol(src, base.Unsol(5), nil) // is nil because rejected by lines-base
	invalidChange1, _ := from.NewCanUnsol(src, base.Unsol(6), nil)
	invalidChange2, _ := from.NewCanUnsol(src, base.Unsol(6), []byte{ 1 })
	for can, e := range map[*from.Can]error {
		invalidUnsol2:  unsols.ErrorInvalidUnsol,
		invalidUnsol5:  unsols.ErrorInvalidUnsol,

		invalidChange1: unsols.ErrorInvalidType,
		invalidChange2: unsols.ErrorInvalidType,
	} {
		resp, err := UnsolsFunc(can)
		tt.Assert(err!=nil, "nil error for %v", resp)
		tt.Equals(err, e)
	}
}

