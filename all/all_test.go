package all

import (
	"bytes"
	"fmt"
	"strings"
	"sync"
	"testing"
	"time"

	"net/http"
	"net/http/httptest"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/line/i4"
	"gitlab.com/jmireles/cans-base/line/p1"
	"gitlab.com/jmireles/cans-base/line/t4"
	"gitlab.com/jmireles/cans-base/to"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/api"
	"gitlab.com/jmireles/cans-lines/boxes"
	"gitlab.com/jmireles/cans-lines/i4s"
	"gitlab.com/jmireles/cans-lines/p1s"
	"gitlab.com/jmireles/cans-lines/rss"
	"gitlab.com/jmireles/cans-lines/t4s"
	"gitlab.com/jmireles/cans-lines/unsols"
)

func TestComms(t *testing.T) {
	// Common P1, T4, I4, RSS
	for line, m := range map[string]lines.ReadOnlyMap {
		"i4s": i4s.ReadOnlyMap,
		"p1s": p1s.ReadOnlyMap,
		"rss": rss.ReadOnlyMap,
		"t4s": t4s.ReadOnlyMap,
	} {
		for get, exp := range lines.ReadOnlyCommon {
			if ro := m.Exist(get); ro == nil {
				t.Fatalf("line:%s get:%d not found", line, get)
			} else if len(ro.Sols) < 1 || ro.Sols[0] != exp.Sols[0] {
				t.Fatalf("line:%s sol:%d not found", line, get)
			}
		}
	}

	// p1s readWrite
	if exp, m := 8, p1s.ReadWriteMap; exp != len(m) {
		t.Fatalf("Expected:%d, got:%d", exp, len(m))
	} else if rw := m.Exist(base.Get00_Version); rw != nil {
		t.Fatal("Unexpected not nil")
	} else if rw := m.Exist(p1.Get03_Config); rw == nil {
		t.Fatal("Unexpected nil")
	} else if exp, got := rw.Set, p1.Set04_Config; exp != got {
		t.Fatalf("Expected:%d, got:%d", exp, got)
	} else if exp, got := rw.Sol, p1.Sol03_Config; exp != got {
		t.Fatalf("Expected:%d, got:%d", exp, got)
	}
	// p1s readOnly
	if exp, m := 5, p1s.ReadOnlyMap; exp != len(m) {
		t.Fatalf("Expected:%d, got:%d", exp, len(m))
	} else if ro := m.Exist(p1.Get03_Config); ro != nil {
		t.Fatal("Unexpected not nil")
	}

	// t4s readWrite
	if exp, m := 6, t4s.ReadWriteMap; exp != len(m) {
		t.Fatalf("Expected:%d, got:%d", exp, len(m))
	} else if rw := m.Exist(base.Get00_Version); rw != nil {
		t.Fatal("Unexpected not nil")
	} else if rw := m.Exist(t4.Get03_Stalls); rw == nil {
		t.Fatal("Unexpected nil")
	} else if exp, got := rw.Set, t4.Set09_Stalls; exp != got {
		t.Fatalf("Expected:%d, got:%d", exp, got)
	} else if exp, got := rw.Sol, t4.Sol03_Stalls; exp != got {
		t.Fatalf("Expected:%d, got:%d", exp, got)
	}
	// t4s readOnly
	if exp, m := 4, t4s.ReadOnlyMap; exp != len(m) {
		t.Fatalf("Expected:%d, got:%d", exp, len(m))
	} else if ro := m.Exist(t4.Get03_Stalls); ro != nil {
		t.Fatal("Unexpected not nil")
	}
}

func TestSols(t *testing.T) {
	dst, _ := to.NewDst(2, to.P1, 7)
	// version get/sol
	if req, err := to.NewCanGet(dst, base.Get00_Version, nil); err != nil {
		t.Fatal(err)
	} else if err := testSols(req, 1, [][]byte{
		[]byte{ 16,0,7,0 },
	}); err != nil {
		t.Fatal(err)
	}
	// serial get/sol
	if req, err := to.NewCanGet(dst, base.Get01_Serial, nil); err != nil {
		t.Fatal(err)
	} else if err := testSols(req, 1, [][]byte{
		[]byte{ 16,0,7,31 },
	}); err != nil {
		t.Fatal(err)
	}
	// pm get-config
	if req, err := to.NewCanGet(dst, p1.Get03_Config, nil); err != nil {
		t.Fatal(err)
	} else if err := testSols(req, 4, [][]byte{
		[]byte{ 16,0,7,3,1 },
		[]byte{ 16,0,7,3,2 },
		[]byte{ 16,0,7,3,3 },
		[]byte{ 16,0,7,3,4 },
	}); err != nil {
		t.Fatal(err)
	}
	// pm set-config
	if req, err := to.NewCanSet(dst, p1.Set04_Config, nil); err != nil {
		t.Fatal(err)
	} else if err := testSols(req, 4, [][]byte{
		[]byte{ 16,0,7,3,1 },
		[]byte{ 16,0,7,3,2 },
		[]byte{ 16,0,7,3,3 },
		[]byte{ 16,0,7,3,4 },
	}); err != nil {
		t.Fatal(err)
	}
}

func TestUnsols(t *testing.T) {
	tt := base.Test{t}
	// net=2, line=1, box=2, key=1 (UnsolKeyStatus)
	pm, i4, t4 := from.P1, from.I4, from.T4
	src, _ := from.NewSrc(2, pm, 7)
	baseRestart := base.Unsol01_Restart
	alarms, extra := p1.Unsol07_Alarmed, []byte{1,2}
	restart, _ := from.NewCanUnsol(src, baseRestart, nil)
	canAlarms, _ := from.NewCanUnsol(src, alarms, extra)

	rt := unsols.NewRT(UnsolsFunc)
	idle, _  := from.NewCanUnsol(src, p1.Unsol05_Idle, nil)
	work, _  := from.NewCanUnsol(src, p1.Unsol06_Working, nil)
	front, _ := from.NewCanUnsol(src, p1.Unsol16_NoPhaseFront, nil)
	rear, _  := from.NewCanUnsol(src, p1.Unsol17_NoPhaseRear, nil)
	stim, _  := from.NewCanUnsol(src, p1.Unsol20_Stimulation, nil)
	for can, json := range map[*from.Can]string {
		restart:   "{2:{1:{7:{1:1}}}}",
		idle:      "{2:{1:{7:{1:2}}}}",
		work:      "{2:{1:{7:{1:3}}}}",
		stim:      "{2:{1:{7:{1:4}}}}",
		canAlarms: "{2:{1:{7:{1:66049}}}}",
		front:     "{2:{1:{7:{1:131072}}}}",
		rear:      "{2:{1:{7:{1:262144}}}}",
	} {
		rt.Update(can)
		tt.Equals(json, rt.Json(nil))
	}

	// net=2, line=1, box=2, key=2 (UnsolKeyNotify)
	rt.Clear() // clear database
	notify, _ := from.NewCanUnsol(src, p1.Unsol26_Notify, []byte{
		1,255,255,255,255,255,
	})
	for can, json := range map[*from.Can]string {
		notify: "{2:{1:{7:{2:131071,3:16711937}}}}",
	} {
		rt.Update(can)
		tt.Equals(json, rt.Json(nil))
	}

	// several nets, lines, boxes, UnsolKeyStatus
	rt.Clear() // clear database
	pm_18, _ := from.NewSrc(1, pm, 18)
	pm_19, _ := from.NewSrc(1, pm, 19)
	pm_20, _ := from.NewSrc(2, pm, 20)
	pm_21, _ := from.NewSrc(2, pm, 21)
	i4_01, _ := from.NewSrc(3, from.I4, 1)

	pm_18_1, _ := from.NewCanUnsol(pm_18, base.Unsol01_Restart, nil)
	pm_19_1, _ := from.NewCanUnsol(pm_19, base.Unsol01_Restart, nil)
	pm_20_1, _ := from.NewCanUnsol(pm_20, base.Unsol01_Restart, nil)
	pm_21_1, _ := from.NewCanUnsol(pm_21, base.Unsol01_Restart, nil)
	i4_01_1, _ := from.NewCanUnsol(i4_01, base.Unsol01_Restart, nil)
	for _, can := range []*from.Can {
		pm_18_1, // net=1, pm=18 restart
		pm_19_1, // net=1, pm=19 restart
		pm_20_1, // net=2, pm=20 restart
		pm_21_1, // net=2, pm=21 restart
		i4_01_1, // net=3, i4=01 restart
	} {
		rt.Update(can)
	}

	// filters
	for filter, json := range map[unsols.Filter]string {
		// pass everything
		unsols.Filter{}: "{1:{1:{18:{1:1},19:{1:1}}},2:{1:{20:{1:1},21:{1:1}}},3:{8:{1:{1:1}}}}",
		// filter complete nets		
		unsols.Filter{Net:1}: "{1:{1:{18:{1:1},19:{1:1}}}}", // pass net=1
		unsols.Filter{Net:2}: "{2:{1:{20:{1:1},21:{1:1}}}}", // pass net=2
		unsols.Filter{Net:3}: "{3:{8:{1:{1:1}}}}", // pass net=3
		unsols.Filter{Net:4}: "", // Pass net=4
		// filter line=PM
		unsols.Filter{Line:&pm}:       "{1:{1:{18:{1:1},19:{1:1}}},2:{1:{20:{1:1},21:{1:1}}},3:{}}",
		unsols.Filter{Net:1,Line:&pm}: "{1:{1:{18:{1:1},19:{1:1}}}}",
		unsols.Filter{Net:2,Line:&pm}: "{2:{1:{20:{1:1},21:{1:1}}}}",
		// filter line=PM boxs
		unsols.Filter{Net:1,Line:&pm,Box:17}:       "",
		unsols.Filter{Net:1,Line:&pm,Box:18}:       "{1:{1:{18:{1:1}}}}",
		unsols.Filter{Net:1,Line:&pm,Box:19}:       "{1:{1:{19:{1:1}}}}", // all 19 keys
		unsols.Filter{Net:1,Line:&pm,Box:19,Key:0}: "{1:{1:{19:{1:1}}}}", // all keys
		unsols.Filter{Net:1,Line:&pm,Box:19,Key:1}: "{1:{1:{19:{1:1}}}}", // only key status
		unsols.Filter{Net:1,Line:&pm,Box:19,Key:2}: "", // only key input1
		unsols.Filter{Line:&pm,Box:18}:             "{1:{1:{18:{1:1}}},2:{1:{}},3:{}}",
		// filter line=I4
		unsols.Filter{Line:&i4}:             "{1:{},2:{},3:{8:{1:{1:1}}}}",
		unsols.Filter{Net:3,Line:&i4}:       "{3:{8:{1:{1:1}}}}",
		unsols.Filter{Net:3,Line:&i4,Box:1}: "{3:{8:{1:{1:1}}}}",
		unsols.Filter{Net:3,Line:&i4,Box:2}: "",
		// filter line=T4
		unsols.Filter{Line:&t4}:       "",
		unsols.Filter{Net:1,Line:&t4}: "",
	} {
		tt.Equals(json, rt.Json(&filter))
	}

	// unsols:NewRT1 with FormatMat filtered
	for filter, reqs := range map[*unsols.Filter]map[*from.Can]string {
		&unsols.Filter{}: map[*from.Can]string {
			pm_18_1: "{1:{1:{18:{1:1}}}}",
			pm_19_1: "{1:{1:{19:{1:1}}}}",
			pm_20_1: "{2:{1:{20:{1:1}}}}",
			pm_21_1: "{2:{1:{21:{1:1}}}}",
			i4_01_1: "{3:{8:{1:{1:1}}}}",
		},
		&unsols.Filter{Net:1}: map[*from.Can]string {
			pm_18_1: "{1:{1:{18:{1:1}}}}",
			pm_19_1: "{1:{1:{19:{1:1}}}}",
			pm_20_1: "",
			pm_21_1: "",
			i4_01_1: "",
		},
		&unsols.Filter{Line:&pm}: map[*from.Can]string {
			pm_18_1: "{1:{1:{18:{1:1}}}}",
			pm_19_1: "{1:{1:{19:{1:1}}}}",
			pm_20_1: "{2:{1:{20:{1:1}}}}",
			pm_21_1: "{2:{1:{21:{1:1}}}}",
			i4_01_1: "",
		},
		&unsols.Filter{Line:&pm,Box:18}: map[*from.Can]string {
			pm_18_1: "{1:{1:{18:{1:1}}}}",
			pm_19_1: "",
			pm_20_1: "",
			pm_21_1: "",
			i4_01_1: "",
		},
		&unsols.Filter{Line:&pm,Box:18,Key:2}: map[*from.Can]string {
			pm_18_1: "",
			pm_19_1: "",
			pm_20_1: "",
			pm_21_1: "",
			i4_01_1: "",
		},
	} {
		filter.Format = unsols.FormatMap
		rt1 := unsols.NewRT1(UnsolsFunc, filter)
		for can, json := range reqs {
			tt.Equals(json, rt1.Json(can))		
		}
	}
}



func TestClientNoServer(t *testing.T) {
	//tt := lines.Test{t}
	client := newCli()
	dst, _ := to.NewDst(2, to.P1, 7)

	extras := test1234Extras
	reqs := make([]*to.Can, len(extras))
	for pos, extra := range extras {
		reqs[pos], _ = to.NewCanSet(dst, p1.Set04_Config, extra)
	}
	if resps, err := SolsPost(client.client, reqs...); err != nil {
		t.Fatal(err)
	} else if exp, got := 4, len(resps.Resps); exp != got {
		t.Fatalf("expected:%d, got:%d", exp, got)
	} else {
		for _, resp := range resps.Resps {
			if resp.Error == "" {
				// "Post \"http://127.0.0.1:8081/can/2/req/1,7,0,4,1,17,33,49,65,81,97,113\": 
				// dial tcp 127.0.0.1:8081: connect: connection refused" []
				t.Skipf("expected error, got nil. No 8081 service should be running, close any!")
			} else if exp, got := 0, len(resp.Cans); exp != got {
				t.Fatalf("expected:%d, got:%d", exp, got)
			}
			//t.Log(resp)
		}
	}
}

func TestClientServer(t *testing.T) {
	tt := base.Test{t}
	src, _ := from.NewSrc(2, from.P1, 7)
	dst, _ := to.NewDst(2, to.P1, 7)
	box := boxes.NewBox(src)

	srv, err := newSrvDefault()
	if err != nil {
		t.Fatal(err)
	}
	defer srv.Close()

	version := []byte{ 44 }
	serial := []byte{ 1,2,3 }

	// box is off, write yet, don't expect sols stored (0)
	tt.Ok(srv.writeErr(box, base.Sol00_Version, version))
	tt.Ok(srv.solsErr(0, nil))

	// Turn on box
	srv.sim.Power(box, true)
	// write version, expect first sols stored
	tt.Ok(srv.writeErr(box, base.Sol00_Version, version))
	tt.Ok(srv.solsErr(1, map[string][]byte{
		"210000700": append([]byte{ 0x10,0,7,0 }, version...),
	}))
	// write serial, sols stored=2
	tt.Ok(srv.writeErr(box, base.Sol31_Serial, serial))
	tt.Ok(srv.solsErr(2, map[string][]byte {
		"21000071f": append([]byte{ 0x10,0,7,31 }, serial...),
	}))

	client := newCli()

	// Get readonly version written above
	get, err := to.NewCanGet(dst, base.Get00_Version, nil)
	tt.Ok(err)
	tt.Ok(client.RespSolsPostErr([]*to.Can{get}, 1, []string{
    	"2100700#2c00000000000000", // version
	}))
	// Get readonly serial written above
	get, err = to.NewCanGet(dst, base.Get01_Serial, nil)
	tt.Ok(err)
	tt.Ok(client.RespSolsPostErr([]*to.Can{get}, 1, []string{
    	"210071f#0102030000000000", // serial
	}))

	// Set readwrite cfg
	time.Sleep(100 * time.Millisecond)
	extras := test1234Extras
	reqs := make([]*to.Can, len(extras))
	for pos, extra := range extras {
		reqs[pos], _ = to.NewCanSet(dst, p1.Set04_Config, extra)
	}
	tt.Ok(client.RespSolsPostErr(reqs, 4, nil))
	tt.Ok(srv.solsErr(6, map[string][]byte {
		"21000070301": append([]byte{ 0x10,0,7,3 }, extras[0]...),
		"21000070302": append([]byte{ 0x10,0,7,3 }, extras[1]...),
		"21000070303": append([]byte{ 0x10,0,7,3 }, extras[2]...),
		"21000070304": append([]byte{ 0x10,0,7,3 }, extras[3]...),
	}))

	// Get cfg previously set
	time.Sleep(100 * time.Millisecond)
	get, err = to.NewCanGet(dst, p1.Get03_Config, nil)
	tt.Ok(err)
	tt.Ok(client.RespSolsPostErr([]*to.Can{get}, 1, []string{
    	"2100703#0111213141516171", // extras[0]
    	"2100703#0212223242526272", // extras[1]
    	"2100703#0313233343536373", // extras[2]
    	"2100703#0414243444546474", // extras[3]
	}))
}

func TestClientUnsols(t *testing.T) {
	tt := base.Test{t}
	srv, err := newSrvDefault()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("HTTP: %s%s%s", base.Green, srv.api.URL, base.Reset)
	//defer srv.Close()

	srv.SetUnsolSSE("/", "/sse")

	pm := from.P1
	li4 := from.I4
	restart := base.Unsol01_Restart
	change := i4.Unsol05_Change

	all := make([]*boxes.Box, 0)
	for _, nb := range [][]byte { // four 4 pms and 1 i4
		[]byte{ 1, 0x10, 18 }, // net=1, pm=18
		[]byte{ 1, 0x10, 19 }, // net=1, pm=19
		[]byte{ 2, 0x10, 20 }, // net=2, pm=20
		[]byte{ 2, 0x10, 21 }, // net=2, pm=21
		[]byte{ 3, 0x80,  1 }, // net=3, i4= 1
	} {
		line, _ := from.NewFrom(nb[1])
		src, _ := from.NewSrc(nb[0], line, nb[2])
		box := boxes.NewBox(src)
		srv.sim.Power(box, true) // turn it on to receive responses
		all = append(all, box)
	}

	type U struct { unsol base.Unsol; extra []byte }
	send := func() {
		// send unsol responses
		for _, a := range all {
			switch a.Src.Line {
			case from.P1:
				for _, u := range []U {
					U{ restart,            nil },
					U{ p1.Unsol05_Idle,    nil },
					U{ p1.Unsol06_Working, nil },
					U{ p1.Unsol07_Alarmed, []byte{ 1,2 } },
					U{ p1.Unsol26_Notify,  []byte{ 1,0,0,0,0,0 } },
				} {
					_, err := srv.sim.UnsolResps(a, u.unsol, u.extra)
					tt.Ok(err)
					time.Sleep(50 * time.Millisecond)
				}
			case from.I4:
				for _, u := range []U {
					U{ restart,           nil },
					U{ i4.Unsol05_Change, []byte{ 1,2,3 } },
				} {
					_, err := srv.sim.UnsolResps(a, u.unsol, u.extra)
					tt.Ok(err)
					time.Sleep(50 * time.Millisecond)
				}
			}
		}
	}

	type B struct { f unsols.Filter; exp []string; got []string }
	browsers := []B {
		B{ f:unsols.Filter{}, exp:[]string { // all cans
			"1101201", "1101205", "1101206", "1101207.0102", "110121a.010000000000", 
			"1101301", "1101305", "1101306", "1101307.0102", "110131a.010000000000", 
			"2101401", "2101405", "2101406", "2101407.0102", "210141a.010000000000", 
			"2101501", "2101505", "2101506", "2101507.0102", "210151a.010000000000", 
			"3800101", "3800105.010203",
		}},
		B{ f:unsols.Filter{ Net:2 }, exp:[]string { // all net=2 cans
			"2101401", "2101405", "2101406", "2101407.0102", "210141a.010000000000", 
			"2101501", "2101505", "2101506", "2101507.0102", "210151a.010000000000",
		}},
		B{ f:unsols.Filter{ Line:&pm }, exp:[]string { // all line=pm cans
			"1101201", "1101205", "1101206", "1101207.0102", "110121a.010000000000", 
			"1101301", "1101305", "1101306", "1101307.0102", "110131a.010000000000", 
			"2101401", "2101405", "2101406", "2101407.0102", "210141a.010000000000", 
			"2101501", "2101505", "2101506", "2101507.0102", "210151a.010000000000",
		}},
		B{ f:unsols.Filter{ Box:19 }, exp:[]string { // all box=19 cans
			"1101301", "1101305", "1101306", "1101307.0102", "110131a.010000000000",
		}},
		B{ f:unsols.Filter{ Unsol:&restart }, exp:[]string { // all unsol=restart cans
			"1101201",
			"1101301",
			"2101401",
			"2101501",
			"3800101",
		}},
		B{ f:unsols.Filter{ Line:&li4, Unsol:&change }, exp:[]string { // all line=i4 unsol=change cans
			"3800105.010203",
		}},

		B{ f:unsols.Filter{ Format:unsols.FormatMap }, exp:[]string{ // all maps json
			"{1:{1:{18:{1:1}}}}", "{1:{1:{18:{1:2}}}}", "{1:{1:{18:{1:3}}}}", "{1:{1:{18:{1:66049}}}}", "{1:{1:{18:{2:65536,3:257}}}}", 
			"{1:{1:{19:{1:1}}}}", "{1:{1:{19:{1:2}}}}", "{1:{1:{19:{1:3}}}}", "{1:{1:{19:{1:66049}}}}", "{1:{1:{19:{2:65536,3:257}}}}",
			"{2:{1:{20:{1:1}}}}", "{2:{1:{20:{1:2}}}}", "{2:{1:{20:{1:3}}}}", "{2:{1:{20:{1:66049}}}}", "{2:{1:{20:{2:65536,3:257}}}}",
			"{2:{1:{21:{1:1}}}}", "{2:{1:{21:{1:2}}}}", "{2:{1:{21:{1:3}}}}", "{2:{1:{21:{1:66049}}}}", "{2:{1:{21:{2:65536,3:257}}}}",
			"{3:{8:{1:{1:1}}}}", "{3:{8:{1:{16:1}}}}",
		}},
		B{ f:unsols.Filter{ Format:unsols.FormatMap, Net:2 }, exp:[]string{ // all maps net=2 json
			"{2:{1:{20:{1:1}}}}", "{2:{1:{20:{1:2}}}}", "{2:{1:{20:{1:3}}}}", "{2:{1:{20:{1:66049}}}}", "{2:{1:{20:{2:65536,3:257}}}}", 
			"{2:{1:{21:{1:1}}}}", "{2:{1:{21:{1:2}}}}", "{2:{1:{21:{1:3}}}}", "{2:{1:{21:{1:66049}}}}", "{2:{1:{21:{2:65536,3:257}}}}",
		}},
		B{ f:unsols.Filter{ Format:unsols.FormatMap, Line:&pm }, exp:[]string{ // all maps line=pm json
			"{1:{1:{18:{1:1}}}}", "{1:{1:{18:{1:2}}}}", "{1:{1:{18:{1:3}}}}", "{1:{1:{18:{1:66049}}}}", "{1:{1:{18:{2:65536,3:257}}}}",
			"{1:{1:{19:{1:1}}}}", "{1:{1:{19:{1:2}}}}", "{1:{1:{19:{1:3}}}}", "{1:{1:{19:{1:66049}}}}", "{1:{1:{19:{2:65536,3:257}}}}",
			"{2:{1:{20:{1:1}}}}", "{2:{1:{20:{1:2}}}}", "{2:{1:{20:{1:3}}}}", "{2:{1:{20:{1:66049}}}}", "{2:{1:{20:{2:65536,3:257}}}}",
			"{2:{1:{21:{1:1}}}}", "{2:{1:{21:{1:2}}}}", "{2:{1:{21:{1:3}}}}", "{2:{1:{21:{1:66049}}}}", "{2:{1:{21:{2:65536,3:257}}}}",
		}},
		B{ f:unsols.Filter{ Format:unsols.FormatMap, Box:19 }, exp:[]string{ // all maps box=19 json
			"{1:{1:{19:{1:1}}}}", "{1:{1:{19:{1:2}}}}", "{1:{1:{19:{1:3}}}}", "{1:{1:{19:{1:66049}}}}", "{1:{1:{19:{2:65536,3:257}}}}", 
		}},
		B{ f:unsols.Filter{ Format:unsols.FormatMap, Key:1 }, exp:[]string{ // all maps key=1 json
			"{1:{1:{18:{1:1}}}}", "{1:{1:{18:{1:2}}}}", "{1:{1:{18:{1:3}}}}", "{1:{1:{18:{1:66049}}}}", 
			"{1:{1:{19:{1:1}}}}", "{1:{1:{19:{1:2}}}}", "{1:{1:{19:{1:3}}}}", "{1:{1:{19:{1:66049}}}}", 
			"{2:{1:{20:{1:1}}}}", "{2:{1:{20:{1:2}}}}", "{2:{1:{20:{1:3}}}}", "{2:{1:{20:{1:66049}}}}", 
			"{2:{1:{21:{1:1}}}}", "{2:{1:{21:{1:2}}}}", "{2:{1:{21:{1:3}}}}", "{2:{1:{21:{1:66049}}}}", 
			"{3:{8:{1:{1:1}}}}",
		}},
		B{ f:unsols.Filter{ Format:unsols.FormatMap, Key:2 }, exp:[]string{ // all maps key=2 json
			"{1:{1:{18:{2:65536}}}}", "{1:{1:{19:{2:65536}}}}", "{2:{1:{20:{2:65536}}}}", "{2:{1:{21:{2:65536}}}}",
		}},
	}
	browsersReady := sync.WaitGroup{}
	browsersReady.Add(len(browsers))
	for b, _ := range browsers {
		go func(b *B) { // start a browser page
			b.got = make([]string, 0)
			//log := base.LogF("ClientSSE", base.Yellow)
			c := unsols.NewClient(srv.api.Client(), srv.api.URL, nil)
			c.GetSSE("/sse", &b.f, func(resp *http.Response, text string) {
				if resp != nil {
					tt.Equals(200, resp.StatusCode)
					tt.Equals(resp.Header["Cache-Control"], []string{"no-cache"})
					tt.Equals(resp.Header["Connection"],    []string{"keep-alive"})
					tt.Equals(resp.Header["Content-Type"],  []string{"text/event-stream"})
					browsersReady.Done() // browser page ready
				} else {
					if strings.HasPrefix(text, "data: ") {
						b.got = append(b.got, text[6:])
					}
				}
			})
		}(&browsers[b])
	}
	browsersReady.Wait()
	// send to all browsers
	send()
	// Check all responses
	for _, browser := range browsers {
		tt.Equals(browser.exp, browser.got)
	}
}



func testSols(req *to.Can, length int, exps [][]byte) error {
	if sols, err := SolsFunc(req); err != nil {
		return err
	} else if data, err := sols.Data(); err != nil {
		return err
	} else if exp, got := length, len(data); exp != got {
		return fmt.Errorf("Expected:%d, got:%d", exp, got)
	} else {
		for i, exp := range exps {
			if got := data[i]; bytes.Compare(got, exp) != 0 {
				return fmt.Errorf("Expected:%v, got:%v", exp, got)
			}
		}
		return nil
	}
}

type Srv struct {
	sim  *boxes.Sim
	api  *httptest.Server
	tcps *from.TcpSrv
	get  http.Handler
}

// newSrvDefault returns a Srv tied to default sockets
func newSrvDefault() (*Srv, error) {
	socks := api.NewSocketsDefault()
	tcpDebug := base.LogF("TCP", base.Blue)
	if srv, err := newSrv(socks, tcpDebug); err != nil {
		return nil, err
	} else {
		return srv, nil
	}
}

// newSrv returns a Srv tied to given sockets and tcp logger
func newSrv(socks *api.Sockets, tcpLog base.Log) (*Srv, error) {
	if httpListener, err := socks.HttpListener(); err != nil {
		return nil, err
	} else if tcpPort, err := socks.TcpAddr(); err != nil {
		return nil, err
	} else if tcps, err := from.NewTcpSrv(tcpPort, tcpLog); err != nil {
		return nil, err
	} else {
		srv := &Srv {
			sim:   boxes.NewSim(SolsFunc),
			tcps:  tcps,
		}
		srv.api = &httptest.Server{
			Listener: httpListener,
			Config: &http.Server{
				Handler: http.HandlerFunc(srv.handler),
			},
		}
		srv.api.Start()
		return srv, nil
	}
}

func (s *Srv) SetUnsolSSE(indexPath, streamPath string) {
	srvSSE := unsols.NewSrvSSE(indexPath, streamPath)
	// redirect simulator unsol events to SSE to fire to browsers
	srvSSE.SetFunc(UnsolsFunc)
	s.sim.SetUnsolFunc(srvSSE.Fire)
	// redirect server gets to SSE http.Handler ServeHTTP
	s.get = srvSSE
}

func (s *Srv) handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		if s.get != nil {
			s.get.ServeHTTP(w, r)
		}
	case http.MethodPost:
		s.post(w, r)
	default:
		w.Write(api.JsonBytes(fmt.Errorf("Invalid path")))
	}
}

// expect format "/can/%d/req/%d,%d,0,%s"
// as cans-server microservice API
func (s *Srv) post(w http.ResponseWriter, r *http.Request) {
	if req, err := to.NewCanHttp(r.URL.Path); err != nil {
		w.Write(api.JsonBytes(err))
	} else if box := boxes.NewBox(req.Dst); box == nil {
		w.Write(api.JsonBytes(fmt.Errorf("Nil box from dst")))
	} else if req.Set != nil {
		if ok, err := s.sim.SolsSet(box, *req.Set, req.Extra); err != nil {
			w.Write(api.JsonBytes(err))
		} else if !ok {
			w.Write(api.JsonBytes(fmt.Errorf("Box off")))
		}
		w.Write(req.HttpResp())
	} else if req.Get != nil {
		if data, err := s.sim.SolsGet(box, *req.Get, req.Extra); err != nil {
			w.Write(api.JsonBytes(err))
		} else {
			for _, value := range data {
				s.tcps.Write(byte(req.Net), value)
				time.Sleep(100 * time.Millisecond)
			}
			w.Write(req.HttpResp())
		}
	}
}

func (s *Srv) Close() {
	s.api.Close()
	s.tcps.Close("")
}

func (s *Srv) writeErr(box *boxes.Box, sol base.Sol, extra []byte) error {
	if _, err := s.sim.SolsWrite(box, sol, extra); err != nil {
		return err
	}
	return nil
}

func (s *Srv) solsErr(length int, items map[string][]byte) error {
	if exp, got := length, s.sim.Sols(); exp != got {
		return fmt.Errorf("Expected:%02x, got:%02x", exp, got)
	}
	for key, sol := range items {
		if got := s.sim.Sol(key); bytes.Compare(sol, got) != 0 {
			return fmt.Errorf("Expected:%02x, got:%02x (not found key:%s)", sol, got, key)
		}
	}
	return nil
}

type Cli struct {
	client *api.Client
}

func newCli() *Cli {
	socks := api.NewSocketsDefault()
	client := api.NewClient(socks, base.LogF("Client", base.Yellow))
	client.SetTimeout(2 * time.Second) // actually the default already set at NewClient
	return &Cli{ client:client }
}

func (c *Cli) RespSolsPostErr(reqs []*to.Can, length int, exps []string) error {
	if resps, err := SolsPost(c.client, reqs...); err != nil {
		return err
	} else if exp, got := length, len(resps.Resps); exp != got {
		return fmt.Errorf("expected:%d, got:%d", exp, got)
	} else {
		// Check single http OK response WITH four tcp's Cans.
		for _, resp := range resps.Resps {
			if resp.Error != "" {
				return fmt.Errorf("unexpected error:%v", resp.Error)
			} else if exp, got := 200, resp.StatusCode; exp != got {
				return fmt.Errorf("expected:%d, got:%d", exp, got)
			} else if exp, got := len(exps), len(resp.Cans); exp != got {
				return fmt.Errorf("expected:%d, got:%d", exp, got)
			}
			for pos, can := range resp.Cans {
				if exp, got := exps[pos], can.String(); exp != got {
					return fmt.Errorf("expected:%s, got:%s", exp, got)
				}
			}
		}
		return nil
	}
}

var test1234Extras = [][]byte{
	[]byte{1, 0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71},
	[]byte{2, 0x12, 0x22, 0x32, 0x42, 0x52, 0x62, 0x72},
	[]byte{3, 0x13, 0x23, 0x33, 0x43, 0x53, 0x63, 0x73},
	[]byte{4, 0x14, 0x24, 0x34, 0x44, 0x54, 0x64, 0x74},
}
