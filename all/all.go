package all

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
	"gitlab.com/jmireles/cans-base/to"

	"gitlab.com/jmireles/cans-lines"
	"gitlab.com/jmireles/cans-lines/api"
	"gitlab.com/jmireles/cans-lines/i4s"
	"gitlab.com/jmireles/cans-lines/p1s"
	"gitlab.com/jmireles/cans-lines/rss"
	"gitlab.com/jmireles/cans-lines/t4s"
)

// SolsPost do a single or multiple given requests using the given client.
// The client's post waits for a response and use TCP to respond all solicited expected.
// All requests API responses are returned either sucessfull, timeout or in error.
func SolsPost(c *api.Client, reqs ...*to.Can) (*api.Resps, error) {
	if c == nil {
		return nil, fmt.Errorf("Invalid client")
	}
	resps := make([]*api.Resp, len(reqs))
	for i, req := range reqs {
		if sols, err := SolsFunc(req); err != nil {
			resps[i] = api.NewRespError(err)
		} else {
			resps[i] = c.Post(sols)
		}
	}
	return api.NewResps(resps), nil
}

// SolsFunc returns all solicited responses associated with the given CAN request.
// All lines types are checked: P1, RS, I4, T4...
func SolsFunc(req *to.Can) (sols *from.Sols, err error) {

	sols, err = from.NewSols(req)
	if err != nil {
		return
	}
	// check first common responses for any line
	if solsFunc(sols) {
		return
	}

	// then check particular responses for particular line
	switch req.Line {

	case to.P1:
		err = p1s.Sols(sols)

	/*
	case to.RS:
		if sol, masks, ok := rss.Masks(comm); ok {
			return doMasks(sol, masks), nil
		}
	
	case to.I4:
		if sol, masks, ok := i4s.Masks(comm); ok {
			return doMasks(sol, masks), nil
		}
	
	case to.T4:
		if sol, masks, ok := t4s.Masks(comm); ok {
			return doMasks(sol, masks), nil
		}*/

	default:
		err = fmt.Errorf("Invalid line:%d, comm:%d", req.Line, sols.Comm)

	}
	return
}

func solsFunc(sols *from.Sols) bool {
	switch sols.Comm {
		
	case byte(base.Get00_Version):
		// append the single command to expect for version
		sols.Add(base.Sol00_Version, [][]byte{nil})
		// no extra filter bytes are needed
		return true
	
	case byte(base.Get01_Serial):
		// append the single command to expect for serial
		// no extra filter bytes are needed
		sols.Add(base.Sol31_Serial, [][]byte{nil})
		return true

	default:
		// Not base solicited response, request line by line
		return false
	}
}

// UnsolsFunc implements lines.RespFunc
func UnsolsFunc(req *from.Can) (*lines.Resp, error) {
	switch req.Line {
	case from.P1:
		return p1s.UnsolsFunc(req)
	
	case from.I4:
		return i4s.UnsolsFunc(req)

	case from.RS:
		return rss.UnsolsFunc(req)

	case from.T4:
		return t4s.UnsolsFunc(req)

	default:
		return nil, fmt.Errorf("Invalid line:%d", req.Line)
	}
}







