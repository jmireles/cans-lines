package lines

import (
	"fmt"
	"strconv"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/from"
)

var (
	ReadOnlyCommon ReadOnlyMap
)

func init() {
	// Any line can request and receive...
	ReadOnlyCommon = map[base.Get]*ReadOnly {
		// ...box's firware version.
		base.Get00_Version: NewReadOnly(base.Sol00_Version),
		// ...box's serial number.
		base.Get01_Serial:  NewReadOnly(base.Sol31_Serial),
		// ...box's status values.
		base.Get02_Status:  NewReadOnly(base.Sol02_Status),
	}
}


// ReadWrite relates a get/set commands pair with an expected response sol command.
// Used for read-write configuration parameters.
type ReadWrite struct {
	// Set is the command sent to CAN box to write a read-write data.
	Set base.Set
	// Sol is the expected command response after requesting with Get command.
	Sol base.Sol
}

func NewReadWrite(set base.Set, sol base.Sol) *ReadWrite {
	return &ReadWrite{ 
		Set: set,
		Sol: sol,
	}
}

// ReadWriteMap is the complete array of ReadWrite operations a CAN line can perform.
type ReadWriteMap map[base.Get]*ReadWrite

// Exist returns the set command related with the given get command
// Get is the command sent to CAN box to read a read-write data.
func (m *ReadWriteMap) Exist(get base.Get) *ReadWrite {
	if rw, ok := (*m)[get]; ok {
		return rw
	}
	return nil
}

// ReadOnly relates a req command and an expected response sol command.
// Used for read-only CAN lines's values fixed such as version, serial or 
// variable like status, curves, etc
type ReadOnly struct {
	// Sols are the expected commands response after requesting with Get command.
	Sols []base.Sol
}

func NewReadOnly(sols ...base.Sol) *ReadOnly {
	return &ReadOnly{
		Sols: sols,
	}
}

// ReadOnlyMap is the complete array of ReadOnly operations a give CAN line can perform.
type ReadOnlyMap map[base.Get]*ReadOnly

// Exist returns the set command related with the given get command
// Get is the command sent to CAN box to read a read-only data.
func (m *ReadOnlyMap) Exist(get base.Get) *ReadOnly {
	if ro, ok := (*m)[get]; ok {
		return ro
	}
	return nil
}









// Key is an enum to group line's exclusive unsol commands from others.
// As an example pulsators have seven base.Resp commands all related to the
// same Key KeyStatus. The commands are: base.Resp01_Restart, 
// p1.Resp05_Idle, p1.Resp06_Working, p1.Resp07_Alarmed, 
// p1.Resp16_NoPhaseFront, p1.Resp17_NoPhaseRear and p1.Resp20_Stimulation.
// A box can have only one of these states at once.
type Key int



type Value uint64

func NewValue(base uint64, extra []byte) Value {
	for i, e := range extra {
		base += uint64(e) << (8*i)
	}
	return Value(base)
}

func (v Value) String() string {
	return strconv.FormatUint(uint64(v), 10)
}

type Map map[Key]Value

// RespFunc converts can into Resp which contains a reference to can data
// and also a Map with Keys/Values related to line/command/extra can's fields
type RespFunc func(can *from.Can) (*Resp, error)

type Resp struct {
    Can *from.Can
    Map Map
}

func NewResp(can *from.Can, respFunc RespFunc) (*Resp, error) {
	if can == nil {
		return nil, fmt.Errorf("can nil")
	}
	if can.Unsol == nil {
		return nil, fmt.Errorf("can not unsol")
	}
	if respFunc != nil {
		return respFunc(can)
	} else {
		return &Resp{
			Can: can,
		}, nil
	}
}
